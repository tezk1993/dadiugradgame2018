﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashAndFade : MonoBehaviour {

    public bool flash;
    public AnimationCurve flashSpeed;
    public bool fade;
    public AnimationCurve[] fadeCurves;
    [Tooltip("only needed if fadeCurve is not clamped")] public float numberOfCycles;
    private float totalFadeTime;
    private float elapsedFadeTime = 0;    
    private bool disableFade = true;
	public int fadeCurveIndex = 0;

	// Use this for initialization
	void Start () {
        if (fade)
        {
            StartCoroutine(FadeInOut());
        }
        if (flash)
        {
            StartCoroutine(Flash());
        }
    }

    public IEnumerator Flash()
    {
        yield return null;
    }
    public void disableFadeOnTouch()
    {
        disableFade = true;
    }
    public void enableFadeOnTouch()
    {
        disableFade = false;
    }
    public IEnumerator FadeInOut()
    {
        if (fadeCurves[fadeCurveIndex].postWrapMode != WrapMode.Clamp)
        {
            totalFadeTime = fadeCurves[fadeCurveIndex].keys[fadeCurves[fadeCurveIndex].length - 1].time * numberOfCycles;
        }
        else
        {
            totalFadeTime = fadeCurves[fadeCurveIndex].keys[fadeCurves[fadeCurveIndex].length - 1].time;
        }
        var image = gameObject.GetComponent<Image>();
        Color imageColor = gameObject.GetComponent<Image>().color;
        while (elapsedFadeTime < totalFadeTime || !disableFade)
        {
            imageColor.a = fadeCurves[fadeCurveIndex].Evaluate(elapsedFadeTime);
            image.color = imageColor;
            yield return null;
            elapsedFadeTime += Time.deltaTime;
        }
        elapsedFadeTime = 0;
    }

	public IEnumerator FadeInOut(GameObject objetToDisable)
	{
		if (fadeCurves[fadeCurveIndex].postWrapMode != WrapMode.Clamp)
		{
			totalFadeTime = fadeCurves[fadeCurveIndex].keys[fadeCurves[fadeCurveIndex].length - 1].time * numberOfCycles;
		}
		else
		{
			totalFadeTime = fadeCurves[fadeCurveIndex].keys[fadeCurves[fadeCurveIndex].length - 1].time;
		}
		var image = gameObject.GetComponent<Image>();
		Color imageColor = gameObject.GetComponent<Image>().color;
		while (elapsedFadeTime < totalFadeTime || !disableFade)
		{
			imageColor.a = fadeCurves[fadeCurveIndex].Evaluate(elapsedFadeTime);
			image.color = imageColor;
			yield return null;
			elapsedFadeTime += Time.deltaTime;
		}
		elapsedFadeTime = 0;
		objetToDisable.SetActive(false);
	}
}
