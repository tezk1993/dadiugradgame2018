﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
public class EventManager : MonoBehaviour
{


    public float deadEnemyCount;
    public float deathPercentage;
    public float bossHealthPercentage;

    public StateController Boss;
    public bool checkEnemyDeath = true;

    public bool phasesOver = false;

    public Phase currentPhase;

    public List<StateController> Enemies;
    public List<StateController> aliveEnemies;
    public float closestEnemy;
    public List<enemySpawner> enemySpawners;

    public List<pillarController> allPillars;
    public List<pillarController> activePillars;

    public bool incrementPhaseOccuring = false;


    [SerializeField]
    public enum TriggerType
    {
        EnemyDeathTrigger,
        AreaTrigger,
        HealthTrigger,
        BossHealthTrigger
    }

    int eventListCount;

    [System.Serializable]
    public class Phase
    {
        [Tooltip("This phases number in the list")]
        [Range(0, 100)]
        public int phaseNumber;
        [Tooltip("The method for triggering this phase")]
        public TriggerType thisTrigger;
        [Range(0f, 100f)]
        [Tooltip("The percentage of enemies that need to die to trigger ")]
        public float deathPercentageThreshold;
        [Range(0f, 100f)]
        [Tooltip("The percentage of health that the boss to lose to trigger ")]
        public float bossHealthPercentageThreshold;
        [Tooltip("Areas that are triggers this phase")]
        public List<TriggerArea> TriggerAreas;
        [Tooltip("The events that shall happen in this phase")]
        public UnityEvent phaseEvent;
        [Tooltip("Delay for triggering the phase")]
        [Range(0f, 50f)]
        public float phaseDelay;
    }


    public List<Phase> eventsLists;
    private bool phaseTriggered;
    #region 
    private void Awake()
    {
        GameObject[] spawners = GameObject.FindGameObjectsWithTag("EnemySpawner");
        foreach (GameObject spawner in spawners)
        {
            enemySpawners.Add(spawner.GetComponent<enemySpawner>());
        }
        foreach (enemySpawner spawner in enemySpawners)
        {
            spawner.eventManager = this;
        }
        /*
        if (enemySpawners.Count > 0)
        {
            for (int i = 0; i < enemySpawners.Count; i++)
            {
                for (int j = 0; j < enemySpawners[i].enemySpawnSets.Count; j++)
                {
                    for (int o = 0; o < enemySpawners[i].enemySpawnSets[j].enemyScript.Count; o++)
                    {
                        enemySpawners[i].enemySpawnSets[j].enemyScript[o].eventManager = this;
                    }
                }
            }
        }
        */
        if (eventsLists.Count != 0)
        {
            currentPhase = eventsLists[0];

            assignTriggerAreas();
            for (int i = 0; i < eventsLists.Count; i++)
            {
                eventsLists[i].phaseNumber = i;
            }
        }
    }
    #endregion

    #region

    [ContextMenu("Lower all active pillars")]
    public void lowerPillars()
    {
        for (int i = 0; i < activePillars.Count; i++)
        {
            activePillars[i].LowerPillars();
        }
    }

    [ContextMenu("Check enemy count")]
    public void CheckEnemyCount()
    {
        checkEnemyDeath = false;
        deadEnemyCount = 0;
        for (int i = 0; i < Enemies.Count; i++)
        {
            if (Enemies[i].dead == true)
            {
                deadEnemyCount++;
            }

        }
        deathPercentage = deadEnemyCount / Enemies.Count * 100;
        if (currentPhase.deathPercentageThreshold <= deathPercentage)
        {
            CheckEventTrigger(TriggerType.EnemyDeathTrigger);
        }
        for (int i = 0; i < aliveEnemies.Count; i++)
        {
            if (Enemies[i].dead == true)
            {
                aliveEnemies.RemoveAt(i);
            }
        }
        checkEnemyDeath = true;
    }

    public void clearEnemies()
    {
        deadEnemyCount = 0;
        deathPercentage = 0;
        for (int i = 0; i < Enemies.Count; i++)
        {
            if (Enemies[i].dead == true)
            {
                Enemies.RemoveAt(i);
            }
        }
    }

    public void checkBossHealth()
    {
        bossHealthPercentage = (float)Boss.enemyHP / Boss.bossHP * 100;
        Debug.Log("Check Boss Health" + bossHealthPercentage);
        Debug.Log(Boss.enemyHP + " " + Boss.bossHP);

        if (currentPhase.bossHealthPercentageThreshold >= bossHealthPercentage)
        {
            CheckEventTrigger(TriggerType.BossHealthTrigger);
        }

    }



    public void CheckEventTrigger(TriggerType trigger)
    {
        if (currentPhase.thisTrigger == trigger)
        {
            InvokeEvent(currentPhase.phaseDelay, currentPhase.phaseEvent);
            phaseTriggered = true;
        }
    }


    [ContextMenu("Delayed Invoke Test")]
    void InvokeEvent(float delay, UnityEvent triggeredEvent)
    {

        StartCoroutine(DelayedInvokeCoroutine(delay, triggeredEvent));
    }

    void incrementPhase()
    {
        if (currentPhase.phaseNumber != eventsLists.Count - 1 && phasesOver == false)
        {
            currentPhase = eventsLists[currentPhase.phaseNumber + 1];
            assignTriggerAreas();
            phaseTriggered = false;
            if (currentPhase.thisTrigger == TriggerType.EnemyDeathTrigger)
            {
                StartCoroutine(deathCheck());
            }
            else
            {
                StopCoroutine(deathCheck());
            }
        }
        else
        {
            phasesOver = true;
        }
    }
    IEnumerator deathCheck()
    {
        yield return new WaitForSeconds(5f);
        CheckEnemyCount();
    }
    public void gotoLastPhase()
    {
        currentPhase = eventsLists[eventsLists.Count - 1];
        InvokeEvent(currentPhase.phaseDelay, currentPhase.phaseEvent);
    }

    void assignTriggerAreas()
    {

        if (currentPhase.thisTrigger == TriggerType.AreaTrigger)
        {
            for (int i = 0; i < currentPhase.TriggerAreas.Count; i++)
            {
                currentPhase.TriggerAreas[i].eventManager = this;
            }
        }
    }

    IEnumerator DelayedInvokeCoroutine(float delay, UnityEvent triggeredEvent)
    {
        yield return new WaitForSeconds(delay);
        triggeredEvent.Invoke();
        incrementPhase();

        yield return null;
    }

    public void PlayMusicBattle()
    {
        AkSoundEngine.SetState("Music_State", "Battle");
    }


    public void LoadNextLevel()
    {
        // Temporary solution for the DEMO on 22/11/2018
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

        // This would be the final solution
        SceneSwitcher.instance.LoadNextSceneAsync();
    }

    public void LoadTestLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    #endregion
}
