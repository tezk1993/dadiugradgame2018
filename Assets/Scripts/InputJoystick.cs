﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputJoystick : InputBase {
    private int screenHeigth;
    private int screenWidth;
    private float screenHeigthThreshold;
    public float screenThresholdY; // needs a value like 80 (the value determines the amount of the screen the user can use in %)

    private Vector2 pointA;
    private Vector2 pointB;
    Vector2 direction;
    public float fingerMaxDistance;
    public float joystickScreenBorder;
    private float inputAngleOffset;


    private int leftTouchId;
    private int rightTouchId;
    private bool leftTouching;
    private bool rightTouching;

    public float timeBeforeInputIsCharging;
    Coroutine dashCoroutine;

    private Transform circle;
    private Transform outerCircle;
    public GameObject dashIndicator;
    
    Vector2 outputDirection;
    Vector2 outputDashDirection;
    bool isTouching;
    bool isCharging;

    public override Vector2 Direction()
    {
        return outputDirection;
    }

    public override Vector2 DashDirection()
    {
        return outputDashDirection;
    }

    public override bool IsTouching()
    {
        return isTouching;
    }
    public override bool IsCharging()
    {
        return isCharging; ;
    }

    // Use this for initialization
    void Start () {
        screenHeigth = Screen.height;
        screenWidth = Screen.width;
        if(screenThresholdY == 0)
        {
            screenThresholdY = 80f;
        }
        if (timeBeforeInputIsCharging == 0)
        {
            timeBeforeInputIsCharging = 0.2f;
        }
        if(fingerMaxDistance == 0)
        {
            fingerMaxDistance = 100;
        }
        if(joystickScreenBorder == 0)
        {
            joystickScreenBorder = 20;
        }
        dashIndicator = GameObject.Find("AimArrow");
        screenHeigthThreshold = screenHeigth * (screenThresholdY / 100);
        circle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Inner");
        outerCircle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Outer");

        leftTouchId = -1;
        rightTouchId = -1;
        
        inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, Camera.main.transform.forward, Vector3.up);

    }


    private void Update()
    {
        for (int i = 0; i < Input.touchCount && i < 2; i++)
        {
            Touch touch = Input.touches[i];
            
            if (touch.phase == TouchPhase.Began)
            {
                bool isLeft = Input.GetTouch(i).position.x < screenWidth / 2;
                if (isLeft && !leftTouching)
                {
                    leftTouching = true;
                    leftTouchId = i;
                    if (rightTouchId == i) rightTouchId = FlipTouchId(rightTouchId);
                    //Debug.Log("LeftTouch Set");
                }
                else if(!isLeft && !rightTouching)
                {
                    rightTouching = true;
                    rightTouchId = i;
                    if (leftTouchId == i) leftTouchId = FlipTouchId(leftTouchId);
                    //Debug.Log("RightTouch Set");
                }
            }
            if (i == leftTouchId)
            {
                HandleMovementTouch(touch);
            }
            else if (i == rightTouchId)
            {
                HandleDashTouch(touch);
            }
            if (touch.phase == TouchPhase.Ended)
            {
                if (leftTouchId == i)
                {
                    leftTouching = false;
                    leftTouchId = -1;
                    if (rightTouchId > -1) rightTouchId = 0;
                    //Debug.Log("LeftTouch UNSet");

                }
                else if(rightTouchId == i)
                {
                    rightTouching = false;
                    rightTouchId = -1;
                    if (leftTouchId > -1) leftTouchId = 0;
                    //Debug.Log("RightTouch UNSet");

                }
            }
            
        }
    } 

    int FlipTouchId(int touchId)
    {
        return touchId ^ 1;
    }

    void HandleMovementTouch(Touch touch)
    {
        
        if(touch.phase == TouchPhase.Began)
        {
            if (touch.position.y < screenHeigthThreshold)
            {
                pointA = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
                circle.transform.position = pointA;
                outerCircle.transform.position = pointA;

                AdjustJoystickToBorder();

                circle.gameObject.SetActive(true);
                outerCircle.gameObject.SetActive(true);
                //dirIndicator.gameObject.SetActive(true); 
            }
        }
        if(touch.phase == TouchPhase.Moved)
        {
            isTouching = true;
            if (touch.position.y < screenHeigthThreshold)
            {
                pointB = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
                Vector2 offset = pointB - pointA;
                if (offset.magnitude > fingerMaxDistance)
                {
                    offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
                    pointA = pointB - offset;

                    AdjustJoystickToBorder();
                }
                direction = Vector2.ClampMagnitude(offset, 1.0f);

                var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                outputDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);
            }
        }
        else if(touch.phase == TouchPhase.Stationary)
        {
            isTouching = true;
            if (touch.position.y < screenHeigthThreshold)
            {
                pointB = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
                Vector2 offset = pointB - pointA;
                if (offset.magnitude > fingerMaxDistance)
                {
                    offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
                    pointA = pointB - offset;

                    AdjustJoystickToBorder();
                }
                direction = Vector2.ClampMagnitude(offset, 1.0f);

                var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                outputDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
                circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);
            }
        }
        else
        {
            isTouching = false;
        }
        if(touch.phase == TouchPhase.Ended)
        {
            
            //isTouching = false;
            circle.gameObject.SetActive(false);
            outerCircle.gameObject.SetActive(false);
        }
        
    }

    void HandleDashTouch(Touch touch)
    {
        if(touch.phase == TouchPhase.Began)
        {
            if (touch.position.y < screenHeigthThreshold)
            {
                dashCoroutine = StartCoroutine(Charging());
            }
        }
        if(touch.phase == TouchPhase.Ended)
        {
            StopCoroutine(dashCoroutine);
            InvokeDash(outputDirection);
            isCharging = false;
        }
    }

    IEnumerator Charging()
    {
        yield return new WaitForSeconds(timeBeforeInputIsCharging);
        isCharging = true;
    }

    void AdjustJoystickToBorder()
    {
        pointA.x = Mathf.Max(pointA.x, joystickScreenBorder);
        pointA.x = Mathf.Min(pointA.x, screenWidth - joystickScreenBorder);
        pointA.y = Mathf.Max(pointA.y, joystickScreenBorder);
        pointA.y = Mathf.Min(pointA.y, screenHeigth - joystickScreenBorder);

        outerCircle.transform.position = pointA;
    }
}
