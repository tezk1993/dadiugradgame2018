﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
public class ParticleController : MonoBehaviour {

    public enum Particles
    {
        PlayerDash,
        PlayerRipple,
        PlayerHit,
        EnemyHit,
        EnemyDeath
    }

    [System.Serializable]
    public class particleControl
    {
        public Particles choosenParticle;
        public GameObject particlePrefab;
        public Vector3 particlePosition;
        public float particleDelay;
    }
    
    public List<particleControl> particleControllers;

    public void playParticle(particleControl particle)
    {

    }
}
