﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerArea : MonoBehaviour {

    public EventManager eventManager;

    private void Awake()
    {
        //eventManager = GameObject.FindGameObjectWithTag("EventManager").GetComponent<EventManager>();
    }

    public bool triggered = false;
    private void OnTriggerStay(Collider other)
    {

        if (eventManager != null && other.CompareTag("Player") && triggered == false)
        {
            if (eventManager.currentPhase.TriggerAreas.Contains(this))
            {
                triggered = true;
                Debug.Log("Test tag");
                eventManager.CheckEventTrigger(EventManager.TriggerType.AreaTrigger);
            }

        }
    }
}
