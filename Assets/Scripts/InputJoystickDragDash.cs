﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputJoystickDragDash : InputBase {
	private int screenHeigth;
	private int screenWidth;
	private float screenHeigthThreshold;
	public float screenThresholdY; // needs a value like 80 (the value determines the amount of the screen the user can use in %)

	private Vector2 pointA;
	private Vector2 pointB;
	private Vector2 pointADash;
	private Vector2 pointBDash;
	private Vector2 movementRest;
	private Vector2 dashRest;
	public float transparencyValue;
	Vector2 direction;
	Vector2 directionDash;
	public float dashDirThreshold;
	public float fingerMaxDistance;
	public float joystickScreenBorder;
	private float inputAngleOffset;


	private int leftTouchId;
	private int rightTouchId;
	private bool leftTouching;
	private bool rightTouching;

	public float timeBeforeInputIsCharging;
	Coroutine dashCoroutine;

	private Transform circle;
	private Transform outerCircle;
	private Transform circleDash;
	private Transform outerCircleDash;

	Vector2 outputDirection;
	Vector2 outputDirectionDash;
	bool isTouching;
	bool isCharging;

	public bool disableMovement;
	public bool disableDash;
	FlashAndFade fadeCircle;
	FlashAndFade fadeOuterCircle;
	FlashAndFade fadeCircleDash;
	FlashAndFade fadeOuterCircleDash;
	bool callOnce = true;
	bool callOnceDash = true;

	public override Vector2 Direction()
	{
		return outputDirection;
	}

	public override Vector2 DashDirection()
	{
		return outputDirectionDash;
	}

	public override bool IsTouching()
	{
		return isTouching;
	}
	public override bool IsCharging()
	{
		return isCharging;
	}

	// Use this for initialization
	void Start() {
		screenHeigth = Screen.height;
		screenWidth = Screen.width;
		movementRest = new Vector2(200, 200);
		dashRest = new Vector2(screenWidth - 200, 200);



		if (transparencyValue <= 0 || transparencyValue > 1)
		{
			transparencyValue = 0.5f;
		}
		if (screenThresholdY == 0)
		{
			screenThresholdY = 80f;
		}
		if (timeBeforeInputIsCharging == 0)
		{
			timeBeforeInputIsCharging = 0.2f;
		}
		if (fingerMaxDistance == 0)
		{
			fingerMaxDistance = 100;
		}
		if (joystickScreenBorder == 0)
		{
			joystickScreenBorder = 20;
		}
		if (dashDirThreshold == 0)
		{
			dashDirThreshold = 150;
		}

		screenHeigthThreshold = screenHeigth * (screenThresholdY / 100);
		circle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Inner");
		outerCircle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Outer");
		fadeCircle = circle.GetComponent<FlashAndFade>();
		fadeOuterCircle = outerCircle.GetComponent<FlashAndFade>();
		ChangeTransparency(circle, transparencyValue);
		ChangeTransparency(outerCircle, transparencyValue);

		circleDash = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Inner2");
		outerCircleDash = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Outer2");
		fadeCircleDash = circleDash.GetComponent<FlashAndFade>();
		fadeOuterCircleDash = outerCircleDash.GetComponent<FlashAndFade>();
		ChangeTransparency(circleDash, transparencyValue);
		ChangeTransparency(outerCircleDash, 0);


		leftTouchId = -1;
		rightTouchId = -1;

		inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, Camera.main.transform.forward, Vector3.up);


		circle.gameObject.SetActive(true);
		outerCircle.gameObject.SetActive(true);
		circleDash.gameObject.SetActive(true);
		outerCircleDash.gameObject.SetActive(true);
		//circle.transform.position = movementRest;
		movementRest = outerCircle.transform.position;
		dashRest = circleDash.transform.position;
		//outerCircleDash.transform.position = dashRest;
	}


	private void Update()
	{

		for (int i = 0; i < Input.touchCount && i < 2; i++)
		{
			Touch touch = Input.touches[i];

			if (touch.phase == TouchPhase.Began)
			{
				bool isLeft = Input.GetTouch(i).position.x < screenWidth / 2;
				if (isLeft && !leftTouching)
				{
					leftTouching = true;
					leftTouchId = i;
					if (rightTouchId == i) rightTouchId = FlipTouchId(rightTouchId);
					//Debug.Log("LeftTouch Set");
				}
				else if (!isLeft && !rightTouching)
				{
					rightTouching = true;
					rightTouchId = i;
					if (leftTouchId == i) leftTouchId = FlipTouchId(leftTouchId);
					//Debug.Log("RightTouch Set");
				}
			}
			if (i == leftTouchId)
			{
				HandleMovementTouch(touch);
			}
			else if (i == rightTouchId)
			{
				HandleDashTouch(touch);
			}
			if (touch.phase == TouchPhase.Ended)
			{
				if (leftTouchId == i)
				{
					leftTouching = false;
					leftTouchId = -1;
					if (rightTouchId > -1) rightTouchId = 0;
					//Debug.Log("LeftTouch UNSet");

				}
				else if (rightTouchId == i)
				{
					rightTouching = false;
					rightTouchId = -1;
					if (leftTouchId > -1) leftTouchId = 0;
					//Debug.Log("RightTouch UNSet");

				}
			}

		}
	}

	int FlipTouchId(int touchId)
	{
		return touchId ^ 1;
	}

	void HandleMovementTouch(Touch touch)
	{
		if (disableMovement) {return;}
		if (callOnce) 
		{
			fadeCircle.disableFadeOnTouch();
			fadeOuterCircle.disableFadeOnTouch();
			callOnce = false;
		}
		if (touch.phase == TouchPhase.Began)
		{
			if (touch.position.y < screenHeigthThreshold)
			{
				pointA = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
				circle.transform.position = pointA;
				outerCircle.transform.position = pointA;

				AdjustJoystickToBorder();

				ChangeTransparency(circle, 1);
				ChangeTransparency(outerCircle, 1);
			}
		}
		if (touch.phase == TouchPhase.Moved)
		{
			isTouching = true;
			if (touch.position.y < screenHeigthThreshold)
			{
				pointB = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
				Vector2 offset = pointB - pointA;
				if (offset.magnitude > fingerMaxDistance)
				{
					offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
					pointA = pointB - offset;

					AdjustJoystickToBorder();
				}
				direction = Vector2.ClampMagnitude(offset, 1.0f);

				var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
				angle += inputAngleOffset;
				angle *= Mathf.Deg2Rad;
				outputDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

				if (!isCharging)
				{
					outputDirectionDash = outputDirection;
				}
				circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);
			}
		}
		else if (touch.phase == TouchPhase.Stationary)
		{
			isTouching = true;
			if (touch.position.y < screenHeigthThreshold)
			{
				pointB = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
				Vector2 offset = pointB - pointA;
				if (offset.magnitude > fingerMaxDistance)
				{
					offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
					pointA = pointB - offset;

					AdjustJoystickToBorder();
				}
				direction = Vector2.ClampMagnitude(offset, 1.0f);

				var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
				angle += inputAngleOffset;
				angle *= Mathf.Deg2Rad;
				outputDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
				if (!isCharging)
				{
					outputDirectionDash = outputDirection;
				}
				circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);
			}
		}
		if (touch.phase == TouchPhase.Ended)
		{
			isTouching = false;
			ChangeTransparency(circle, transparencyValue);
			ChangeTransparency(outerCircle, transparencyValue);
			circle.transform.position = movementRest;
			outerCircle.transform.position = movementRest;
		}

	}

	void HandleDashTouch(Touch touch)
	{
		if (disableDash) { return; }
		if (callOnceDash) 
		{
			fadeCircleDash.disableFadeOnTouch();
			fadeOuterCircleDash.disableFadeOnTouch();
			fadeCircleDash.numberOfCycles = 0;
			fadeOuterCircleDash.numberOfCycles = 0;
			callOnceDash = false;
		}
		if (touch.phase == TouchPhase.Began)
		{
			if (touch.position.y < screenHeigthThreshold)
			{
				pointADash = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
				circleDash.transform.position = pointADash;
				outerCircleDash.transform.position = pointADash;

				ChangeTransparency(circleDash, 1);
				isCharging = true;
			}
		}
		if (touch.phase == TouchPhase.Moved)
		{
			if (touch.position.y < screenHeigthThreshold)
			{
				pointBDash = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
				Vector2 offsetDash = pointBDash - pointADash;
				directionDash = Vector2.ClampMagnitude(offsetDash, 1.0f);

				var angle = Vector2.SignedAngle(new Vector2(1, 0), directionDash.normalized);
				angle += inputAngleOffset;
				angle *= Mathf.Deg2Rad;
				if (Vector2.Distance(pointADash, pointBDash) > dashDirThreshold)
				{
					outputDirectionDash = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
					ChangeTransparency(outerCircleDash, 1);
				}
				else
				{
					outputDirectionDash = outputDirection;
					ChangeTransparency(outerCircleDash, transparencyValue);
				}

				circleDash.transform.position = new Vector3(pointADash.x + offsetDash.x, pointADash.y + offsetDash.y, 0);
			}
		}
		else if (touch.phase == TouchPhase.Stationary)
		{
			if (touch.position.y < screenHeigthThreshold)
			{
				pointBDash = new Vector3(touch.position.x, touch.position.y, Camera.main.transform.position.z);
				Vector2 offsetDash = pointBDash - pointADash;

				directionDash = Vector2.ClampMagnitude(offsetDash, 1.0f);

				var angle = Vector2.SignedAngle(new Vector2(1, 0), directionDash.normalized);
				angle += inputAngleOffset;
				angle *= Mathf.Deg2Rad;
				if (Vector2.Distance(pointADash, pointBDash) > dashDirThreshold)
				{
					outputDirectionDash = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
					ChangeTransparency(outerCircleDash, 1);
				} else
				{
					outputDirectionDash = outputDirection;
					ChangeTransparency(outerCircleDash, transparencyValue);
				}
				circleDash.transform.position = new Vector3(pointADash.x + offsetDash.x, pointADash.y + offsetDash.y, 0);
			}
		}
		if (touch.phase == TouchPhase.Ended)
		{
			InvokeDash(outputDirectionDash);

			ChangeTransparency(circleDash, transparencyValue);
			ChangeTransparency(outerCircleDash, 0);
			circleDash.transform.position = dashRest;
			outerCircleDash.transform.position = dashRest;

			isCharging = false;
		}

	}

	IEnumerator Charging()
	{
		yield return new WaitForSeconds(timeBeforeInputIsCharging);
		isCharging = true;
		Debug.Log("Dash has been charged");
	}

	void AdjustJoystickToBorder()
	{
		pointA.x = Mathf.Max(pointA.x, joystickScreenBorder);
		pointA.x = Mathf.Min(pointA.x, screenWidth - joystickScreenBorder);
		pointA.y = Mathf.Max(pointA.y, joystickScreenBorder);
		pointA.y = Mathf.Min(pointA.y, screenHeigth - joystickScreenBorder);

		outerCircle.transform.position = pointA;
	}

	public Transform ChangeTransparency(Transform target, float val)
	{
		var tempColor = target.GetComponent<Image>().color;
		tempColor.a = val;
		target.GetComponent<Image>().color = tempColor;
		return target;
	}

	public void EnableMovement(bool enabled) 
	{
		disableMovement = !enabled;
		circle.gameObject.SetActive(enabled);
		outerCircle.gameObject.SetActive(enabled);
		
	}

	public void EnableDash(bool enabled) 
	{
		disableDash = !enabled;
		circleDash.gameObject.SetActive(enabled);
		outerCircleDash.gameObject.SetActive(enabled);
	}

	public void StartMovementFade() 
	{

		fadeCircle.enableFadeOnTouch();
		fadeOuterCircle.enableFadeOnTouch();
		StartCoroutine(fadeCircle.FadeInOut());
		StartCoroutine(fadeOuterCircle.FadeInOut());

	}

	public void StartDashFade() 
	{
		fadeOuterCircleDash.enableFadeOnTouch();
		fadeCircleDash.enableFadeOnTouch();
		StartCoroutine(fadeCircleDash.FadeInOut());
		StartCoroutine(fadeOuterCircleDash.FadeInOut());
	}

	public void FadeAndDisableMovement() 
	{
		fadeCircle.fadeCurveIndex = 1;
		fadeOuterCircle.fadeCurveIndex = 1;
		StartCoroutine(fadeCircle.FadeInOut(circle.gameObject));
		StartCoroutine(fadeOuterCircle.FadeInOut(outerCircle.gameObject));
		EnableMovement(false);
		outputDirection = Vector2.zero;
		isTouching = false;
		isCharging = false;
		StartCoroutine(TempPostStaffSequence());
	}

	public void FadeAndDisableDash() 
	{
		//fadeCircleDash.fadeCurveIndex = 1;
		//fadeOuterCircleDash.fadeCurveIndex = 1;
		StartCoroutine(fadeCircleDash.FadeInOut(circleDash.gameObject));
		StartCoroutine(fadeOuterCircle.FadeInOut(outerCircleDash.gameObject));
	}
	public IEnumerator TempPostStaffSequence() 
	{
		yield return new WaitForSeconds(1);
		EnableMovement(true);
		EnableDash(true);
		StartDashFade();
		//enable & fade UI elements in
		//fade in and out dash button
		//enable dash meter elements
		//enable direction indicator

	}
}
