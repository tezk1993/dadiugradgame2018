﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour {

    //objects for each image contain that needs to be able to change language
    public GameObject mainMenuPlay;
    public GameObject mainMenuSettings;
    public GameObject settingsBack;
    public GameObject settingsSound;
    public GameObject settingsLanguage;
    public GameObject settingsCredits;
    public GameObject settingsGamePlay;
    public GameObject soundSound;
    public GameObject soundBack;
    public GameObject soundMaster;
    public GameObject soundMusic;
    public GameObject soundSFX;
    public GameObject languageBack;
    public GameObject languageEnglish;
    public GameObject languageDanish;
    public GameObject languageLanguage;
    public GameObject gamePlayBack;
    public GameObject gamePlayCamShake;
    public GameObject gamePlayGamePlay;
    public GameObject creditsBack;
    public GameObject creditsAbout;
    public GameObject creditsText;

    private List<GameObject> languageDependant;

    private FadeScript fadeScript;
    private GameObject[] menus;
    public GameObject cameraMenu;
    public GameObject cameraSettings;
    public GameObject cameraAbout;

    public Transform head;
    public GameObject loadingObject;

    public float volumeIncrements;

    private PermanentDataContainer settingsContainer;

    public float fadeTime;
    [Tooltip("time for each fade, double for complete fade time")]private float animSpeed;

    [Tooltip("dont mess with")]public Sprite[] danishSprites;
    private Sprite[] englishSprites;

    private GameObject mainMenu;
    private GameObject creditsMenu;
    private GameObject controlsMenu;
    private GameObject languageMenu;
    private GameObject soundMenu;
    private GameObject settingsMenu;

    private GameObject english;
    private GameObject danish;

    // Use this for initialization
    private void Start()
    {
        //    //loadingObject = GameObject.Find("LoadingScreen");
        //    head = GameObject.Find("LoadingScreen").transform.Find("Head");
        //    loadingBar = loadingObject.transform.Find("LoadingBar");
        //    loadingBarFill = loadingObject.transform.Find("LoadingBar_Fill");

        menus = new GameObject[6];

        
        fadeScript = transform.GetChild(6).GetComponent<FadeScript>();

        settingsContainer = PermanentDataContainer.instance;

        //ensuring main menu is the only active menu
        menus[0] = mainMenu = transform.Find("Main").gameObject;
        menus[1] = settingsMenu = transform.Find("Settings").gameObject;
        menus[2] = soundMenu = transform.Find("SoundSettings").gameObject;
        menus[3] = languageMenu = transform.Find("Language").gameObject;
        menus[4] = controlsMenu = transform.Find("GamePlay").gameObject;
        menus[5] = creditsMenu = transform.Find("Credits").gameObject;

        english = languageMenu.transform.Find("English").gameObject;
        danish = languageMenu.transform.Find("Danish").gameObject;

        Debug.Log(transform.childCount);
        for (int i = 0; i < 6; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        transform.GetChild(0).gameObject.SetActive(true);

        //putting all items that need to be able to change langauge into a list

        languageDependant = new List<GameObject>();

        languageDependant.Add(mainMenuPlay);
        languageDependant.Add(mainMenuSettings);
        languageDependant.Add(settingsBack);
        languageDependant.Add(settingsSound);
        languageDependant.Add(settingsLanguage);
        languageDependant.Add(settingsCredits);
        languageDependant.Add(settingsGamePlay);
        languageDependant.Add(soundSound);
        languageDependant.Add(soundBack);
        languageDependant.Add(soundMaster);
        languageDependant.Add(soundMusic);
        languageDependant.Add(soundSFX);
        languageDependant.Add(languageBack);
        languageDependant.Add(languageEnglish);
        languageDependant.Add(languageDanish);
        languageDependant.Add(languageLanguage);
        languageDependant.Add(gamePlayBack);
        //languageDependant.Add(gamePlayVibrate);
        languageDependant.Add(gamePlayCamShake);
        languageDependant.Add(gamePlayGamePlay);
        languageDependant.Add(creditsBack);
        languageDependant.Add(creditsAbout);
        languageDependant.Add(creditsText);

        englishSprites = new Sprite[languageDependant.Count];

        //computing and setting the animation speed
        animSpeed = 1 / fadeTime;
        if (fadeScript.animator == null)
        {
            Debug.Log("fadescript animator not set");
            fadeScript.animator = fadeScript.gameObject.GetComponent<Animator>();
        }
        fadeScript.animator.speed = animSpeed;

        for (int i = 0; i < languageDependant.Count; i++)
        {

            englishSprites[i] = languageDependant[i].GetComponent<Image>().sprite;
            //danish.GetComponent<ChangeColor>().SetInActive();
            //english.GetComponent<ChangeColor>().SetActive();
        }
        //PermanentDataContainer.instance.english = true;
        if (!PermanentDataContainer.instance.english)
        {
            LanguageDanish();
        }
    }

    public void LanguageDanish()
    {
        for (int i = 0; i < languageDependant.Count; i++)
        {
            languageDependant[i].GetComponent<RectTransform>().sizeDelta = new Vector2(danishSprites[i].texture.width, danishSprites[i].texture.height);
            languageDependant[i].GetComponent<Image>().sprite = danishSprites[i];
        }
        PermanentDataContainer.instance.english = false;
        //danish.GetComponent<ChangeColor>().SetActive();
        //english.GetComponent<ChangeColor>().SetInActive();
    }

    public void LanguageEnglish()
    {
        for (int i = 0; i < languageDependant.Count; i++)
        {
            languageDependant[i].GetComponent<RectTransform>().sizeDelta = new Vector2(englishSprites[i].texture.width, englishSprites[i].texture.height);
            languageDependant[i].GetComponent<Image>().sprite = englishSprites[i];
        }
        PermanentDataContainer.instance.english = true;

    }

    public void ActivateMenuFade(int menu)
    {
        AnimationEvent animEvent = new AnimationEvent();
        animEvent.time = 0.9f;
        animEvent.intParameter = menu;
        animEvent.functionName = "ActivateMenu";
        fadeScript.animator.runtimeAnimatorController.animationClips[1].AddEvent(animEvent);
        fadeScript.FadeOutIn();
        PermanentDataContainer.instance.SaveSettings();
    }

    public void ActivateMenu(int menu)
    {
        //called
        for (int i = 0; i < menus.Length; i++)
        {
            menus[i].SetActive(false);
        }
        menus[menu].SetActive(true);
        fadeScript.animator.runtimeAnimatorController.animationClips[1].events = new AnimationEvent[0];
        PermanentDataContainer.instance.SaveSettings();
    }

    private void OnGUI()
    {
        //if (settingsContainer.vibrate)
        {
            //Handheld.Vibrate();
        }
    }

    //public void toggleVibrate()
    //{
    //    if (settingsContainer.vibrate)
    //    {
    //        settingsContainer.vibrate = false;
    //    }
    //    else
    //    {
    //        settingsContainer.vibrate = true;
    //    }
    //}

    public void toggleCamShake()
    {
        if (settingsContainer.cameraShake)
        {
            settingsContainer.cameraShake = false;
        }
        else
        {
            settingsContainer.cameraShake = true;
        }
    }
    public void toggleNGP()
    {
        if (settingsContainer.newGamePlus)
        {
            settingsContainer.newGamePlus = false;
        }
        else
        {
            settingsContainer.newGamePlus = true;
        }
    }

    public void LoadScene1(int higherNumber)
    {
        SceneManager.LoadScene(higherNumber);
    }

    public void TempStartGame()
    {
        /*
        int higherNumber = Mathf.Max(PermanentSettingsContainer.settings.levelProgress, SceneManager.GetActiveScene().buildIndex + 1);
        fadeScript.animator.runtimeAnimatorController.animationClips[1].events = new AnimationEvent[0];
        AnimationEvent animEvent = new AnimationEvent();
        animEvent.time = 1.0f;
        animEvent.intParameter = higherNumber;
        animEvent.functionName = "LoadScene1";
        fadeScript.animator.runtimeAnimatorController.animationClips[1].AddEvent(animEvent);
         */
        fadeScript.FadeOut();
        StartCoroutine(LoadingScreen());
        //SceneManager.LoadScene(higherNumber);
    }
    IEnumerator LoadingScreen()
    {
        bool loading = true;
        bool increase = true;
        float val = 0f;
        loadingObject.SetActive(true);
        while (loading)
        {
            if(increase && val < 1f)
            {
                val += Time.deltaTime;
                if(val >= 0.9f)
                {
                    increase = false;
                }
            }else if(!increase && val > 0f)
            {
                val -= Time.deltaTime;
                if (val <= 0.1f)
                {
                    increase = true;
                }
            }
            ChangeTransparency(head, val);
            yield return null;
        }

        
    }

    public void SelectCamera(GameObject camera)
    {
        cameraMenu.SetActive(false);
        cameraAbout.SetActive(false);
        cameraSettings.SetActive(false);
        camera.SetActive(true);
    }

    public Transform ChangeTransparency(Transform target, float val)
    {
        var tempColor = target.GetComponent<Image>().color;
        tempColor.a = val;
        target.GetComponent<Image>().color = tempColor;
        return target;
    }


}
