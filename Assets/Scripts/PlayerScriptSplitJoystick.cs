﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



[RequireComponent(typeof(TrailRenderer))]
public class PlayerScriptSplitJoystick : MonoBehaviour
{
    public delegate void EnemyHit();
    public static event EnemyHit hit;
    private EventManager eventManager;

    public delegate void LoseCombo();
    public static event LoseCombo loseCombo;
    [HideInInspector]
    private int screenWidth;
    [HideInInspector]
    private int screenHeigth;

    [Header("Movement Stuff")]
    public float startSpeed = 5;
    private float indicatorSpeed = 5;
    public float speedWhileCharging = 2.5f;
    public float dashSpeed = 30;
    [Tooltip("How long should the character be dashing")]
    public float dashDuration = 0.2f;
    private float curDashTime = 0;
    public int maxDashCharges = 2;
    private int dashCharges;
    [Tooltip("How long the dash-cooldown should be")]
    public float dashCooldown = 1;
    private float cooldown;
    private Text cooldownText;
    [HideInInspector]
    public bool dashing;
    public ParticleSystem dashEffect;
    [Tooltip("Inivibility time measured in seconds")]
    public float invisibilityTime = 0.5f;

    public float upwardAnddownwardModifier;
    public float joystickScreenBorder = 20;
    public float fingerMaxDistance = 30;

    private bool leftFirstTouch;
    private bool rightTouched;
    private bool leftTouched;
    private Vector3 dashVelocity;

    public Transform dirIndicator;
    public Transform chargeIndicator;
    private Rigidbody rb;
    private Transform circle;
    private Transform outerCircle;
    private Transform circle2;
    private Transform outerCircle2;
    private Animator animator;

    private float speed;
    private bool startTouch;
    private Vector2 pointA;
    private Vector2 pointB;
    private Vector2 pointADash;
    private Vector2 pointBDash;

    [Header("Dash Stuff")]
    [Tooltip("A number between 0 and 1 specifying the size of area in the middle of the joystick where the input is not registered.")]
    public float joystickDeadzone = 0.2f;
    [Tooltip("The time the user has to let go of the screen in order for it to be counted as a tap")]
    public float tapTime = 0.2f;
    [Tooltip("The time inbetween each charge upgrade")]
    public float chargeTime = 0.5f;
    private float timeleft;
    private float dashChargeTime;
    private bool chargingDash;
    private bool rightHolding;
    public int chargeMultiplier = 1;
    public GameObject dashMeter;
    public Material dashMeterChargeArcMat;

    [Header("dash winddown variables")]
    [Tooltip("initial speed for winddown")] public float windDownInitSpeed = 8;
    [Tooltip("duration of first part of winddown")] public float windDownInitToMidTime = 0.2f;
    [Tooltip("should first part of winddown be able to kill enemies")] public bool windDownInitToMidDashing = false;
    [Tooltip("middle speed of winddown")] public float windDownMiddleSpeed = 2;
    [Tooltip("duration of second part of winddown")] public float windDownMidToEndTime = 0.3f;
    [Tooltip("should second part of winddown be able to kill enemies?")] public bool windDownMidToEndDashing = false;
    [Tooltip("end speed of winddown")] public float windDownEndSpeed = 5;
    private float dashWindDownTimer = 0;


    [Header("Trail Renderer Stuff")]
    [Tooltip("This is the material for the trail, defining its color")]
    public Material trailMaterial;
    [Tooltip("The fade time of the trail")]
    public float trailTime = .5f;
    [Tooltip("The width of the trails head")]
    public float trailStartWidth = 1;
    [Tooltip("The width of the trails tail")]
    public float trailEndWidth = 0.5f;
    private TrailRenderer trail;

    [Header("Time Slow Stuff")]
    public AnimationCurve timeSlowCurve;
    public float numberOfTimeScaleUpdates;
    private float timeScaleUpdateCounter;
    public float timeSlowCurveDuration;

    [Header("Health Stuff")]
    public GameObject[] healthArray;
    private GameObject healthContainer;
    public int healthInt;

    [Header("Second Wind Stuff")]
    public State secondWindState;
    private List<State> enemiesPrevState;
    public GameObject safeSpace;
    private GameObject safeSpaceZone;
    private Image secondWindUI;
    private bool savedBySecondWind;
    public float secondWindDuration = 3;
    private float secondWindTimer;
    private bool curInSecondWind;

    private bool aboutToDieBacking = false;
    private bool aboutToDie {
        get {
            return aboutToDieBacking;
        }
        set {
            aboutToDieBacking = value;
            animator.SetBool("SecondWind", value);
        }
    }

    private int secondWindCharge = 10;
    public int requiredSecondWindCharges = 10;
    

    [Header("Screenshake Stuff")]
    public float screenshakeMult = 1.5f;
    public float screenshakeDuration = 0.35f;

    [Header("Camera Zoom stuff")]
    public Camera mainCamera;
    private Camera lineRenderCamera;
    public float farAwayZoom = 6;
    public float closeUpZoom = 4;
    private float standardZoom;

    float inputAngleOffset;

    private BoxCollider chargeHitBox;

    public bool invulnerable = false;

    void Start()
    {
        screenHeigth = Screen.height;
        screenWidth = Screen.width;

        animator.SetFloat("RunSpeed", startSpeed / 7);

        circle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Inner");
        outerCircle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Outer");
        cooldownText = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Cooldown text").GetComponent<Text>();
        healthContainer = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("HealthContainerForRun").gameObject;
        eventManager = GameObject.FindGameObjectWithTag("EventManager").transform.GetComponent<EventManager>();

        rb = gameObject.transform.GetComponent<Rigidbody>();
        
        dashCharges = maxDashCharges;
        speed = startSpeed;
        cooldown = dashCooldown;
        trail = GetComponent<TrailRenderer>();
        trail.material = trailMaterial;
        trail.startWidth = trailStartWidth;
        trail.endWidth = trailEndWidth;
        trail.time = trailTime;
        trail.enabled = false;

        healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }

        mainCamera = Camera.main;
        lineRenderCamera = mainCamera.transform.GetChild(0).GetComponent<Camera>();
        standardZoom = mainCamera.orthographicSize;

        inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, mainCamera.transform.forward, Vector3.up);
        secondWindUI = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("SecondWindUI").GetComponent<Image>();
        secondWindUI.gameObject.SetActive(false);
        secondWindTimer = 0;

        var mainCam = Camera.main;
        inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, mainCam.transform.forward, Vector3.up);

        animator = GetComponent<Animator>();

        //dashMeter = GameObject.Find("DashMeter");
        //dashMeterChargeArcMat = dashMeter.transform.GetChild(2).gameObject.GetComponent<Image>().material;

        chargeHitBox = GetComponent<BoxCollider>();
        chargeHitBox.enabled = false;
    }


    private void Awake()
    {
        //not sure this needs to be updated if it works in start in another scene
        dashMeter = GameObject.Find("DashMeter");
        dashMeterChargeArcMat = dashMeter.transform.Find("Bar").GetComponent<Image>().material;
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        Movement();
    }

    private void FixedUpdate()
    {
        invisibilityTime -= Time.deltaTime;

        if (curInSecondWind == true) // if the player is in second wind state
        {
            if (secondWindTimer > 0)
            {
                AkSoundEngine.SetRTPCValue("Second_Wind_Time", secondWindTimer);
                secondWindTimer -= Time.deltaTime;
                secondWindUI.fillAmount = secondWindTimer / secondWindDuration;
            }
        }
        else if (dashCharges < maxDashCharges) //reduce charge cooldown if not in secondwind
        {
            cooldown -= Time.deltaTime;

            if (cooldown <= 0)
            {
                dashCharges++;
                cooldown = dashCooldown;
                AkSoundEngine.PostEvent("Play_Dash_Recharge", gameObject);
            }
        }

        cooldownText.text = "Dash Charge: " + dashCharges.ToString() + "\n" + "Dash CD: " + cooldown.ToString("F2");
        dashMeterChargeArcMat.SetFloat("_Arcrange", (dashCharges * 180) + (dashCooldown - cooldown) / dashCooldown * 180);


        //Camera zoom debug
        if (Input.GetKey(KeyCode.Alpha1))
        {
            StartCoroutine(changeCameraPos(standardZoom));
        }
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            StartCoroutine(changeCameraPos(farAwayZoom));
        }
        else if (Input.GetKey(KeyCode.Alpha3))
        {
            StartCoroutine(changeCameraPos(closeUpZoom));
        }

        bool isRunning = false;
        if (startTouch && aboutToDie) // if the player is in second wind state
        {
            Vector2 offset = pointB - pointA;
            if (offset.magnitude > fingerMaxDistance) // adjust the inner circle in the joystick if the player is not in second wind state
            {
                offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
                pointA = pointB - offset;
                if (!aboutToDie)
                {
                    AdjustJoystickToBorder();
                }

            }

            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f); // the direction the joystick s pointed // move the joystick
            circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);

            if (direction.magnitude > joystickDeadzone && !dashing && leftTouched) // if the player is not dashing and not in the deadzone
            {
                var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                //Convert to vector 3 rotate 45 degrees to match camera rotation

                if (direction.x > 0.7f && direction.x < 1f && !rightTouched || direction.x < -0.7f && direction.x > -1f && !rightTouched) // calculate whether a modifier should be used on the speed if the player goes upwards or downwards
                {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                    if (chargingDash)
                    {
                        speed = speedWhileCharging;
                    }
                    velocity = new Vector3(direction.x, 0, direction.y) * (speed + upwardAnddownwardModifier);
                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                    dirIndicator.transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
                else if (!rightTouched)
                {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                    if (chargingDash)
                    {
                        speed = speedWhileCharging;
                    }
                    velocity = new Vector3(direction.x, 0, direction.y) * speed;
                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                    dirIndicator.transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
                if (rightTouched)
                {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                }
            }
        }
        if (startTouch && !aboutToDie) // if the player is touching the screen and is not in second wind state
        {
            Vector2 offset = pointB - pointA;
            if (offset.magnitude > fingerMaxDistance)
            {
                offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
                pointA = pointB - offset;
                if (!aboutToDie)
                {
                    AdjustJoystickToBorder();
                }

            }

            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);

            if (direction.magnitude > joystickDeadzone && !dashing && leftTouched)
            {
                var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                //Convert to vector 3 rotate 45 degrees to match camera rotation

                if (direction.x > 0.7f && direction.x < 1f && !rightTouched || direction.x < -0.7f && direction.x > -1f && !rightTouched)
                {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                    if (chargingDash) // slow the player while he is charging his dash
                    {
                        speed = speedWhileCharging;
                    }
                    velocity = new Vector3(direction.x, 0, direction.y) * (speed + upwardAnddownwardModifier); // add modifier for when the player moves upwards or downwards
                    if (!aboutToDie)
                    {
                        isRunning = true;
                        transform.Translate(velocity * Time.deltaTime, Space.World); // only move the player if he is not in secondwind state
                    }
                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                    dirIndicator.transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
                else if (!rightTouched)
                {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                    if (chargingDash)
                    {
                        speed = speedWhileCharging;
                    }
                    velocity = new Vector3(direction.x, 0, direction.y) * speed;
                    if (!aboutToDie)
                    {
                        isRunning = true;
                        transform.Translate(velocity * Time.deltaTime, Space.World);
                    }

                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                    dirIndicator.transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
                if (rightTouched)
                {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                }
            }
        }
        animator.SetBool("Running", isRunning);
    }

    public void Movement()
    {
        if (timeleft > 0 && rightHolding) //Are we tapping or hold down right finger?
        {
            timeleft -= Time.deltaTime;
        }

        if (timeleft <= 0 && rightHolding && !chargingDash && 0 < dashCharges) //Am i charging a dash?
        {
            chargingDash = true;
            chargeIndicator.gameObject.SetActive(true);
            dashChargeTime = chargeTime;
            StartCoroutine(IndicatorDash());
        }

        if (dashChargeTime > 0 && chargingDash && 0 < dashCharges) //Have we charged the dash enough to get a multiplier?
        {
            dashChargeTime -= Time.deltaTime;
        }

        if (dashChargeTime <= 0 && chargingDash && chargeMultiplier < dashCharges) //Assign dash multiplier
        {
            chargeMultiplier++;
            StartCoroutine(IndicatorDash());
        }
        if (Input.touchCount > 0 && aboutToDie)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(0).position.y < 900) // change this value to something like screenheight / somenumber
                {
                    leftTouched = true;
                    pointA = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);

                    circle.transform.position = pointA;
                    outerCircle.transform.position = pointA;

                    circle.gameObject.SetActive(true);
                    outerCircle.gameObject.SetActive(true);
                    dirIndicator.gameObject.SetActive(true);
                }

            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                startTouch = true;
                if (Input.GetTouch(0).position.y < 900 && leftTouched)
                {
                    pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                }

            }
            else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                startTouch = true;
                if (Input.GetTouch(0).position.y < 900 && leftTouched)
                {
                    pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                }

            }
            else
            {
                //startTouch = false;
                StartCoroutine(StopStartTouch(startTouch));
            }
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                leftTouched = false;
                startTouch = false;
                circle.gameObject.SetActive(false);
                outerCircle.gameObject.SetActive(false);
                dashCharges--;
                StartCoroutine(Dash());
                chargeIndicator.position = transform.position;
                chargeIndicator.gameObject.SetActive(false);
            }
        }

        if (Input.touchCount > 0)
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (Input.GetTouch(0).position.y < 900) // change this value to something like screenheight / somenumber
                    {
                        if (Input.GetTouch(0).position.x < screenWidth / 2) // if the first touch is on the left side
                        {
                            leftTouched = true;

                            pointA = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);

                            circle.transform.position = pointA;
                            outerCircle.transform.position = pointA;

                            AdjustJoystickToBorder();

                            circle.gameObject.SetActive(true);
                            outerCircle.gameObject.SetActive(true);
                            dirIndicator.gameObject.SetActive(true);
                        }

                        if (Input.GetTouch(0).position.x > screenWidth / 2) // if the first touch is on the right side of the screen
                        {
                            rightHolding = true;
                            rightTouched = true;
                            timeleft = tapTime;
                        }
                    }

                }
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    startTouch = true;
                    if (Input.GetTouch(0).position.y < 900 && leftTouched)
                    {
                        pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                    }

                }
                else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    startTouch = true;
                    if (Input.GetTouch(0).position.y < 900 && leftTouched)
                    {
                        pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                    }

                }
                else
                {
                    //startTouch = false;
                    StartCoroutine(StopStartTouch(startTouch));
                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    startTouch = false;
                    if (leftTouched)
                    {
                        leftTouched = false;
                        circle.gameObject.SetActive(false);
                        outerCircle.gameObject.SetActive(false);
                    }
                    else
                    {
                        rightHolding = false;
                        rightTouched = false;
                        if (dashCharges > 1 && chargeMultiplier > 1)// if the charge multiplier is above 1 means it is 2 therefore dash charges looses 2;
                        {
                            dashCharges--;
                            dashCharges--;
                            StartCoroutine(Dash());
                            chargeIndicator.position = transform.position;
                            chargeIndicator.gameObject.SetActive(false);
                        }
                        else if (dashCharges > 0)
                        {
                            dashCharges--;
                            StartCoroutine(Dash());
                            chargeIndicator.position = transform.position;
                            chargeIndicator.gameObject.SetActive(false);
                        }
                        else
                        {
                            chargingDash = false;
                            AkSoundEngine.PostEvent("Play_Dash_Denied", gameObject);
                        }
                        dirIndicator.gameObject.SetActive(false);
                    }
                }

                if (Input.touchCount > 1)
                {
                    if (Input.GetTouch(1).phase == TouchPhase.Began)
                    {
                        if (Input.GetTouch(1).position.y < 900) // change this value to something like screenheight / somenumber
                        {
                            if (Input.GetTouch(1).position.x < screenWidth / 2 && rightTouched) // if the second touch on the screen is on the left side and the rightside was the first touch
                            {
                                pointA = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);

                                circle.transform.position = pointA;
                                outerCircle.transform.position = pointA;

                                AdjustJoystickToBorder();

                                circle.gameObject.SetActive(true);
                                outerCircle.gameObject.SetActive(true);
                                dirIndicator.gameObject.SetActive(true);
                            }

                            if (Input.GetTouch(1).position.x > screenWidth / 2 && leftTouched) // if the second touch is on the right side of the screen and the left side was the first touched
                            {
                                rightHolding = true;
                                timeleft = tapTime;
                            }
                        }
                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Moved)
                    {
                        if (Input.GetTouch(1).position.y < 900 && rightTouched)
                        {
                            pointB = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Stationary)
                    {
                        if (Input.GetTouch(1).position.y < 900 && rightTouched)
                        {
                            pointB = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Ended)
                    {
                        if (rightTouched)
                        {
                            leftTouched = false;
                            circle.gameObject.SetActive(false);
                            outerCircle.gameObject.SetActive(false);
                        }
                        else
                        {
                            rightHolding = false;
                            rightTouched = false;
                            if (dashCharges > 1 && chargeMultiplier > 1)
                            {
                                dashCharges--;
                                dashCharges--;
                                StartCoroutine(Dash());
                                chargeIndicator.position = transform.position;
                                chargeIndicator.gameObject.SetActive(false);
                            }
                            else if (dashCharges > 0)
                            {
                                dashCharges--;
                                StartCoroutine(Dash());
                                chargeIndicator.position = transform.position;
                                chargeIndicator.gameObject.SetActive(false);
                            }
                            else
                            {
                                chargingDash = false;
                                AkSoundEngine.PostEvent("Play_Dash_Denied", gameObject);
                            }
                        }
                    }
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            if (!dashing)
            {
                TakeDamage();
            }

            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
            if (dashCharges < maxDashCharges)
            {
                dashCharges++;
            }
        }
        else if (other.gameObject.CompareTag("Shield"))
        {
            RaycastHit hit;
            Physics.Raycast(transform.position, transform.forward, out hit);
            Debug.Log(hit.normal.z);
            Debug.Log("bumped into shield");

            if (hit.normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject);
                    Debug.Log("dashed into shield");
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (other.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (aboutToDie)
                {
                    savedBySecondWind = true;
                    secondWindUI.gameObject.SetActive(false);
                    determineSecondWindSurvival();
                    StopCoroutine(secondWind());

                }

                if (hit != null)
                {
                    hit();
                }
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject);
                Debug.Log("Murder: " + other.gameObject.name);
                other.gameObject.GetComponent<StateController>().gotHit();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                if (dashCharges < maxDashCharges)
                {
                    StartCoroutine(SlowTimeNew());
                    dashCharges++;
                }
                secondWindCharge++;

            }
       
        }
        else if (other.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structure_Impact", gameObject);
                StopDash();
            }
        }
        else if (other.gameObject.CompareTag("Hazard"))
        {
            if (!dashing)
            {
                TakeDamage();
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {

            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject);
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (aboutToDie)
                {
                    savedBySecondWind = true;
                    secondWindUI.gameObject.SetActive(false);
                    determineSecondWindSurvival();
                    StopCoroutine(secondWind());
                }

                if (hit != null)
                {
                    hit();
                }
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject);
                collision.gameObject.GetComponent<StateController>().gotHit();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                if (dashCharges < maxDashCharges)
                {
                    StartCoroutine(SlowTimeNew());
                    dashCharges++;
                }
                secondWindCharge++;

            }
        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structure_Impact", gameObject);
                StopDash();
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {
            if (collision.contacts[0].normal.z < 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject); // test to see if the sound would keep playing
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (aboutToDie)
                {
                    savedBySecondWind = true;
                    secondWindUI.gameObject.SetActive(false);
                    
                    determineSecondWindSurvival();
                    StopCoroutine(secondWind());
                }

                //if (hit != null)
                //{
                //    hit();
                //}
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject); // test to see if the sound would keep playing
                collision.gameObject.GetComponent<StateController>().gotHit();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                if (dashCharges < maxDashCharges)
                {
                    dashCharges++;
                }
                secondWindCharge++;

            }

        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structures_Impact", gameObject);
                StopDash();
            }
        }
    }

    public void TakeDamage()
    {
        if (!invulnerable)
        {
            if (invisibilityTime <= 0.0f && aboutToDie == false)
            {
                if (healthInt > 0)
                {
                    if (loseCombo != null)
                    {
                        loseCombo();
                    }
                    healthArray[healthInt - 1].SetActive(false);
                    healthInt--;
                    AkSoundEngine.PostEvent("Play_Hit_Hero", gameObject);
                }

                if (healthInt <= 0)
                {
                    aboutToDie = true;
                    StartCoroutine(secondWind());
                }
                invisibilityTime = 0.5f;
            }
        }
    }

    public IEnumerator secondWind()
    {

        if (secondWindCharge >= requiredSecondWindCharges)
        {
            AkSoundEngine.PostEvent("Play_Second_Wind_Activated", gameObject);
            savedBySecondWind = false;
            enemiesPrevState = new List<State>();
            maxDashCharges = 1;
            dashCharges = 1;

            for (int i = 0; i < eventManager.Enemies.Count / 2; i++) //Change if the eventmanager enemy count is fixed.
            {
                enemiesPrevState.Add(eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState);
                eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState = secondWindState;
                Debug.Log("Told following enemy to stop: " + eventManager.Enemies[i]);
            }

            rb.isKinematic = true;
            safeSpaceZone = Instantiate(safeSpace, this.transform, false);
            float progress = 0;

            while (progress < 1)
            {
                safeSpaceZone.transform.localScale = Vector3.Lerp(safeSpaceZone.transform.localScale, new Vector3(3, 3, 3), progress);
                progress += Time.deltaTime;
                yield return null;
            }

            curInSecondWind = true;
            secondWindUI.gameObject.SetActive(true);
            secondWindTimer = secondWindDuration;
            Destroy(safeSpaceZone);
            rb.isKinematic = false;

            yield return new WaitForSeconds(secondWindDuration);//shouldnt this coroutine be stopped if determineSecondWindSurvival is triggered by a collision?

            determineSecondWindSurvival();
            secondWindCharge -= requiredSecondWindCharges;
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }
    public void determineSecondWindSurvival()
    {
        if (savedBySecondWind == true)
        {
            AkSoundEngine.PostEvent("Play_Second_Wind_Respawn", gameObject);
            rb.isKinematic = false;
            aboutToDie = false;
            maxDashCharges = 2;

            for (int i = 0; i < eventManager.Enemies.Count / 2; i++)
            {
                eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState = enemiesPrevState[i];
            }

            curInSecondWind = false;

            healthInt = 2;
            healthArray[0].SetActive(true);
            healthArray[1].SetActive(true);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            startTouch = false;
            AkSoundEngine.PostEvent("Play_Death_Hero", gameObject);
        }
    }

    public IEnumerator SlowTimeNew()
    {
        while (timeScaleUpdateCounter < numberOfTimeScaleUpdates)
        {
            Time.timeScale = timeSlowCurve.Evaluate(timeScaleUpdateCounter / numberOfTimeScaleUpdates);
            timeScaleUpdateCounter++;
            yield return new WaitForSecondsRealtime(timeSlowCurveDuration / numberOfTimeScaleUpdates);
        }
        Time.timeScale = 1;
    }

    public IEnumerator SlowTime(float timeSlowDegree, float timeSlowDuration)
    {
        Time.timeScale = timeSlowDegree;
        yield return new WaitForSecondsRealtime(timeSlowDuration);
        Time.timeScale = 1;
    }

    public IEnumerator StopStartTouch(bool stop)
    {
        yield return new WaitForSeconds(0.1f);
        stop = false;
    }

    void AdjustJoystickToBorder()
    {
        pointA.x = Mathf.Max(pointA.x, joystickScreenBorder);
        pointA.x = Mathf.Min(pointA.x, screenWidth - joystickScreenBorder);
        pointA.y = Mathf.Max(pointA.y, joystickScreenBorder);
        pointA.y = Mathf.Min(pointA.y, screenHeigth - joystickScreenBorder);

        outerCircle.transform.position = pointA;
    }
    /// <summary>
    /// default size is (2,1,1) as of 12/11 15:26
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    public void setChargeHitBoxSize(float x, float y, float z)
    {
        chargeHitBox.size = new Vector3(x, y, z);
    }

    public void resetChargeHitBoxSize()
    {
        chargeHitBox.size = new Vector3(2,1,1);
    }

    public IEnumerator Dash() //dash function
    {
        if (!dashing)
        {
            chargingDash = false;
            //chargeHitBox.SetActive(true);

            dashEffect.Play();
            curDashTime = 0;
            speed = dashSpeed;
            dashing = true;
            trail.enabled = true;
            animator.SetTrigger("Dash");
            AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);
            while (curDashTime < dashDuration * chargeMultiplier)
            {
                curDashTime += Time.deltaTime;
                transform.Translate(dashVelocity * Time.deltaTime * speed, Space.World);
                transform.rotation = Quaternion.LookRotation(dashVelocity, Vector3.up);
                yield return null;
            }
            chargeMultiplier = 1;
            StopDash();
        }
    }

    public IEnumerator IndicatorDash()
    {
        float curIndiDashTime = 0;
        indicatorSpeed = dashSpeed;
        while (curIndiDashTime < dashDuration)
        {
            curIndiDashTime += Time.deltaTime;
            chargeIndicator.transform.Translate(dashVelocity * Time.deltaTime * indicatorSpeed, Space.World);
            yield return null;
        }
    }

    public IEnumerator DashWindDown()
    {
        if (windDownInitToMidDashing) //first part of dash winddown
        {
            dashing = true;
        }
        else
        {
            dashing = false;
        }
        while (windDownInitToMidTime > dashWindDownTimer)//winding down from init speed
        {
            speed = Mathf.Lerp(windDownInitSpeed, windDownMiddleSpeed, dashWindDownTimer / windDownInitToMidTime);
            yield return null;
            dashWindDownTimer += Time.deltaTime;
        }
        dashWindDownTimer = 0;
        if (windDownMidToEndDashing) //second part of dash winddown 
        {
            dashing = true;
        }
        else
        {
            dashing = false;
        }
        while (windDownMidToEndTime < dashWindDownTimer)//winding up from min speed
        {
            speed = Mathf.Lerp(windDownMiddleSpeed, windDownEndSpeed, dashWindDownTimer / windDownMidToEndTime);
            yield return null;
            dashWindDownTimer += Time.deltaTime;
        }
        speed = startSpeed;
    }

    public void StopDash()
    {
        dashWindDownTimer = 0;
        speed = startSpeed;
        dashing = false;
        trail.enabled = false;
        chargeHitBox.enabled= false;
        StartCoroutine(DashWindDown());
        if (curInSecondWind && !savedBySecondWind) //if youre in secondwind and you havnt hit anything by the end of stopdash you die
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public IEnumerator changeCameraPos(float distanceFromPlayer)
    {
        float progress = 0;
        var tempMCSize = mainCamera.orthographicSize;
        var tempLRCSize = lineRenderCamera.orthographicSize;

        Debug.Log("Changing camera position to: " + distanceFromPlayer);

        while (progress < 1)
        {
            mainCamera.orthographicSize = Mathf.Lerp(tempMCSize, distanceFromPlayer, progress);
            lineRenderCamera.orthographicSize = Mathf.Lerp(tempLRCSize, distanceFromPlayer, progress);
            yield return null;
        }
    }



}

