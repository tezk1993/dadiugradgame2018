﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeController : MonoBehaviour {

    public Slider master, music, sfx;
    private float convertedMasterValue, convertedMusicValue, convertedSFXValue;

    private void Awake()
    {
        master.onValueChanged.AddListener(delegate { MasterValueChange(); });
        music.onValueChanged.AddListener(delegate { MusicValueChange(); });
        sfx.onValueChanged.AddListener(delegate { sfxValueChange(); });
    }

    private void OnEnable()
    {
        master.value = PermanentDataContainer.instance.convertedMasterValue / 100;
        music.value = PermanentDataContainer.instance.convertedMusicValue / 100;
        sfx.value = PermanentDataContainer.instance.convertedSFXValue / 100;
    }
    public void MasterValueChange()
    {
        //Debug.Log(master.value);
        AkSoundEngine.SetRTPCValue("Master_Volume", master.value * 100);
        convertedMasterValue = Mathf.FloorToInt(master.value * 100);
    }
    public void MusicValueChange()
    {
        //Debug.Log(music.value);
        AkSoundEngine.SetRTPCValue("Music_Volume", music.value * 100);
        convertedMusicValue = Mathf.FloorToInt(music.value * 100);
    }
    public void sfxValueChange()
    {
        //Debug.Log(sfx.value);
        AkSoundEngine.SetRTPCValue("Sound_Volume", sfx.value * 100);
        convertedSFXValue = Mathf.FloorToInt(sfx.value * 100);
    }

    public void UpdatePermSettings()
    {
        Debug.Log(PermanentDataContainer.instance.convertedMasterValue);
        PermanentDataContainer.instance.convertedMasterValue = convertedMasterValue;
        Debug.Log(PermanentDataContainer.instance.convertedMasterValue);
        PermanentDataContainer.instance.convertedMusicValue = convertedMusicValue;
        PermanentDataContainer.instance.convertedSFXValue = convertedSFXValue;
    }
}
