﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InputBase : MonoBehaviour
{

    public delegate void Dash(Vector2 dir);
    public event Dash dash;

    public void InvokeDash(Vector2 dir)
    {
        if (dash != null) dash(dir);
    }

    //public abstract float GetHorizontal();
    //public abstract float GetVertical();
    public abstract Vector2 Direction();
    public abstract Vector2 DashDirection();
    public abstract bool IsTouching();
    public abstract bool IsCharging();

}
