﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepScript : MonoBehaviour {

    public GameObject Container;


    Animator anim;

    public Transform leftFootpos;
    public Transform rightFootpos;

    public GameObject waterRipplePrefab;

    public int rippleAmount;
    public List<GameObject> waterRipplesPool;

    private void Start()
    {
        for (int i = 0; i < rippleAmount; i++)
        {
            GameObject waterRipple = GameObject.Instantiate<GameObject>(waterRipplePrefab);
            waterRipple.name = "Water ripple" + i.ToString();
            waterRipple.transform.parent = Container.transform;
            waterRipple.SetActive(false);
            waterRipplesPool.Add(waterRipple);

        }
        anim = gameObject.GetComponent<Animator>();
    }


    public GameObject GetPooledObject()
    {
        for (int i = 0; i < waterRipplesPool.Count; i++)
        {
            if (!waterRipplesPool[i].activeInHierarchy)
            {
                return waterRipplesPool[i];
            }

        }
        return null;
    }

    void StepLeft()
    {
        if (GetPooledObject())
        {
            AkSoundEngine.PostEvent("Play_Locomotion_Hero", gameObject);
            var unpooledRipple = GetPooledObject();
            unpooledRipple.transform.position = leftFootpos.transform.position;
            unpooledRipple.transform.parent = Container.transform;
            unpooledRipple.transform.GetChild(0).transform.position = unpooledRipple.transform.position;
            unpooledRipple.SetActive(true);
        }
    }

    void StepRight()
    {
        if (GetPooledObject())
        {
            AkSoundEngine.PostEvent("Play_Locomotion_Hero", gameObject);
            var unpooledRipple = GetPooledObject();
            unpooledRipple.transform.position = rightFootpos.transform.position;
            unpooledRipple.transform.parent = Container.transform;

            unpooledRipple.transform.GetChild(0).transform.position = unpooledRipple.transform.position;
            unpooledRipple.SetActive(true);
        }
    }
}
