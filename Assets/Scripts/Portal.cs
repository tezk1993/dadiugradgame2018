﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
    [Header("Create Two Teleporters and link the other below")]
    public GameObject linkedPortal;
    [HideInInspector]
    public bool isReady;
    [Tooltip("Amount of time before the teleporter can be used again, if it is too low the player will teleport back and forth")]
    public float cooldownTimeAfterUse;
    private bool isLinked;

	// Use this for initialization
	void Start () {

        cooldownTimeAfterUse = 3.0f;

        if (linkedPortal == null)
        {
            isLinked = false;
        }
        else
        {
            isLinked = true;
            isReady = true;
            linkedPortal.GetComponent<Portal>().isReady = true;
        }  
	}
	
	// Update is called once per frame
	void Update () {
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Player")
        {
            if (isLinked)
            {
                if (isReady)
                {
                    isReady = false;
                    linkedPortal.GetComponent<Portal>().isReady = false;

                    StartCoroutine(MakeActiveAgain());
                    AkSoundEngine.PostEvent("Play_Portal", gameObject);
                    StartCoroutine(Teleport(other, 0.1f));
                }
            }
        }   
    }

    IEnumerator MakeActiveAgain()
    {
        yield return new WaitForSeconds(cooldownTimeAfterUse);
        isReady = true;
        linkedPortal.GetComponent<Portal>().isReady = true;
        //StartCoroutine(UpdateLinkedReady());
    }

    IEnumerator Teleport(Collider other, float time)
    {
        yield return new WaitForSeconds(time);
        other.transform.position = linkedPortal.transform.position;
        
        //Debug.Log("teleported");
    }
}
