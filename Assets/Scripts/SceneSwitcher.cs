﻿/**
 * SceneSwitcher.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    
    #region Singleton
    private static SceneSwitcher instanceBacking;
    public static SceneSwitcher instance
    {
        get
        {
            if (instanceBacking == null)
            {
                //var gameObject = new GameObject("Scene Switcher", typeof(SceneSwitcher));
                //instanceBacking = gameObject.GetComponent<SceneSwitcher>();
                instanceBacking = FindObjectOfType<SceneSwitcher>();
            }
            return instanceBacking;
        }
    }
    #endregion
    

    #region Variables

    private bool loading;

    #endregion

    #region Unity Methods

    public void Start()
    {
        loading = false;
    }
    #endregion

    #region Methods

    
    public void Awake()
    {
        if (instanceBacking == null)
        {
            instanceBacking = this;
        }
        else if (instanceBacking != this) // If the instance is already assigned then destroy this one to preseve the singleton property.
        {
            Debug.LogWarning("There shouldn't be an other scene switcher instance");
            return;

            var allComponents = gameObject.GetComponents<Component>();
            // If this is the only script on the game object then
            // said game object has two components: a Transform and this script
            bool noOtherScriptsOnObject = allComponents.Length == 2;
            Destroy(this);
            if (noOtherScriptsOnObject)
            {
                Destroy(gameObject);
            }
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    

    public static void ReloadAllInScene()
    {
        MakePersistent.DestroyAllPersistent();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadSceneAsyc(string nextSceneName)
    {
        int buildIndex = SceneManager.GetSceneByName(nextSceneName).buildIndex;
        StartCoroutine(SceneLoadAsyncCoroutine(buildIndex));
    }

    IEnumerator SceneLoadAsyncCoroutine(int buildIndex)
    {
        loading = true;
        try {
            // Set the current Scene to be able to unload it later
            Scene currentScene = SceneManager.GetActiveScene();

            // The Application loads the Scene in the background at the same time as the current Scene.
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(buildIndex, LoadSceneMode.Additive);
            asyncLoad.allowSceneActivation = true;
            Debug.Log("--2");

            // Wait until the last operation fully loads
            // for an explanation to why it is checked against 0.9, see:
            // https://docs.unity3d.com/ScriptReference/AsyncOperation-progress.html
            //while (asyncLoad.progress < 0.9f)
            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            Debug.Log("--3");
            Scene targetScene = SceneManager.GetSceneByBuildIndex(buildIndex);
            Debug.Log("--4");
            // Unload the previous Scene
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(currentScene);
            // activate the loaded scene
        } finally {
            loading = false;
        }
    }

    public void StartLevelLoadBuildIndex(int buildindex)
    {
        StartCoroutine(LoadLevelViaBuildIndex(buildindex));
    }

    IEnumerator LoadLevelViaBuildIndex(int buildindex)
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(buildindex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadNextSceneAsync()
    {
        if (loading)
        {
            throw new UnityException("Scene load requested while scene is already being loaded.");
        }
        int activeSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
        StartCoroutine(SceneLoadAsyncCoroutine(activeSceneBuildIndex + 1));
    }
    #endregion
}
