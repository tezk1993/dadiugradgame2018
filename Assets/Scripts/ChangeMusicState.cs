﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusicState : MonoBehaviour {

    private static int safeCounter = 0;

    public void SetMusicStateBattle()
    {
        AkSoundEngine.SetState("Music_State", "Battle");
    }

    public void SetMusicStateBoss()
    {
        AkSoundEngine.SetState("Music_State", "Boss");

        AkSoundEngine.SetState("Boss_Phases", "Intro");

    }

    public void SetMusicStateSafe()
    {
        safeCounter++;
        AkSoundEngine.SetRTPCValue("Safe_Zone_Progress", safeCounter);

        AkSoundEngine.SetState("Music_State", "Safe");

    }

    public void PlayPhaseChange()
    {

        AkSoundEngine.PostEvent("Play_Phase_Change", gameObject);
    }
}
