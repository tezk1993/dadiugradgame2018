﻿/**
 * MotionMatching.cs
 * Created by: Artur Barnabas
 * Created on: 12/10/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

/// Using linq to get the first frame from the database
using System.Linq;


class TrajectoryVisualizer
{
    private List<GameObject> trajectoryPoints;

    public TrajectoryVisualizer(Transform playerTransform, Color color) 
    {
        trajectoryPoints = new List<GameObject>();
        for (int i = 0; i < MotionMatchingDatabase.TRAJECTORY_POS_COUNT * 2; i++)
        {
            var newObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            newObject.transform.parent = playerTransform;
            newObject.transform.localScale = Vector3.one * 0.15f;
            newObject.GetComponent<Renderer>().material.color = color;
            trajectoryPoints.Add(newObject);
        }
    }

    public void UpdatePoints(IEnumerable<Vector3> past, IEnumerable<Vector3> future)
    {
        int index = 0;
        foreach (var point in past)
        {
            trajectoryPoints[index].transform.localPosition = point;
            index++;
        }
        foreach (var point in future)
        {
            trajectoryPoints[index].transform.localPosition = point;
            index++;
        }
    }
}

[RequireComponent(typeof(Rigidbody))]
public class MotionMatching : MonoBehaviour
{
    #region Variables

    [Range(0, 1)]
    public float poseVsTrajectory = 0.7f;

    public bool debugDisplay = false;

    [HideInInspector]
    public Vector3 targetVelocity;

    private Vector3 prevTargetVel;

    // There are two playback animators between which we interpolate to match
    // the requested trajectory.
    Animator[] playbackAnimators;
    Animator meshAnimator;

    int currPlaybackAnimatorId = 0;
    Animator currPlaybackAnimator { get { return playbackAnimators[currPlaybackAnimatorId]; } }
    Animator backgroundPlaybackAnimator { get { return playbackAnimators[currPlaybackAnimatorId ^ 1]; } }

    MotionMatchingDatabase database;

    const float TRAJECTORY_SECONDS = MotionMatchingDatabase.TRAJECTORY_SECONDS;

    const int TRAJECTORY_SEARCH_ITERATION_COUNT = 3;

    const int trajectoryUpdateFramerate = 60;
    const float UPDATE_DELTA_TIME = 1 / (float)trajectoryUpdateFramerate;

    const float TRANSITION_TIME = 0.25f;
    const float NEW_TRANSITION_DELAY = 0.5f;

    const float TRAJECTORY_CURVATURE = 0.03f;

    // The target velocity is simply ignored if it changes faster than a certain speed.
    // The rational for this behaviour is to prevent the situation where the user moves the
    // stick to the other direction to make a sharp 180 turn but the clip is matched at the middle
    // which causes the character to play a stop animation instead of the sharp turn animation.
    const float VELOCITY_CHANGE_IGNORE_TRESHOLD = 0.1f;

    int lastFrameId;

    float currSpeed;

    MotionState currMotionState;

    int lastTrajectoryFrameId;

    FrameId currClosestStateId;

    Vector3[] pastPositionsWorld;
    
    TrajectoryVisualizer playerTrajectoryVisualizer;
    TrajectoryVisualizer candidateTrajectoryVisualizer;

    float transitionStartTime;
    
    HumanBodyBones[] allBones = (HumanBodyBones[])Enum.GetValues(typeof(HumanBodyBones));

    Rigidbody rigidbody;

    #endregion

    #region Unity Methods

    public void Awake()
    {
        var debugObjects = new GameObject();
        debugObjects.transform.parent = transform;
        playerTrajectoryVisualizer = new TrajectoryVisualizer(debugObjects.transform, Color.white);
        candidateTrajectoryVisualizer = new TrajectoryVisualizer(debugObjects.transform, Color.green);

        if (debugDisplay == false)
        {
            debugObjects.SetActive(false);
        }

        int pastTrajectorySampleCount = (int)(MotionMatchingDatabase.TRAJECTORY_SECONDS * trajectoryUpdateFramerate);
        pastPositionsWorld = new Vector3[pastTrajectorySampleCount];

        currMotionState = new MotionState();
        currMotionState.futurePositions = new Vector3[MotionMatchingDatabase.TRAJECTORY_POS_COUNT];
        currMotionState.pastPositions = new Vector3[MotionMatchingDatabase.TRAJECTORY_POS_COUNT];
        
        currClosestStateId = new FrameId {stateName = "", frameNumber = 31};        
    }

    public void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        var animators = GetComponentsInChildren<Animator>();
        playbackAnimators = new Animator[animators.Length - 1];
        for (int i = 0; i < playbackAnimators.Length; i++)
        {
            playbackAnimators[i] = animators[i];
        }
        currPlaybackAnimatorId = 0;
        meshAnimator = animators[animators.Length - 1];
        database = UnityEngine.Object.FindObjectOfType<MotionMatchingDatabase>();

        if (database.rebuildDatabase)
        {
            database.RebuildDatabase(currPlaybackAnimator);
        }
        
        // DEBUG
        currPlaybackAnimator.speed = 1;

        // RESET character position and rotation
        currClosestStateId = database.frames.Keys.First();
        database.PlayClipFromFrame(currPlaybackAnimator, currClosestStateId);
        var hipTransform = currPlaybackAnimator.GetBoneTransform(HumanBodyBones.Hips);
        hipTransform.localPosition = new Vector3(0, hipTransform.position.y, 0);
        hipTransform.localRotation = Quaternion.identity;
        currPlaybackAnimator.Update(1);
        currPlaybackAnimator.applyRootMotion = false;
        currPlaybackAnimator.transform.localPosition = Vector3.zero;
        currPlaybackAnimator.transform.localRotation = Quaternion.identity;
    }

    public void Update()
    {
        UpdateMotionState();

        // =====================================================================
        // Find closest motion state from database.
        FrameId closest = currClosestStateId;
        float shortestDist = MotionStateDistance(
            database.frames[currClosestStateId].state, currMotionState
        );
        for (int iter = 0; iter < TRAJECTORY_SEARCH_ITERATION_COUNT; iter++)
        {
            foreach (var neighbourId in database.frames[currClosestStateId].similarFrames)
            {
                var neighbour = database.frames[neighbourId].state;
                float distance = MotionStateDistance(neighbour, currMotionState);
                if (distance < shortestDist)
                {
                    closest = neighbourId;
                    shortestDist = distance;
                }
            }
            currClosestStateId = closest;
        }

        float velChangeSpeed = (targetVelocity - prevTargetVel).magnitude / Time.deltaTime;
        prevTargetVel = targetVelocity;
        float transitionElapsed = Time.time - transitionStartTime;
        if (transitionElapsed > NEW_TRANSITION_DELAY && velChangeSpeed < VELOCITY_CHANGE_IGNORE_TRESHOLD)
        {
            var closestDesc = database.frames[currClosestStateId].state;
            Debug.Assert(closestDesc.pastPositions.Length == MotionMatchingDatabase.TRAJECTORY_POS_COUNT);
            candidateTrajectoryVisualizer.UpdatePoints(
                closestDesc.pastPositions,
                closestDesc.futurePositions
            );
            // Start playing that clip from the next frame after the closest pose and start
            var nextFrameId = currClosestStateId;
            nextFrameId.frameNumber += 1;
            ExchangeAnimators();
            database.PlayClipFromFrame(currPlaybackAnimator, nextFrameId);
            transitionStartTime = Time.time;
        }
        // =====================================================================

        //////////////////////////////////////////////////////////////////////////////////
        // DEBUG
        FrameId newFrameId = currClosestStateId;
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            newFrameId.frameNumber -= 1;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            newFrameId.frameNumber += 1;
        }
        
        float normalizedTime = database.FrameIdToNormalizedTime(newFrameId);
        if (normalizedTime > 0.7)
        {
            newFrameId.frameNumber = 31;
            normalizedTime = database.FrameIdToNormalizedTime(newFrameId);
        }
        currPlaybackAnimator.applyRootMotion = false;
        //animator.Play(newFrameId.stateName, -1, normalizedTime);
        //currClosestPoseId = newFrameId;
        //////////////////////////////////////////////////////////////////////////////////

        ApplyPoseToMesh();
    }

    #endregion

    #region Methods

    float MotionStateDistance(MotionState stateA, MotionState stateB)
    {
        return MotionMatchingDatabase.MotionStateDistance(stateA, stateB, poseVsTrajectory, 3);
    }

    void ExchangeAnimators()
    {
        var currStateInfo = currPlaybackAnimator.GetCurrentAnimatorStateInfo(0);
        var currClipNameHash = currStateInfo.fullPathHash;
        var currClipNormalizedTime = currStateInfo.normalizedTime;

        backgroundPlaybackAnimator.Play(currClipNameHash, -1, currClipNormalizedTime);

        currPlaybackAnimatorId ^= 1; // swap animators
    }

    void UpdateMotionState()
    {
        // =====================================================================
        // Update trajectories
        Vector3 velocity = currSpeed * transform.forward;
        Vector3 prevSegmentPos = transform.position;
        prevSegmentPos.y = 0;
        List<Vector3> futurePositions = new List<Vector3>();
        for (float elapsedTime = 0; elapsedTime < TRAJECTORY_SECONDS; elapsedTime += UPDATE_DELTA_TIME)
        {
            velocity = Vector3.Lerp(velocity, targetVelocity, TRAJECTORY_CURVATURE);
            var segmentPos = prevSegmentPos + velocity * UPDATE_DELTA_TIME;
            futurePositions.Add(segmentPos);
            prevSegmentPos = segmentPos;
        }
        var worldToBody = MotionMatchingDatabase.GetWorldToBodyTransform(meshAnimator);
        int stepSize = (int)((trajectoryUpdateFramerate * TRAJECTORY_SECONDS) / MotionMatchingDatabase.TRAJECTORY_POS_COUNT);
        for (int i = 0; i < MotionMatchingDatabase.TRAJECTORY_POS_COUNT; i++)
        {
            var position = futurePositions[i * stepSize];
            currMotionState.futurePositions[i] = worldToBody.MultiplyPoint(position);
        }

        // Update past trajectory
        var hipPos = meshAnimator.GetBoneTransform(HumanBodyBones.Hips).position;
        int currTrajectoryFrameId = (int)(Time.time * trajectoryUpdateFramerate);
        int diff = currTrajectoryFrameId - lastTrajectoryFrameId;
        lastTrajectoryFrameId = currTrajectoryFrameId;
        Vector3 currBodyPosition = MotionMatchingDatabase.GetBodyPositionFromHip(hipPos);
        if (diff > 0)
        {
            for (int i = pastPositionsWorld.Length-1; i >= diff; i--)
            {
                pastPositionsWorld[i] = pastPositionsWorld[i-diff];
            }
            for (int i = 0; i < diff; i++)
            {
                pastPositionsWorld[i] = currBodyPosition;
            }
        }
        stepSize = (int)((trajectoryUpdateFramerate * TRAJECTORY_SECONDS) / MotionMatchingDatabase.TRAJECTORY_POS_COUNT);
        for (int i = 0; i < currMotionState.pastPositions.Length; i++)
        {
            var position = pastPositionsWorld[i * stepSize];
            currMotionState.pastPositions[i] = worldToBody.MultiplyPoint(position);
        }

        playerTrajectoryVisualizer.UpdatePoints(
            currMotionState.pastPositions,
            currMotionState.futurePositions
        );
        // =====================================================================

        // =====================================================================
        // Update limb state
        worldToBody = MotionMatchingDatabase.GetWorldToBodyTransform(meshAnimator);
        var leftFootPos = meshAnimator.GetBoneTransform(HumanBodyBones.LeftFoot).position;
        leftFootPos = worldToBody.MultiplyPoint(leftFootPos);
        var rightFootPos = meshAnimator.GetBoneTransform(HumanBodyBones.RightFoot).position;
        rightFootPos = worldToBody.MultiplyPoint(rightFootPos);

        var prevLeftFootPos = currMotionState.leftFootPos;
        var prevRightFootPos = currMotionState.rightFootPos;

        currMotionState.hipHeight = hipPos.y;
        currMotionState.leftFootVelocity = (leftFootPos - prevLeftFootPos) / Time.deltaTime;
        currMotionState.rightFootVelocity = (rightFootPos - prevRightFootPos) / Time.deltaTime;
        currMotionState.leftFootPos = leftFootPos;
        currMotionState.rightFootPos = rightFootPos;
        // =====================================================================
    }

    void GetTrajectoryDelta(out Vector3 deltaPos, out Quaternion deltaRot)
    {
        const float maxRotSpeed = 45;
        float maxRotDelta = maxRotSpeed * Time.deltaTime;
        Vector3 velocity = currSpeed * transform.forward;
        int frameId = (int)(Time.time * trajectoryUpdateFramerate);
        int frameDiff = frameId - lastFrameId;
        lastFrameId = frameId;
        if (frameDiff > 0)
        {
            Vector3 appliedVel = Vector3.Lerp(velocity, targetVelocity, TRAJECTORY_CURVATURE);
            currSpeed = appliedVel.magnitude;
            if (currSpeed > 0.05)
            {
                deltaPos = appliedVel * Time.deltaTime;

                var newRot = Quaternion.LookRotation(appliedVel.normalized, Vector3.up);
                deltaRot = newRot * Quaternion.Inverse(transform.rotation);
                var deltaEuler = deltaRot.eulerAngles;
                deltaRot = Quaternion.Euler(
                    Mathf.Min(deltaEuler.x, maxRotDelta),
                    Mathf.Min(deltaEuler.y, maxRotDelta),
                    Mathf.Min(deltaEuler.z, maxRotDelta)
                );
                return;
            }
        }

        deltaPos = Vector3.zero;
        deltaRot = Quaternion.identity;
    }

    void ApplyPoseToMesh()
    {
        var transitionRatio = Mathf.Min(1, (Time.time - transitionStartTime) / TRANSITION_TIME);

        for (int boneIndex = 0; boneIndex < (int)HumanBodyBones.LastBone; boneIndex++)
        {
            var boneId = (HumanBodyBones)boneIndex;
            var bgTransform = backgroundPlaybackAnimator.GetBoneTransform(boneId);
            var targetTransform = currPlaybackAnimator.GetBoneTransform(boneId);
            if (bgTransform != null)
            {
                var meshTransform = meshAnimator.GetBoneTransform(boneId);
                meshTransform.localRotation = Quaternion.Slerp(
                    bgTransform.localRotation, targetTransform.localRotation, transitionRatio
                );
            }
		}
        var bgHipTransform = backgroundPlaybackAnimator.GetBoneTransform(HumanBodyBones.Hips);
        var targetHipTransform = currPlaybackAnimator.GetBoneTransform(HumanBodyBones.Hips);
        var meshHipTransform = meshAnimator.GetBoneTransform(HumanBodyBones.Hips);
        meshHipTransform.localPosition = Vector3.Lerp(
            bgHipTransform.localPosition, targetHipTransform.localPosition, transitionRatio
        );

        // Interpolate between requested trajectory and playback movement.
        var animationDeltaPos = Vector3.Lerp(
            backgroundPlaybackAnimator.deltaPosition, currPlaybackAnimator.deltaPosition, transitionRatio
        );
        animationDeltaPos.Scale(new Vector3(1, 0, 1));
        var animationDeltaRot = Quaternion.Slerp(
            backgroundPlaybackAnimator.deltaRotation, currPlaybackAnimator.deltaRotation, transitionRatio
        );

        Vector3 inputDeltaPos;
        Quaternion inputDeltaRot;
        GetTrajectoryDelta(out inputDeltaPos, out inputDeltaRot);

        const float forceInput = 0.25f;
        float rotForceScale = (1 - Mathf.Min(1, Vector3.Distance(targetVelocity.normalized, transform.forward)));
        var newPosition = transform.position + Vector3.Lerp(animationDeltaPos, inputDeltaPos, forceInput);
        rigidbody.MovePosition(newPosition);
        transform.rotation *= Quaternion.Slerp(animationDeltaRot, inputDeltaRot, rotForceScale);
        //transform.rotation *= inputDeltaRot;
    }

    #endregion
}
