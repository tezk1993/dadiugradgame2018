﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Homebrew;
using UnityEngine.Rendering.PostProcessing;

public class Player : MonoBehaviour {

    //Charge Hit Box not enabled anywhere
    //setChargeHitBoxSize not called anywhere
    //resetChargeHitBoxSize not called anywhere
    public delegate void EnemyHit();
    public static event EnemyHit hit;
    public delegate void LoseCombo();
    public static event LoseCombo loseCombo;

	public delegate void SecondWindCount();
	public static event SecondWindCount swCount;

	public delegate void AliveTime();
	public static event AliveTime aliveTime;

	public delegate void DashCount();
	public static event DashCount dashCount;

	public delegate void DeathScene();
	public static event DeathScene deathScene;

    private Coroutine slowtime;
    
    private InputBase input;
    private EventManager eventManager;

    public float startSpeed;
    public float speedWhileCharging;
    public float upwardAndDownwardModifier;
    float vignetteLayerSecondWind = 0.5f;

    public float dashSpeed;
    public float dashDuration = 0.2f;
    public int maxDashCharges = 2;
    //[HideInInspector]
    public int dashCharges;
    public float dashCooldown = 1.5f;
    private float cooldown;
    private Text cooldownText;
    public Material dashMeterChargeArcMat;
    public GameObject dashMeter;
    //public GameObject dashMeter;
    public GameObject dashIndicator;
    public GameObject dashChargeInner;
    public GameObject dashChargeOuter;
    public GameObject staff;
    //public ParticleSystem deathParcticle;
    //public CollectParticles deathParticlesScript;

    [Tooltip("Can the player dash (this is false initially if the bamboo stick is not picked up)")]
    public bool dashAble;

    //new stuff
    private GameObject dashButtonInner;
    private GameObject dashButtonOuter;
    //end new stuff

    //public int chargeMultiplier; // only public for debugging

    float startDashSpeed;
    float startDashDuration;


    private float speed;
    private float speedMod;
    private Vector3 dashVelocity;
    private bool chargingDash;
    public bool dashing; // does this need to be public?
    private float curDashTime = 0;
    private BoxCollider chargeHitBox;

    public bool invulnerable; // not used for anything right now
    public float invinsibleTime = 0.5f;
    private bool invinsible;



    private bool aboutToDieBacking = false;
    private bool aboutToDie
    {
        get
        {
            return aboutToDieBacking;
        }
        set
        {
            aboutToDieBacking = value;
            animator.SetBool("SecondWind", value);
        }
    }

    private Animator animator;
    public Rigidbody rb;

    [Foldout("Special Combo Stuff", true)]
    [Header("Special Combo Stuff")]
    public float specialComboMultiplier;
    public float specialComboTime;
    private bool inSpecialCombo;


    [Foldout("Second Wind Stuff", true)]
    [Header("Second Wind Stuff")]
    public State secondWindState;
    public List<State> enemiesPrevState;
    public GameObject safeSpace;
    public GameObject safeSpaceZone;
    private Image secondWindUI;
    public PostProcessVolume volume;
    Vignette vignetteLayer;
    public OutlineRenderer outlineRenderScript;
    public bool goingIntoSecondWind;
    public float timeBeforeSecondWindIsActivated = 1f;
    public float pulseEffectSizeSecondWind = 1.15f;

    private Vector3 startScale;
    private Vector3 startScaleBackground;
    public bool savedBySecondWind;
    public float secondWindDuration = 3;
    public float secondWindTimer;
    public bool curInSecondWind;
    private Image secondWindChargeMeter;
    private Image secondWindChargeMeterBackground;

    public float secondWindCharge = 10;
    public float requiredSecondWindCharges = 10;

    [Foldout ("Health Stuff", true)]
    [Header("Health Stuff")]
    public GameObject[] healthArray;
    private GameObject healthContainer;
    public int healthInt;
    //new stuff
    public int initialHealth;
    //end new stuff
    private bool isAlive;
    private bool dead = false;

    [Foldout("Trail Stuff", true)]
    [Header("Trail Renderer Stuff")]
    public Material trailMaterial;
    public Material trailMaterialSpecial;
    public float trailFadeTime = .5f;
    public float trailStartWidth = 1;
    public float trailEndWidth = 0.5f;
    private TrailRenderer trail;

    [Foldout("Dash Winddown Stuff", true)]
    [Header("dash winddown variables")]
    public AnimationCurve winddownCurve;
    private bool windingDown;
    private float winddownDuration;
    [Tooltip("amount of time during winddown that youre still dashing")]public float winddownDashingDuration = 0.1f;
    private Coroutine dashWinddown;
    

    [Foldout("ScreenShake Stuff", true)]
    [Header("Screenshake Stuff")]
    public float screenshakeMult = 1.5f;
    public float screenshakeDuration = 0.35f;

    [Foldout("Time Slow Stuff", true)]
    [Header("Time Slow Stuff")]
    public AnimationCurve timeSlowCurve;
    private float timeSlowCurveDuration;


    private void OnEnable()
    {
        input = FindObjectOfType<InputBase>();
        input.dash += Dash;
        ComboScript.SpecialCount += SpecialComboStart;
        CollectParticles.collect += IncreaseSecondWindCharge;
    }

    private void OnDisable()
    {
        input.dash -= Dash;
        ComboScript.SpecialCount -= SpecialComboStart;
        CollectParticles.collect -= IncreaseSecondWindCharge;

    }

    void Start () {
        if (specialComboMultiplier == 0)
        {
            specialComboMultiplier = 2;
        }
        if(specialComboTime == 0)
        {
            specialComboTime = 3;
        }
        startScale = secondWindChargeMeter.rectTransform.localScale;
        startScaleBackground = secondWindChargeMeterBackground.rectTransform.localScale;


    }

    private void Awake()
    {
        input = FindObjectOfType<InputBase>();
        eventManager = GameObject.FindGameObjectWithTag("EventManager").transform.GetComponent<EventManager>();
        volume = GameObject.FindGameObjectWithTag("PostProcessing").GetComponent<PostProcessVolume>();
        outlineRenderScript = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<OutlineRenderer>();
        animator = GetComponent<Animator>();
        animator.SetFloat("RunSpeed", startSpeed / 7);
        //deathParcticle = gameObject.transform.GetChild(0).GetComponent<ParticleSystem>();
        //deathParticlesScript = gameObject.transform.GetChild(0).GetComponent<CollectParticles>();
        speed = startSpeed;

        dashCharges = maxDashCharges;
        cooldownText = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("DebugStuff").transform.Find("Cooldown text").GetComponent<Text>();
        cooldown = dashCooldown;
        cooldownText.text = "Dash Charge: " + dashCharges.ToString() + "\n" + "Dash CD: " + cooldown.ToString("F2");


        rb = gameObject.transform.GetComponent<Rigidbody>();

        trail = GetComponent<TrailRenderer>();
        trail.material = trailMaterial;
        trail.startWidth = trailStartWidth;
        trail.endWidth = trailEndWidth;
        trail.time = trailFadeTime;
        trail.enabled = false;

        chargeHitBox = GetComponent<BoxCollider>();
        chargeHitBox.enabled = false;

        healthContainer = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("HealthContainerForRun").gameObject;
        /*healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }
        */
        volume.profile.TryGetSettings(out vignetteLayer);
                    vignetteLayer.enabled.value = false;

        secondWindUI = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("SecondWindUI").GetComponent<Image>();
        secondWindUI.gameObject.SetActive(false);
        secondWindTimer = 0;

        //dashIndicator = GameObject.Find("dashIndicator");
        //dashIndicator.SetActive(false);
        //dashChargeInner = GameObject.Find("dashChargeInner");
        //dashChargeInner.SetActive(false);
        //dashChargeOuter = GameObject.Find("dashChargeOuter");
        //dashChargeOuter.SetActive(false);
        //dashMeter = GameObject.Find("DashMeter");
        //dashMeterChargeArcMat = dashMeter.transform.Find("Bar").GetComponent<Image>().material;
        //new stuff
        GameObject inGameUI = GameObject.FindGameObjectWithTag("IngameUI");
        dashButtonInner = inGameUI.transform.Find("Inner2").gameObject;
        dashButtonOuter = inGameUI.transform.Find("Outer2").gameObject;
        inGameUI.transform.Find("Inner").gameObject.GetComponent<FlashAndFade>().enableFadeOnTouch();
        inGameUI.transform.Find("Outer").gameObject.GetComponent<FlashAndFade>().enableFadeOnTouch();
        if (!dashAble)
        {
            //outlineRenderScript.outlineStrength = 0;
            staff.SetActive(false);
            StartCoroutine(HideDashButton());

        }
        startDashSpeed = dashSpeed;
        startDashDuration = dashDuration;
        //end new stuff
        secondWindChargeMeter = inGameUI.transform.Find("Secondwind").transform.Find("Mask").GetComponent<Image>();
        secondWindChargeMeterBackground = inGameUI.transform.Find("Secondwind").transform.Find("Background").GetComponent<Image>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!dead)

        {
            if (dashCharges > 0 && dashAble)
            {
                dashIndicator.SetActive(input.IsCharging());
                if (dashCharges > 1)
                {
                    dashChargeOuter.SetActive(true);
                }
                else
                {
                    dashChargeOuter.SetActive(false);
                }
                dashChargeInner.SetActive(true);

            }
            else
            {
                dashChargeInner.SetActive(false);

                dashChargeOuter.SetActive(false);

                dashIndicator.SetActive(false);
            }




            if (curInSecondWind == true) // if the player is in second wind state
            {
                CurrentlyInSecondWind();
            }
            else if (dashCharges < maxDashCharges)
            {
                DashCooldown();
            }
            bool isRunning = false;
            
            if (!input.IsTouching())
            {
                isAlive = true;
            }

            if (input.IsTouching() && isAlive)
            {

                if (input.Direction().x > 0.7f && input.Direction().x < 1f || input.Direction().x < -0.7f && input.Direction().x > -1f) // calculate whether a modifier should be used on the speed if the player goes upwards or downwards
                {
                    speedMod = upwardAndDownwardModifier;
                }
                else
                {
                    speedMod = 0;
                }
                var velocity = new Vector3(input.Direction().x, 0, input.Direction().y);
                if (chargingDash)
                {
                    speed = speedWhileCharging;
                }

                velocity = new Vector3(input.Direction().x, 0, input.Direction().y) * (speed + speedMod);
                isRunning = true;
                if (!aboutToDie && !dashing && !goingIntoSecondWind)
                {
                    transform.Translate(velocity * Time.fixedDeltaTime, Space.World); // only move the player if he is not in secondwind state
                }
                if (velocity != Vector3.zero)
                {
                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
            }
            if (input.DashDirection().magnitude > 0)
            {
                dashIndicator.transform.rotation = Quaternion.LookRotation(new Vector3(input.DashDirection().x, 0, input.DashDirection().y), Vector3.up);
            }
            animator.SetBool("Running", isRunning);
            aliveTime();
        }
        if (eventManager.aliveEnemies.Count > 0)
        {
            Vector3 tMin = new Vector3(0, 0, 0);
            float minDist = Mathf.Infinity;
            for (int i = 0; i < eventManager.aliveEnemies.Count; i++)
            {
                eventManager.closestEnemy = Vector3.Distance(transform.position, eventManager.aliveEnemies[i].transform.position);
                if (eventManager.closestEnemy < minDist)
                {
                    tMin = eventManager.aliveEnemies[i].transform.position;
                    minDist = eventManager.closestEnemy;
                }
            }
        }
    }


    public void TakeDamage(GameObject particleTarget)
    {
        if (!invulnerable)
        {
            if (!invinsible && aboutToDie == false)
            {
                SpecialComboStop();
                if (healthInt > 0)
                {
                    if (loseCombo != null)
                    {
                        loseCombo();
                    }
                    healthInt--;
                    AkSoundEngine.PostEvent("Play_Hit_Hero", gameObject);
                }

                if (healthInt <= 0)
                {
                    //deathParcticle.Play();
                    //deathParticlesScript.particleFollow(particleTarget);
                    aboutToDie = true;
                    StartCoroutine(SecondWind());
                }
                StartCoroutine(Invinsible());
            }
        }
    }

    public IEnumerator SecondWind()
    {
		swCount();
        dashCharges = 0;
        StartCoroutine(GoingToSecondWind());
        if (secondWindCharge >= requiredSecondWindCharges)
        {

            secondWindChargeMeter.fillAmount = 0;
            secondWindCharge -= requiredSecondWindCharges;

            vignetteLayer.enabled.value = true;
            vignetteLayer.intensity.value = 0.5f;

            AkSoundEngine.PostEvent("Play_Second_Wind_Activated", gameObject);
            savedBySecondWind = false;
            enemiesPrevState = new List<State>();
            maxDashCharges = 1;
            dashCharges = 1;

            for (int i = 0; i < eventManager.aliveEnemies.Count; i++) //Change if the eventmanager enemy count is fixed.
            {
                enemiesPrevState.Add(eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState);
                eventManager.aliveEnemies[i].gameObject.GetComponent<StateController>().currentState = secondWindState;
            }

            rb.isKinematic = true;
            safeSpaceZone = Instantiate(safeSpace, this.transform, false);
            float progress = 0;

            while (progress < 1)
            {
                safeSpaceZone.transform.localScale = Vector3.Lerp(safeSpaceZone.transform.localScale, new Vector3(3, 3, 3), progress);
                progress += Time.deltaTime;
                yield return null;
            }

            float secondWindEffecttimer = 1;
            while (secondWindEffecttimer > 0)
            {
                outlineRenderScript.outlineStrength = secondWindEffecttimer;
                secondWindEffecttimer -= Time.deltaTime;

                yield return null;
            }

            secondWindTimer = secondWindDuration;
            curInSecondWind = true;
            secondWindUI.gameObject.SetActive(true);
            Destroy(safeSpaceZone);
            rb.isKinematic = false;
			aliveTime();
            yield return new WaitForSeconds(secondWindDuration);//shouldnt this coroutine be stopped if determineSecondWindSurvival is triggered by a collision?

            DetermineSecondWindSurvival();
            //secondWindCharge -= requiredSecondWindCharges;
        }
        else
        {
			deathScene();

			//SceneSwitcher.ReloadAllInScene();
        }
    }

    public IEnumerator GoingToSecondWind()
    {
        goingIntoSecondWind = true;

        yield return new WaitForSeconds(timeBeforeSecondWindIsActivated);

        goingIntoSecondWind = false;
    }

    public void HitLost(GameObject enemy)
    {
        Debug.Log("Got 'ems!");
        if (dashing)
        {
            enemy.GetComponent<BoxCollider>().enabled = false;

            if (aboutToDie)
            {
                savedBySecondWind = true;
                secondWindUI.gameObject.SetActive(false);
                DetermineSecondWindSurvival();
            }

            if (hit != null)
            {
                hit();
            }

            AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject);
            Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
            Debug.Log("Murder: " + enemy.name);

            if (outlineRenderScript.outlineStrength <= 1)
            {
                Debug.Log("Increase Second wind charges !");

                outlineRenderScript.outlineStrength += 0.1f;
            }
            Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
            if (dashCharges < maxDashCharges)
            {
                dashCharges++;
                if (!inSpecialCombo)
                {
                    slowtime = StartCoroutine(SlowTimeNew());
                }
            }

            AkSoundEngine.PostEvent("Play_Death_Small", gameObject);
            Instantiate(enemy.GetComponent<LostOneLogic>().deathParticles, transform.position, 
                        enemy.GetComponent<LostOneLogic>().deathParticles.transform.rotation, null);
            enemy.SetActive(false);
        }
    }

    public void HitEnemy(GameObject enemy)
    {
        Debug.Log("Got 'ems!");
        if (dashing)
        {
            if (aboutToDie)
            {
                savedBySecondWind = true;
                secondWindUI.gameObject.SetActive(false);
                DetermineSecondWindSurvival();
            }

            if (hit != null)
            {
                hit();
            }


            if (enemy.GetComponent<StateController>().enemyInvis <= 0.0f)
            {
                enemy.GetComponent<StateController>().gotHit();
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject);
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                Debug.Log("Murder: " + enemy.name);

                if (outlineRenderScript.outlineStrength <= 1)
                {
                    Debug.Log("Increase Second wind charges !");

                    outlineRenderScript.outlineStrength += 0.1f;
                }
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                if (dashCharges < maxDashCharges)
                {
                    dashCharges++;
                    Debug.Log("got a charge");
                    if (!inSpecialCombo)
                    {
                        slowtime = StartCoroutine(SlowTimeNew());
                    }
                }
            }
        }
    }

    public void IncreaseSecondWindCharge()
    {
        if (secondWindCharge < requiredSecondWindCharges)
        {
            secondWindCharge += 0.05f;
            secondWindChargeMeter.fillAmount = secondWindCharge / requiredSecondWindCharges;
            StartCoroutine(PulseEffectSecondWind());
        }
    }

    public IEnumerator PulseEffectSecondWind()
    {
        if(secondWindChargeMeter.rectTransform.localScale.x < startScale.x * pulseEffectSizeSecondWind)
        {
            secondWindChargeMeter.rectTransform.localScale *= pulseEffectSizeSecondWind;
            secondWindChargeMeterBackground.rectTransform.localScale *= pulseEffectSizeSecondWind;
        }

        yield return new WaitForSeconds(0.01f);
        secondWindChargeMeter.rectTransform.localScale = startScale;
        secondWindChargeMeterBackground.rectTransform.localScale = startScaleBackground;

    }

    public void DetermineSecondWindSurvival()
    {
        if (savedBySecondWind == true)
        {
            vignetteLayer.enabled.value = false;
            AkSoundEngine.PostEvent("Play_Second_Wind_Respawn", gameObject);
            rb.isKinematic = false;
            aboutToDie = false;
            maxDashCharges = 2;

            for (int i = 0; i < eventManager.aliveEnemies.Count / 2; i++)
            {
                eventManager.aliveEnemies[i].gameObject.GetComponent<StateController>().currentState = enemiesPrevState[i];
            }

            curInSecondWind = false;

            healthInt = initialHealth;
        }
        else
        {
			//SceneSwitcher.ReloadAllInScene();
			startplayerdeath();
            isAlive = false;  // was startTouch = false;  before
            //AkSoundEngine.PostEvent("Play_Death_Hero", gameObject);
        }
    }

    [ContextMenu("Player death")]
    public void startplayerdeath()
    {
		Debug.Log("startPlayerDeath called");
		StartCoroutine(PlayerDeath());
    }

    public IEnumerator PlayerDeath()
    {
		Debug.Log("PlayerDeath called");
        dead = true;
        //player death animation

        var indicator = GameObject.Find("Indicators");
        SpriteRenderer[] indicatorRenderers = indicator.GetComponentsInChildren<SpriteRenderer>();

        Image[] ingameUIImages = new Image[5];
        ingameUIImages[0] = GameObject.Find("MenuButtonGoesOnHealthUI").GetComponent<Image>();
        ingameUIImages[1] = GameObject.Find("Outer").GetComponent<Image>();
        ingameUIImages[2] = GameObject.Find("Inner").GetComponent<Image>();
        ingameUIImages[3] = GameObject.Find("Outer2").GetComponent<Image>();
        ingameUIImages[4] = GameObject.Find("Inner2").GetComponent<Image>();

        var secondWindGameObject = GameObject.Find("Secondwind");
        Image[] secondWindImages = secondWindGameObject.GetComponentsInChildren<Image>();

        for (int i = 0; i < 100; i++)
        {
            for (int j = 0; j < indicator.GetComponentsInChildren<SpriteRenderer>().Length; j++)
            {
                indicatorRenderers[j].color = new Color(indicatorRenderers[j].color.r, indicatorRenderers[j].color.g, indicatorRenderers[j].color.b, indicatorRenderers[j].color.a - 1f/100f);
            }
            
            for (int k = 0; k < ingameUIImages.Length; k++)
            {
                ingameUIImages[k].color = new Color(ingameUIImages[k].color.r, ingameUIImages[k].color.g, ingameUIImages[k].color.b, ingameUIImages[k].color.a - 1f / 100f);
            }

            for (int l = 0; l < secondWindGameObject.GetComponentsInChildren<Image>().Length; l++)
            {
                secondWindImages[l].color = new Color(secondWindImages[l].color.r, secondWindImages[l].color.g, secondWindImages[l].color.b, secondWindImages[l].color.a - 1f / 100f);
            }

            yield return new WaitForSeconds(1 / 100);
        }

		//Menu pop-up
		deathScene();

        for (int i = 0; i < eventManager.aliveEnemies.Count; i++) 
        {
            eventManager.aliveEnemies[i].GetComponent<StateController>().Stagger(120);
            eventManager.aliveEnemies[i].deathParticles.Play();
            //Enemy death animation
            //Enemy death material            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            if (!dashing)
            {
                TakeDamage(other.gameObject.GetComponent<Bullet>().magazine.transform.parent.gameObject);
            }

            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
            if (dashCharges < maxDashCharges)
            {
                dashCharges++;
                Debug.Log("got a charge");

            }
        }
        else if (other.gameObject.CompareTag("Hazard"))
        {
            TakeDamage(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Killable"))
        {
            HitEnemy(other.gameObject);
        }
        else if (other.gameObject.CompareTag("Lost"))
        {
            HitLost(other.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Hazard"))
        {
            TakeDamage(other.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {
            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject);
                    StopDash();
                }

                TakeDamage(collision.gameObject.transform.parent.gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structures_Impact", gameObject);
                StopDash();
            }
        }
        else if (collision.gameObject.CompareTag("HealthBox"))
        {
            GainHealth();
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            HitEnemy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Lost"))
        {
            HitLost(collision.gameObject);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        
        if (collision.gameObject.CompareTag("Shield"))
        {
            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject);
                    StopDash();
                }

                TakeDamage(collision.gameObject);
            }
        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structures_Impact", gameObject);
                StopDash();
            }
        }

        else if (collision.gameObject.CompareTag("Hazard"))
        {
            Debug.Log("hazard enter");
            TakeDamage(collision.gameObject);
        }
        //new stuff end
        //new stuff

        else if (collision.gameObject.CompareTag("HealthBox"))
        {
            GainHealth();
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            HitEnemy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Lost"))
        {
            HitLost(collision.gameObject);
        }
    }

    public void activateStaff()
    {
        outlineRenderScript.outlineStrength = 1;
        staff.SetActive(true);
        dashAble = true;
        //new stuff
        dashButtonInner.SetActive(true);
        dashButtonOuter.SetActive(true);
        StartCoroutine(dashButtonInner.GetComponent<FlashAndFade>().FadeInOut());
        StartCoroutine(dashButtonOuter.GetComponent<FlashAndFade>().FadeInOut());
        //end new stuff
    }

    //public void PickUpStaff(GameObject staffOnGround)
    //{
    //    staff.SetActive(true);
    //    Destroy(staffOnGround);
    //    dashAble = true;
    //}

    //new stuff
    public void GainHealth()
    {
        healthInt++;/*
        if (healthArray[healthInt - 1])
        {
            healthArray[healthInt - 1].SetActive(true);
        }
        */
    }

    public IEnumerator SlowTimeNew()
    {
        /*new stuff*/
        if (slowtime != null)
        {
            StopCoroutine(slowtime);
        }
        float unscaledElapsedTime = 0;
        timeSlowCurveDuration = timeSlowCurve.keys[timeSlowCurve.length - 1].time;
        while (unscaledElapsedTime < timeSlowCurveDuration)
        {
            Time.timeScale = timeSlowCurve.Evaluate(unscaledElapsedTime);
            unscaledElapsedTime += Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1;
    }
    
    public void SpecialComboStart()
    {
        StartCoroutine(SpecialCombo());
    }

    IEnumerator SpecialCombo()
    {
        inSpecialCombo = true;
        dashSpeed *= specialComboMultiplier;
        dashDuration /= specialComboMultiplier;
        Time.timeScale = 0.5f;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
        if (trailMaterialSpecial != null)
        {
            trail.material = trailMaterialSpecial;
        }
        yield return new WaitForSeconds(specialComboTime);
        SpecialComboStop();
    }

    public void SpecialComboStop()
    {
        inSpecialCombo = false;

        trail.material = trailMaterial;
        dashSpeed = startDashSpeed;
        dashDuration = startDashDuration;
        Time.timeScale = 1;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }

    public void ResetHealth()
    {
        healthInt = initialHealth;
        /*
        foreach (GameObject health in healthArray)
        {
            health.SetActive(false);
        }
        for (int i = 0; i > initialHealth; i++)
        {
            healthArray[i].SetActive(true);
        }
        */
    }
    //end new stuff

    IEnumerator Invinsible()
    {
        invinsible = true;
        yield return new WaitForSeconds(invinsibleTime);
        invinsible = false;
    }

    void CurrentlyInSecondWind()
    {
        if (secondWindTimer > 0)
        {
            AkSoundEngine.SetRTPCValue("Second_Wind_Time", secondWindTimer);
            secondWindTimer -= Time.deltaTime;
            secondWindUI.fillAmount = secondWindTimer / secondWindDuration;

            vignetteLayerSecondWind += Time.deltaTime;

            if(vignetteLayerSecondWind / secondWindDuration > 0.5f)
            {
                vignetteLayer.intensity.value = vignetteLayerSecondWind / secondWindDuration;
            }

        }

    }

    void DashCooldown()
    {
        cooldown -= Time.unscaledDeltaTime;

        if (dashing)
        {
            cooldown = dashCooldown;
        }

        if (cooldown <= 0 && !aboutToDie && !dashing)
        {
            dashCharges = maxDashCharges;


            cooldown = dashCooldown;
            AkSoundEngine.PostEvent("Play_Dash_Recharge", gameObject);
        }
        cooldownText.text = "Dash Charge: " + dashCharges.ToString() + "\n" + "Dash CD: " + cooldown.ToString("F2");
        dashMeterChargeArcMat.SetFloat("_Arcrange", (dashCharges * 180) + (dashCooldown - cooldown) / dashCooldown * 180);
    }


    public void Dash(Vector2 dir)
    {

        if (!goingIntoSecondWind)
        {
            StartCoroutine(DashAttack(dir));
            dashCount();
        }
    }


    public IEnumerator DashAttack(Vector2 dir) //dash function
    {
        if (!dashing && dashCharges > 0)
        {
            if (dashAble)
            {
                if (windingDown)
                {
                    StopCoroutine(dashWinddown);
                    windingDown = false;
                }
                chargeHitBox.enabled = true;
                curDashTime = 0;
                speed = dashSpeed;
                dashing = true;
                trail.enabled = true;
                animator.SetTrigger("Dash");
                AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);

                dashCharges--;
                dashVelocity = new Vector3(dir.x, 0, dir.y);
                while (curDashTime < dashDuration)
                {
                    curDashTime += Time.deltaTime;
                    transform.Translate(dashVelocity * Time.deltaTime * speed, Space.World);
                    transform.rotation = Quaternion.LookRotation(dashVelocity, Vector3.up);
                    yield return null;
                }
                StopDash();
            }
        }
    }

    //public IEnumerator IndicatorDash()
    //{
    //    float curIndiDashTime = 0;
    //    indicatorSpeed = dashSpeed;
    //    while (curIndiDashTime < dashDuration)
    //    {
    //        curIndiDashTime += Time.deltaTime;
    //        chargeIndicator.transform.Translate(dashVelocity * Time.deltaTime * indicatorSpeed, Space.World);
    //        yield return null;
    //    }
    //}

    public IEnumerator DashWindDown()
    {
        windingDown = true;
        winddownDuration = winddownCurve.keys[winddownCurve.length - 1].time;
        float winddownTimer = 0;
        while (winddownDuration > winddownTimer)
        {
            if (winddownTimer < winddownDashingDuration)
            {
                dashing = true;
            }
            else
            {
                dashing = false;
                chargeHitBox.enabled = false;
            }
            speed = winddownCurve.Evaluate(winddownTimer) * dashSpeed;
            yield return null;
            winddownTimer += Time.deltaTime;
        }
        speed = startSpeed;
        windingDown = false;
    }

    public void StopDash()
    {
        speed = startSpeed;
        dashing = false;
        trail.enabled = false;
        //chargeHitBox.enabled = false;
        dashWinddown = StartCoroutine(DashWindDown());
        if (curInSecondWind && !savedBySecondWind) //if youre in secondwind and you havnt hit anything by the end of stopdash you die
        {
            DetermineSecondWindSurvival();
        }
    }
    /// <summary>
    /// default size is (2,1,1)
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    public void setChargeHitBoxSize(float x, float y, float z)
    {
        chargeHitBox.size = new Vector3(x, y, z);
    }

    public void resetChargeHitBoxSize()
    {
        chargeHitBox.size = new Vector3(2, 1, 1);
    }
    private IEnumerator HideDashButton()
    {
        yield return null;
        dashButtonInner.SetActive(false);
        dashButtonOuter.SetActive(false);
    }
}

