﻿/**
 * OutlineRenderer.cs
 * Created by: Artur Barnabas
 * Created on: 05/11/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineRenderer : MonoBehaviour
{
    #region Variables

    public Material outlineFirstPass;
    public Material outlineSecondPass;
    public Shader outlineColorShader;
    public Color outlineColor = Color.yellow;

    [Tooltip("A multiplyer for the outline color. 0 will produce no outline, 1 will produce an outline of the specified outline color, and any number above 1 will produce a brigher outline.")]
    public float outlineStrength = 1;

    int outlineObjectsRtShaderId;
    int outlineIntermediateRtShaderId;
    int pixelSizeShaderId;
    int outlineColorShaderId;
    int outlineStrengthShaderId;

    // Outline objects render texture
    RenderTexture outlineObjectsRt;

    RenderTexture outlineIntermediateRt;

    Camera outlineRendererCamera;
    Camera mainCamera;

    #endregion

    #region Unity Methods

    public void Start()
    {
        mainCamera = Camera.main;

        outlineObjectsRt = new RenderTexture(
            mainCamera.pixelWidth, mainCamera.pixelHeight, 0, RenderTextureFormat.ARGB32
        );

        outlineIntermediateRt = new RenderTexture(
            mainCamera.pixelWidth / 2, mainCamera.pixelHeight / 2, 0, RenderTextureFormat.ARGB32
        );
        
        var outlineRendererObject = transform.Find("Outline Renderer");
        outlineRendererCamera = outlineRendererObject.GetComponent<Camera>();
        outlineRendererCamera.CopyFrom(mainCamera);
        outlineRendererCamera.clearFlags = CameraClearFlags.SolidColor;
        outlineRendererCamera.backgroundColor = new Color(0, 0, 0, 0);
        outlineRendererCamera.targetTexture = outlineObjectsRt;
        outlineRendererCamera.allowHDR = false;
        outlineRendererCamera.allowMSAA = false;
        outlineRendererCamera.useOcclusionCulling = false;

        int playerMeshMask = 1 << 11;
        outlineRendererCamera.cullingMask = playerMeshMask;
        outlineRendererCamera.SetReplacementShader(outlineColorShader, "");

        outlineObjectsRtShaderId = Shader.PropertyToID("_OutlineObjects");
        outlineIntermediateRtShaderId = Shader.PropertyToID("_OutlineIntermediate");
        pixelSizeShaderId = Shader.PropertyToID("_PixelSize");
        outlineColorShaderId = Shader.PropertyToID("_OutlineColor");
        Shader.SetGlobalColor(outlineColorShaderId, outlineColor);

        float intermediateWidth = mainCamera.pixelWidth / 2;
        float intermediateHeight = mainCamera.pixelHeight / 2;
        var intermediatePixelSize = new Vector4(1.0f / intermediateWidth, 1.0f / intermediateHeight, 0, 0);

        outlineFirstPass.SetVector(pixelSizeShaderId, intermediatePixelSize);
        outlineSecondPass.SetVector(pixelSizeShaderId, intermediatePixelSize);
    }

    public void Update()
    {
        outlineRendererCamera.fieldOfView = mainCamera.fieldOfView;
        Color colorWithStrength = Color.LerpUnclamped(Color.black, outlineColor, outlineStrength);
        Shader.SetGlobalColor(outlineColorShaderId, colorWithStrength);
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(outlineObjectsRt, outlineIntermediateRt, outlineFirstPass);
        //Graphics.Blit(outlineIntermediateRt, destination);

        outlineSecondPass.SetTexture(outlineObjectsRtShaderId, outlineObjectsRt);
        outlineSecondPass.SetTexture(outlineIntermediateRtShaderId, outlineIntermediateRt);
        Graphics.Blit(source, destination, outlineSecondPass);
    }

    #endregion

    #region Methods

    #endregion
}
