﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

    //duplicate the bullet object if you need more bullet objects
    private GameObject player;
    private GameObject[] bullets; //gets all children
    private int bulletCounter;  //keeping track of the current bullet
    [Tooltip("the velocity of all bullets")]public float bulletSpeed;
    [Tooltip("fire rate")] public float bulletInterval;
    [Tooltip("time before the bullet is recovered")] public float bulletLifeTime;

    public bool triShooter;
    [Tooltip("amount trishots are in front and behind the player")] public float sideShotsDeg;

    public bool novaShooter;
    [Tooltip("amount of shots in a nova")] public int novaBulletCount;



    // Use this for initialization
    void Start () {
        bulletCounter = 0;
        player = GameObject.FindGameObjectWithTag("Player");
        bullets = new GameObject[transform.childCount];
        for (int i = 0; i < bullets.Length; i++)
        {
            bullets[i] = transform.GetChild(i).gameObject;
        }
        StartCoroutine(Fire());
        //Fire();


	}

    private IEnumerator Fire()
    {
        if (bulletCounter >= bullets.Length)
        {
            bulletCounter = 0;
        }
        if (!novaShooter) { 
        GameObject bullet = bullets[bulletCounter];
        bullet.SetActive(true);
        Vector3 interceptVector = FindInterceptVector(transform.position, bulletSpeed, player.transform.position, player.GetComponent<Rigidbody>().velocity);
        bullet.GetComponent<Rigidbody>().velocity = interceptVector;
        StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
        bulletCounter++;
            if (triShooter)
            {
                //in front of player
                bullet = bullets[bulletCounter];
                bullet.SetActive(true);
                Vector3 frontInterceptVector = Quaternion.AngleAxis(sideShotsDeg, Vector3.up) * interceptVector;
                //frontInterceptVector.magnitude/interceptVector.magnitude
                frontInterceptVector = frontInterceptVector.normalized * bulletSpeed;
                bullet.GetComponent<Rigidbody>().velocity = frontInterceptVector;
                StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                bulletCounter++;
                //behind player
                bullet = bullets[bulletCounter];
                bullet.SetActive(true);
                Vector3 behindInterceptVector = Quaternion.AngleAxis(-sideShotsDeg, Vector3.up) * interceptVector;
                behindInterceptVector = behindInterceptVector.normalized * bulletSpeed;
                bullet.GetComponent<Rigidbody>().velocity = behindInterceptVector;
                StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                bulletCounter++;
            }
        }

        if (novaShooter)
        {
            for (int i = 0; i < novaBulletCount; i++)
            {
                GameObject bullet = bullets[bulletCounter];
                bullet.SetActive(true);
                Vector3 direction = Quaternion.Euler(new Vector3(0, 360 / novaBulletCount * i, 0)) * Vector3.forward;
                bullet.GetComponent<Rigidbody>().velocity = direction*bulletSpeed;
                StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                bulletCounter++;
                
                
            }
        }
        yield return new WaitForSeconds(bulletInterval);
        StartCoroutine(Fire());
        
    }

    private Vector3 FindInterceptVector(Vector3 shotOrigin, float shotSpeed, Vector3 targetOrigin, Vector3 targetVel)
    {

        Vector3 dirToTarget = Vector3.Normalize(targetOrigin - shotOrigin);

        // Decompose the target's velocity into the part parallel to the
        // direction to the cannon and the part tangential to it.
        // The part towards the cannon is found by projecting the target's
        // velocity on dirToTarget using a dot product.
        Vector3 targetVelOrth =
        Vector3.Dot(targetVel, dirToTarget) * dirToTarget;

        // The tangential part is then found by subtracting the
        // result from the target velocity.
        Vector3 targetVelTang = targetVel - targetVelOrth;

        /*
        * targetVelOrth
        * |
        * |
        *
        * ^...7  <-targetVel
        * |  /.
        * | / .
        * |/ .
        * t--->  <-targetVelTang
        *
        *
        * s--->  <-shotVelTang
        *
        */

        // The tangential component of the velocities should be the same
        // (or there is no chance to hit)
        // THIS IS THE MAIN INSIGHT!
        Vector3 shotVelTang = targetVelTang;

        // Now all we have to find is the orthogonal velocity of the shot

        float shotVelSpeed = shotVelTang.magnitude;
        if (shotVelSpeed > shotSpeed)
        {
            // Shot is too slow to intercept target, it will never catch up.
            // Do our best by aiming in the direction of the targets velocity.
            return targetVel.normalized * shotSpeed;
        }
        else
        {
            // We know the shot speed, and the tangential velocity.
            // Using pythagoras we can find the orthogonal velocity.
            float shotSpeedOrth =
            Mathf.Sqrt(shotSpeed * shotSpeed - shotVelSpeed * shotVelSpeed);
            Vector3 shotVelOrth = dirToTarget * shotSpeedOrth;

            // Finally, add the tangential and orthogonal velocities.
            return shotVelOrth + shotVelTang;
        }
    }




}