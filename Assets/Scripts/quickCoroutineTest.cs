﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class quickCoroutineTest : MonoBehaviour {

    public bool myBoolTrue;
    // Use this for initialization
    void Start () {

        StartCoroutine("testCoroutineYield");

	}
    

    IEnumerator testCoroutineYield()
    {

        Debug.Log("Wait untill it becomes true");

        while (!myBoolTrue)
        {
            yield return null;
        }

        Debug.Log("It became true");

    }
}
