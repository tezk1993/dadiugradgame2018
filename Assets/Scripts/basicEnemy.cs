﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicEnemy : MonoBehaviour {

    public bool dead;
    public EventManager eventManager;

    [ContextMenu ("Die")]
    void Die()
    {
        dead = true;
        eventManager.CheckEnemyCount();
    }
}
