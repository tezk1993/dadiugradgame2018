﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour {

    public float rpm;
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up * (rpm / 60) * Time.deltaTime*360);
    }
}
