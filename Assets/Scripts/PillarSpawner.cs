﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarSpawner : MonoBehaviour
{

    [System.Serializable]
    public class pillarPoolSet
    {
        [Range(0f, 30f)]
        public float spawnDelay;
        public List<GameObject> pillarPool;
        public List<GameObject> spawnPoints;
        public List<GameObject> spawnedPillars;

    }

    public List<pillarPoolSet> pillarSets;

    private void Awake()
    {
        createPillars();
    }

    public void StartPillarSpawn(int spawnSetIndex)
    {
        StartCoroutine(spawnPillars(spawnSetIndex));
    }

    IEnumerator spawnPillars(int spawnSetIndex)
    {
        for (int i = 0; i < pillarSets[spawnSetIndex].spawnedPillars.Count; i++)
        {

            pillarSets[spawnSetIndex].spawnedPillars[i].transform.GetChild(0).transform.position = pillarSets[spawnSetIndex].spawnPoints[i].transform.position;
            pillarSets[spawnSetIndex].spawnedPillars[i].SetActive(true);
            yield return new WaitForSeconds(pillarSets[spawnSetIndex].spawnDelay);
        }

    }


    public void StartPillarRemove(int spawnSetIndex)
    {
        StartCoroutine(removePillars(spawnSetIndex));
    }

    IEnumerator removePillars(int spawnSetIndex)
    {
        for (int i = 0; i < pillarSets[spawnSetIndex].spawnedPillars.Count; i++)
        {

            pillarSets[spawnSetIndex].spawnedPillars[i].GetComponent<Animator>().SetTrigger("Lower");
            yield return new WaitForSeconds(pillarSets[spawnSetIndex].spawnDelay);
        }

    }

    public void createPillars()
    {
        for (int i = 0; i < pillarSets.Count; i++)
        {

            for (int j = 0; j < pillarSets[i].pillarPool.Count; j++)
            {
                GameObject Pillar = GameObject.Instantiate<GameObject>(pillarSets[i].pillarPool[j],transform);
                Pillar.name = Pillar.name + i + "," + j;
                Pillar.transform.parent = null;
                Pillar.transform.position = Vector3.zero;
                Pillar.transform.GetChild(0).transform.rotation = pillarSets[i].spawnPoints[j].transform.rotation;
                pillarSets[i].spawnedPillars.Add(Pillar);
                Pillar.gameObject.SetActive(false);
            }
        }
    }
    [ContextMenu("Create SpawnPoints")]
    public void MakeSpawnPoints()
    {

        for (int i = 0; i < pillarSets.Count; i++)
        {
            var Container = new GameObject("spawnPointContainer" + i.ToString());
            Container.name = "SpawnPointsContainer" + i.ToString();
            Container.transform.parent = gameObject.transform;
            for (int j = 0; j < pillarSets[i].spawnPoints.Count; j++)
            {
                var spawnPoint = new GameObject("spawnPoint" + j.ToString());
                spawnPoint.transform.parent = Container.transform;
                pillarSets[i].spawnPoints[j] = spawnPoint;
            }
        }
    }
    private void OnDrawGizmos()
    {
        for (int i = 0; i < pillarSets.Count; i++)
        {
            for (int j = 0; j < pillarSets[i].spawnPoints.Count; j++)
            {
                if (pillarSets[i].spawnPoints[j] != null)
                {
                    Gizmos.color = Color.blue;
                    Gizmos.DrawSphere(pillarSets[i].spawnPoints[j].transform.position, 0.3f);
                }
            }
        }
    }
}
