﻿/**
 * MuralTriggerArea.cs
 * Created by: Artur Barnabas
 * Created on: 21/11/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuralTriggerArea : MonoBehaviour
{
    #region Variables
    public GameObject Player;
    #endregion

    #region Unity Methods

    public void Start()
    {
        if(Player == null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }
    }

    public void Update()
    {
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == Player)
            {
                    Camera.main.GetComponent<CameraMovement>().ZoomOnMural();
            }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == Player)
        {
            Camera.main.GetComponent<CameraMovement>().ZoomBackFromMural();
        }
    }


    #endregion

    #region Methods

    #endregion
}
