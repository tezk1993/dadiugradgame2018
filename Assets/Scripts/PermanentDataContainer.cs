﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Text;
using UnityEngine.SceneManagement;

public class PermanentDataContainer : MonoBehaviour {

    public static PermanentDataContainer instance;

    private static bool musicSystemPlayed = false;
    public bool cameraShake = true;
    //public bool vibrate = true;
    public bool newGamePlus = false;
    [HideInInspector] public bool english = true;
    [HideInInspector] public float convertedMasterValue, convertedMusicValue, convertedSFXValue;
    private SceneManager sceneManager;

    [HideInInspector] public int levelProgress;


    private void Awake()
    {
        sceneManager = new SceneManager();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //cosplaying a singleton
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        if (instance !=this)
        {
            Destroy(gameObject);
        }
        LoadSettings();
        LoadProgress();
    }
    private void Start()
    {
        AkSoundEngine.SetState("Music_State", "Safe");
        AkSoundEngine.SetState("Player_State", "Alive");
        AkSoundEngine.SetState("Menu_State", "Deactive");
        AkSoundEngine.SetState("Second_Wind", "Deactive");
        AkSoundEngine.SetState("Dash_Cool", "Deactive");


        if (musicSystemPlayed == false)
        {
            AkSoundEngine.PostEvent("Play_Music_System", gameObject);
            musicSystemPlayed = true;
        }
    }

    public void SaveSettings()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/settings.dat");
        Settings settings = new Settings();
        //collecting all the things that need to go in the settings file in a Settings object
        settings.cameraShake = PermanentDataContainer.instance.cameraShake; //not sure why i need to reference the static object type&name to get the correct variable
        //settings.vibrate = PermanentDataContainer.instance.vibrate;
        settings.newGamePlus = PermanentDataContainer.instance.newGamePlus;
        settings.english = PermanentDataContainer.instance.english;
        Debug.Log("settings.convertedMasterValue: " + settings.convertedMasterValue);
        settings.convertedMasterValue = PermanentDataContainer.instance.convertedMasterValue;
        Debug.Log("settings.convertedMasterValue: " + settings.convertedMasterValue);
        settings.convertedMusicValue = PermanentDataContainer.instance.convertedMusicValue;
        settings.convertedSFXValue = PermanentDataContainer.instance.convertedSFXValue;
        //stuffing it all into the actual file
        bf.Serialize(file, settings); 
        file.Close();
        ImplementSettings();
        Debug.Log("SaveSettings() finished");
    }

    public void LoadSettings()
    {
        if (File.Exists(Application.persistentDataPath + "/settings.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/settings.dat", FileMode.Open);
            Settings settings = (Settings)bf.Deserialize(file);
            file.Close();
            cameraShake = settings.cameraShake;
            //vibrate = settings.vibrate;
            newGamePlus = settings.newGamePlus;
            english = settings.english;
            convertedMasterValue = settings.convertedMasterValue;
            convertedMusicValue = settings.convertedMusicValue;
            convertedSFXValue = settings.convertedSFXValue;

            //deleted stuff
            //AkSoundEngine.SetRTPCValue("Master_Volume", convertedMasterValue);
            //AkSoundEngine.SetRTPCValue("Music_Volume", convertedMusicValue);
            //AkSoundEngine.SetRTPCValue("Sound_Volume", convertedSFXValue);
            //end deleted stuff

            //new stuff
            ImplementSettings();
            //end new stuff
            Debug.Log("load complete");
        }
    }

    public void SaveProgress()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/progress.dat");
        Progress progress = new Progress();

        progress.level = PermanentDataContainer.instance.levelProgress;

        //stuffing it all into the actual file
        bf.Serialize(file, instance);
        file.Close();
    }

    public void LoadProgress()
    {
        if (File.Exists(Application.persistentDataPath + "/progress.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/progress.dat", FileMode.Open);
            Progress progress = new Progress();
            file.Close();

            levelProgress = progress.level;
        }
    }
    //new stuff
    public void ImplementSettings()
    {
        if (SceneManager.GetActiveScene().name != "Menu")
        {
            Camera.main.GetComponent<CameraShake>().camShakeEnabled = cameraShake;
        }
        AkSoundEngine.SetRTPCValue("Master_Volume", convertedMasterValue);
        AkSoundEngine.SetRTPCValue("Music_Volume", convertedMusicValue);
        AkSoundEngine.SetRTPCValue("Sound_Volume", convertedSFXValue);

        //english/danish language settings handled by MenuControl
    }
    //end new stuff
}


[Serializable]
class Settings
{
    public bool cameraShake;
    //public bool vibrate;
    public bool newGamePlus;
    public bool english;
    public float convertedMasterValue, convertedMusicValue, convertedSFXValue;
}
[Serializable]
class Progress
{
    public int level;
}
