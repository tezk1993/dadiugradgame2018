﻿/**
 * StaffPickupAnimation.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffPickupAnimation : MonoBehaviour
{
    #region Variables
    public float darknessDisappearSpeed = 3;
    private GameObject dangerZones;
    #endregion

    #region Unity Methods

    public void Start()
    {
        dangerZones = GameObject.Find("WaterDarknessDangerZones");
    }

    public void Update()
    {
    }

    public void StartAnimation()
    {
        StartCoroutine(AnimationCoroutine());
    }

    IEnumerator AnimationCoroutine()
    {
        WaterDarkness darkness = FindObjectOfType<WaterDarkness>();

        dangerZones.SetActive(false);

        const float targetRadius = 2;
        while (darkness.darknessRadius < targetRadius)
        {
            darkness.darknessRadius += darknessDisappearSpeed * Time.deltaTime;
            yield return null;
        }
        darkness.darknessRadius = targetRadius;
    }


    #endregion

    #region Methods

    #endregion
}
