﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MeteorLobber : MonoBehaviour {


	[Header("Numbers to tweak meteor-behaviour")]
	[Tooltip("The distance ahead of the metoer-lobber, where the meteor should land")]
	public float dist;
	[Tooltip("The distance within which the dropzone should start changing color")]
	public float alertDist;
	[Tooltip("The height at which the meteor should spawn")]
	public float spawnHeight = 10;

	[Header("Prefab references:")]
	[Tooltip("A reference to the meteor prefab")]
	public GameObject meteor;
	[Tooltip("A reference to the reticle showing the meteor's crash-site")]
	public GameObject dropZone;

	[Header("Colors of the reticle")]
	[Tooltip("The color the reticle should change when the meteor is close enough to the ground")]
	public Color endColor;
	[Tooltip("The color the reticle should have when it is spawned")]
	public Color startColor;

	Renderer rend;

	private void Awake()
	{
		//Instantiate a reference of both the reticle and meteor to be used, then set them inactive
		dropZone = GameObject.Instantiate(dropZone, transform.position, Quaternion.identity);
		rend = dropZone.GetComponent<Renderer>();
		rend.material.color = startColor;
		meteor = GameObject.Instantiate(meteor, transform.position, Quaternion.identity);
		dropZone.SetActive(false);
		meteor.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
            Launch();

		//if the meteor is active in the hierarchy, call the function to give visual feedback on dropzone
		if (meteor.activeInHierarchy)
		{
			ReticleColor();
		}
	}
	//Sets the drop position/height of the meteor and the position of the reticle (beneath the meteor at ground level), then sets both objects active
	public void Launch() 
	{
		dropZone.transform.position = new Vector3(transform.forward.x, 0, transform.position.z + transform.forward.z * dist);
		dropZone.SetActive(true);
		meteor.transform.position = new Vector3(dropZone.transform.position.x, dropZone.transform.position.y + spawnHeight, dropZone.transform.position.z);
		meteor.SetActive(true);
	}

	//Provides visual feedback by changing the color of the reticle when meteor enters the specified "danger-range"
	public void ReticleColor()
	{
		float dist = Vector3.Distance(meteor.transform.position, dropZone.transform.position);
		rend.material.color = startColor;
		if (dist < alertDist)
		{
			rend.material.color = Color.Lerp(startColor, endColor, (alertDist - dist) / alertDist);
		}

		if (dist <= alertDist/4) 
		{
			rend.material.color = Color.white;
		}

		if (dist <= 0.01f) 
		{
			dropZone.SetActive(false);
			meteor.SetActive(false);
		}
	}
}
