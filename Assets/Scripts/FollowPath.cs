﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour {

    public string pathName;

    public float delay;

    public float time;

	// Use this for initialization
	void Start () {
        StartCoroutine("moveAlongPath");
	}

    IEnumerator moveAlongPath()
    {
        yield return new WaitForSeconds(delay);

        iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath(pathName), "easytype", iTween.EaseType.easeInOutSine, "time", time));
    }
}

