﻿/**
 * MakeDontDestroyOnLoad.cs
 * Created by: Artur Barnabas
 * Created on: 21/11/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MakePersistent : MonoBehaviour
{
    #region Variables
    private static Dictionary<string, GameObject> instances = new Dictionary<string, GameObject>();
    #endregion

    #region Unity Methods
    
    public void Awake()
    {
        if (instances.ContainsKey(gameObject.name))
        {
            Destroy(gameObject);
            return;
        }
        
        instances.Add(gameObject.name, gameObject);
        DontDestroyOnLoad(gameObject);
    }

    public static void DestroyAllPersistent()
    {
        foreach (var item in instances)
        {
            Destroy(item.Value);
        }
        instances.Clear();
    }

    #endregion

    #region Methods

    #endregion
}
