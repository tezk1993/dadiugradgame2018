﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputWasd : InputBase {
    float horizontal;
    float vertical;
    bool isTouching;
    bool isCharging;
    Vector2 direction;
    Vector2 dashDirection;


    //public override float GetHorizontal()
    //{
    //    return horizontal;
    //}

    //public override float GetVertical()
    //{
    //    return vertical;
    //}
    public override Vector2 Direction()
    {
        return direction;
    }

    public override Vector2 DashDirection()
    {
        return dashDirection;
    }

    public override bool IsTouching()
    {
        return isTouching;
    }

    public override bool IsCharging()
    {
        return isCharging;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.D))
        {
            horizontal = 1f;
            isTouching = true;
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            horizontal = -1f;
            isTouching = true;
        }
        else
        {
            horizontal = 0f;
            isTouching = true;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            vertical = -1f;
            isTouching = true;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            vertical = -1f;
            isTouching = true;
        }
        else
        {
            vertical = 0f;
            isTouching = true;
        }
        if(Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.W))
        {
            isTouching = false;
        }
    }

    
}
