﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimaBehaviour : MonoBehaviour {

    private float lureAudioTimer;

    ParticleSystem animaParticle;

    public FieldOfView fieldOfView;

    public float speed;

    float step;

    public bool moveAlongPath = true;

    int currentPosInPath;
    public List<GameObject> pathObjects;
    public List<Vector3> pathDirections;
    Vector3 playerDir;
    GameObject closestPoint;
    GameObject reachedPoint;
    public Renderer animaRenderer;
    public GameObject teleportPoint;
    // Use this for initialization
    void Awake () {
        //animaParticle = gameObject.transform.GetChild(0).GetComponent<ParticleSystem>();
        fieldOfView = gameObject.GetComponent<FieldOfView>();
        //animaParticle.Play();
	}

    private void Start()
    {
        lureAudioTimer = Random.Range(3, 5);
        StartCoroutine(PlayLureSound());
    }

    public void switchFollow() { 
            moveAlongPath = false;
    }

    IEnumerator PlayLureSound()
    {
        while (lureAudioTimer > 0)
        {
            lureAudioTimer -= Time.deltaTime;
            yield return null;
        }
        AkSoundEngine.PostEvent("Play_Anima_Lure", gameObject);
        lureAudioTimer = Random.Range(3, 5);
        StartCoroutine(PlayLureSound());
    }

    private void Update()
    {
        checkpathDirections();
        checkPlayerPos();
        if (moveAlongPath && closestPoint != null)
        {
            moveToPoint(closestPoint);
            if (closestPoint.transform.position == gameObject.transform.position)
            {
                reachedPoint = closestPoint;

            }
        }


 
    }

    void checkpathDirections()
    {

        for (int i = 0; i < pathObjects.Count; i++)
        {
            var heading = pathObjects[i].transform.position - gameObject.transform.position;

            var distance = heading.magnitude;
            var direction = heading / distance; // This is now the normalized direction.
            pathDirections[i] = direction;

        }
    }
    void checkPlayerPos()
    {
        if(fieldOfView.visibleTargets.Count > 0) { 
            var heading = fieldOfView.visibleTargets[0].position - gameObject.transform.position;
            var distance = heading.magnitude;
            playerDir = heading / distance; // This is now the normalized direction.
            transform.LookAt(fieldOfView.visibleTargets[0]);
            //transform.rotation = Quaternion.Euler(0, transform.rotation.y, 0);
            checkClosestPoint();
        }
    }

    void checkClosestPoint()
    {
        if (pathObjects.Count > 0) { 
            Vector3 tMin = new Vector3 (0,0,0);
            float minDist = Mathf.Infinity;
            for (int i = 0; i < pathDirections.Count; i++)
            {
                float dist = Vector3.Distance(pathDirections[i], -playerDir);
                if (dist < minDist)
                {
                
                    tMin = pathDirections[i];
                    minDist = dist;
                }
            }
        
            if(reachedPoint != pathObjects[pathDirections.IndexOf(tMin)]) { 
                closestPoint = pathObjects[pathDirections.IndexOf(tMin)];
            }
            Debug.DrawRay(gameObject.transform.position, tMin);
        }

    }

    void moveToPoint(GameObject pointObject)
    {
        if (pointObject != null) { 
            step = speed * Time.deltaTime;
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, pointObject.transform.position, step);
        }
    }

    [ContextMenu("Teleport")]
    public void startTeleportation()
    {
        StartCoroutine("animaTeleport");
    }

    IEnumerator animaTeleport()
    {
        float deathMaterialVal = 2;
        while (deathMaterialVal > 0)
        {
            animaRenderer.material.SetFloat("_Fill", deathMaterialVal);
            deathMaterialVal -= Time.deltaTime;

            yield return null;
        }
        //yield return new WaitForSeconds(5);
        transform.position = teleportPoint.transform.position;
        while (deathMaterialVal < 3)
        {
            animaRenderer.material.SetFloat("_Fill", deathMaterialVal);
            deathMaterialVal += Time.deltaTime;

            yield return null;
        }
    }

    [ContextMenu("Create SpawnPoints")]
    public void MakeSpawnPoints()
    {
        var Container = new GameObject("pathObjectsContainer");
        Container.name = "pathObjectsContainer";
        Container.transform.parent = gameObject.transform;
        for (int i = 0; i < pathObjects.Count; i++)
        {
                var spawnPoint = new GameObject("pathObject" + i.ToString());
                spawnPoint.transform.parent = Container.transform;
                pathObjects[i] = spawnPoint;
        }

        var teleportPointVar = new GameObject("teleportPoint");
        teleportPointVar.transform.parent = Container.transform;
        teleportPoint = teleportPointVar;
    }

    private void OnDrawGizmos()
    {
        if (pathObjects != null)
        {
            for (int i = 0; i < pathObjects.Count; i++)
            {
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawSphere(pathObjects[i].transform.position, 1);
                        Gizmos.color = Color.magenta;
                        Gizmos.DrawSphere(teleportPoint.transform.position, 1);
            }
        }
    }
}
