﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructiblePillar : MonoBehaviour {
    public delegate void EnemyHit();
    public static event EnemyHit hit;

    public float radius;
    public float power;
    private Player playerScript;
    private WASDMovement wasdplayerScript;

    private ParticleSystem destructionParticle;
    private Renderer meshRender;
    public Collider childCollider;

    public bool WasdControls;

    // Use this for initialization
    void Awake () {
        if(WasdControls == true)
            wasdplayerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<WASDMovement>();
        else
            playerScript = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        destructionParticle = gameObject.GetComponent<ParticleSystem>();
        meshRender = gameObject.GetComponent<MeshRenderer>();

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Player hit");
            childCollider.enabled = false;
            meshRender.enabled = false;
            destructionParticle.Play();
            if (hit != null)
            {
                hit();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")){
            if(WasdControls == true)
            {
                if (wasdplayerScript.dashing == true)
                {
                    childCollider.enabled = false;
                    meshRender.enabled = false;
                    destructionParticle.Play();
                    if (hit != null)
                    {
                        hit();
                    }
                    if (meshRender.material.ToString() == "Bamboo_Enviorment")
                    {
                        AkSoundEngine.PostEvent("Play_Bamboo_Destroyed", gameObject);
                    }
                    else
                    {
                        AkSoundEngine.PostEvent("Play_Structures_Destroyed", gameObject); //testing if the sound is cut when gameObject is destroyed
                    }
                    // DestroyPillar();
                }
            }
            else
                if (playerScript.dashing == true)
                {
                if (playerScript.dashCharges < playerScript.maxDashCharges)
                {
                    playerScript.dashCharges++;
                }
                if (meshRender.material.ToString() == "Bamboo_Enviorment")
                    {
                        AkSoundEngine.PostEvent("Play_Bamboo_Destroyed", gameObject);
                    }
                    else
                    {
                        AkSoundEngine.PostEvent("Play_Structures_Destroyed", gameObject); //testing if the sound is cut when gameObject is destroyed
                    }
                    childCollider.enabled = false;
                    meshRender.enabled = false;
                    
                    destructionParticle.Play();
                    if (hit != null)
                    {
                        hit();
                    }
                    // DestroyPillar();
                }
        }
    }

    public void DestroyPillar()
    {
        Vector3 explosionPos = transform.position;
        explosionPos.y = 0f;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null && !rb.CompareTag("Player"))
            {
                rb.isKinematic = false;
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
                
                Destroy(rb.gameObject, 1f);
            }
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
