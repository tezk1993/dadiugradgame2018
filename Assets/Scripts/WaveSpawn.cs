﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawn : MonoBehaviour {
    public EventManager eventManager;
    public enemySpawner spawner;
    public int spawnWave;
	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {
		if(eventManager.Enemies.Count == 0)
        {
            spawner.StartSpawnEnemies(spawnWave);

            if (spawnWave > spawner.enemyPoolSets.Count)
            {
                spawnWave = 0;
            }
        }
	}
}
