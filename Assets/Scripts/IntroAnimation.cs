﻿/**
 * IntroAnimation.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Rendering.PostProcessing;

public class IntroAnimation : MonoBehaviour
{
    #region Types
    [System.Serializable]
    public class CameraMovementInfo
    {
        public GameObject waypoints;
        public float animationTime = 1;
        public float delayTime = 0;
    }
    #endregion

    #region Variables

    public bool enableIntro = true;

    [Space(15)]

    public float darknessTime = 2;

    public float playerAnimStartDelay = 2;
    public float overviewAnimationTime = 4;
    public GameObject startCameraWaypoints;
    public GameObject overviewCameraWaypoints;
    public CameraMovementInfo[] movementWaypoints;


    private WaterDarkness darkness;

    #endregion

    #region Unity Methods

    public void Start()
    {
        if (enableIntro)
        {
            StartCoroutine(PlayCameraAnimation());
        }
    }

    public void Update()
    {
    }

    #endregion

    #region Methods

    IEnumerator PlayCameraAnimation()
    {
        Camera mainCam = Camera.main;
        var cameraMovement = mainCam.GetComponent<CameraMovement>();
		var input = FindObjectOfType<InputJoystickDragDash>();
		input.EnableMovement(false);
		input.EnableDash(false);
        darkness = FindObjectOfType<WaterDarkness>();
        darkness.darknessRadius = 1.3f;

        var player = GameObject.Find("Player");
        var playerAnimator = player.GetComponent<Animator>();
        int introLayerId = 1;
        playerAnimator.speed = 0;
        playerAnimator.SetLayerWeight(introLayerId, 1);


        cameraMovement.AnimateFromCurrent(startCameraWaypoints, 0);

        yield return new WaitForSeconds(playerAnimStartDelay);
        playerAnimator.speed = 1;

        for (int i = 0; i < movementWaypoints.Length; i++)
        {
            var curr = movementWaypoints[i];
            cameraMovement.AnimateFromCurrent(curr.waypoints, curr.animationTime);
            yield return new WaitForSeconds(curr.animationTime + curr.delayTime);
        }

        cameraMovement.AnimateFromCurrent(overviewCameraWaypoints, overviewAnimationTime);

        const float darknessRadiusTarget = 0.4f;
        float darknessRadiusStart = 1;
        float darknessSpeed = (darknessRadiusTarget - darknessRadiusStart) / darknessTime;
        while (darkness.darknessRadius > darknessRadiusTarget)
        {
            darkness.darknessRadius += darknessSpeed * Time.deltaTime;
            yield return null;
        }
        darkness.darknessRadius = darknessRadiusTarget;

        while (darkness.pulse < 1)
        {
            darkness.pulse += 2f * Time.deltaTime;
            yield return null;
        }
        darkness.pulse = 1;

        yield return new WaitForSeconds(0.5f);

        cameraMovement.AnimateToFollowState(2f);
        yield return new WaitForSeconds(1.9f);

        playerAnimator.SetLayerWeight(introLayerId, 0);
        playerAnimator.Play("Idle", 0, 0); // Start playing the idle animation from the beggining.
		input.EnableMovement(true);
		input.StartMovementFade();
    }

    #endregion
}
