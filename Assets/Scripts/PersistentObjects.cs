﻿/**
 * PersistentObjects.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentObjects : MonoBehaviour
{
    #region Variables

    private static PersistentObjects instance;

    #endregion

    #region Unity Methods

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
    
    }

    public void Update()
    {
    
    }

    #endregion

    #region Methods

    #endregion
}
