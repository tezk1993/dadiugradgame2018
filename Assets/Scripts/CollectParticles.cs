﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectParticles : MonoBehaviour {

    public delegate void CollectCharge();
    public static event CollectCharge collect;

    public ParticleSystem collectParticles;
    public ParticleSystem.Particle[] emittedParticles;
    public List<bool> emittedParticlesCheck;
    public GameObject targetObject;
    public Vector3 targetPosition;
	// Use this for initialization
	void Awake () {
        targetObject = GameObject.FindGameObjectWithTag("Player");
	}

    private void Update()
    {
        if (collectParticles.isPlaying && targetObject != null)
        {
            InitializeIfNeeded();
            StartCoroutine(particleStopRising());
          
        }
    }

    public void particleFollow(GameObject target)
    {
        targetObject = target;
        targetPosition = new Vector3(target.transform.position.x, target.transform.position.y + 1.5f, target.transform.position.z);
        StartCoroutine(particleStopRising());

    }

    IEnumerator particleStopRising()
    {
        yield return new WaitForSeconds(1f);
        // GetParticles is allocation free because we reuse the m_Particles buffer between updates
        int numParticlesAlive = collectParticles.GetParticles(emittedParticles);

        // Change only the particles that are alive
        
        for (int i = 0; i < numParticlesAlive; i++)
        {
            emittedParticles[i].position = Vector3.MoveTowards(emittedParticles[i].position, targetObject.transform.position, Time.deltaTime * Mathf.FloorToInt(Random.Range(5,8)));
            //emittedParticles[i].velocity = Vector3.zero;
            if(emittedParticles[i].position == targetObject.transform.position)
            {
                if(collect != null)
                {
                    collect();
                }
                emittedParticlesCheck[i] = true;
                emittedParticles[i].remainingLifetime = 0;
            }
        }
      
            // Apply the particle changes to the particle system
            collectParticles.SetParticles(emittedParticles, numParticlesAlive);
        
        if (!emittedParticlesCheck.Contains(false))
        {
            
            //Destroy(gameObject);
            targetObject = null;
            collectParticles.Stop();
            StopAllCoroutines();
        }
        /*
        collectParticles.GetParticles(emittedParticles);
        Debug.Log(emittedParticles.Length);
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < emittedParticles.Length; i++)
        {
            emittedParticles[i].velocity = Vector3.zero;
            //emittedParticles[i].position = Vector3.MoveTowards(emittedParticles[i].position, spot.transform.position, Time.deltaTime * speed);
        }
        // Reassign back to emitter
        collectParticles.SetParticles(emittedParticles,emittedParticles.Length);
        */
    }

    void InitializeIfNeeded()
    {
        if (collectParticles == null)
            collectParticles = GetComponent<ParticleSystem>();

        if (emittedParticles == null || emittedParticles.Length < collectParticles.main.maxParticles)
            emittedParticles = new ParticleSystem.Particle[collectParticles.main.maxParticles];
        if (emittedParticlesCheck.Count == 0)
        {
            for (int i = 0; i < collectParticles.GetParticles(emittedParticles); i++)
            {
                emittedParticlesCheck.Add(false);
            }
        }
    }
}
