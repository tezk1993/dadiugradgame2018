﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour {

    public Color inActiveColor;
    public Color activeColor;

    public void SetActive()
    {
        GetComponent<Image>().color = activeColor;
    }
    public void SetInActive()
    {
        GetComponent<Image>().color = inActiveColor;
    }
}
