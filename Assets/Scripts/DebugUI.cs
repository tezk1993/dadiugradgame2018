﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public void NextScene()
    {
        //Debug.Log(SceneManager.sceneCountInBuildSettings);
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (SceneManager.sceneCountInBuildSettings > nextSceneIndex)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
    }
    public void PreviousScene()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex - 1;
        if (0 <= nextSceneIndex)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
    }
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }


}
