﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeButtons : MonoBehaviour {

    public float volumeIncrements = 0.05f;

    // Use this for initialization
    void Start () {
		
	}


    public void DecreaseVolume()
    {
        Slider slider = GetComponent<Slider>();
        if (slider.value - volumeIncrements > 0)
        {
            slider.value -= volumeIncrements;
        }
    }
    public void IncreaseVolume()
    {
        Slider slider = GetComponent<Slider>();
        if (slider.value + volumeIncrements < 1)
        {
            slider.value += volumeIncrements;
        }
    }
}
