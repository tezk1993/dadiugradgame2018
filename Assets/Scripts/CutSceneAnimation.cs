﻿/**
 * CutSceneAnimation.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CutSceneAnimation : MonoBehaviour
{
    #region Types

    [System.Serializable]
    public class AnimationEvent
    {
        public GameObject cameraWaypoints;
        public float animationTime = 1;
        public float delayTime = 0;
        public UnityEvent afterEvent;
    }
    #endregion

    #region Variables

    public bool animateOnStart = false;

    public AnimationEvent[] events;

    #endregion

    private void Start()
    {
        if (animateOnStart)
        {
            Animate();
        }
    }

    public void Animate()
    {
        StartCoroutine(AnimateCoroutine());
    }

    IEnumerator AnimateCoroutine()
    {
        CameraMovement camMovement = Camera.main.GetComponent<CameraMovement>();
        for (int i = 0; i < events.Length; i++)
        {
            var curr = events[i];
            if (curr.cameraWaypoints != null)
            {
                camMovement.AnimateFromCurrent(curr.cameraWaypoints, curr.animationTime);
            }
            yield return new WaitForSeconds(curr.animationTime + curr.delayTime);
            curr.afterEvent.Invoke();
        }
    }
}
