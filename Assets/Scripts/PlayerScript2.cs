﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



[RequireComponent(typeof(TrailRenderer))]
public class PlayerScript2 : MonoBehaviour {
    [Header("REMEMBER!!!!")]
    [Header("DebugUI prefab + HealthUI prefab in the scene!")]
    [Header("")]
    [Header("Drag the Joystick UI + cooldown text from DebugUI")]
    [Header("to the inspector")]
    [Header("Drag HealthUI prefab from scene to inspector on the ")]
    [Header("dedicated locations")]
    [Header("")]
    [Header("Movement Stuff + Temp Dash")]
    public float startSpeed;
    public float dashSpeed;
    public float dashTime;
    public Transform circle;
    public Transform outerCircle;
    [Tooltip("Threshold of movement, lower value == more responsive (use higher values to avoid jitter)")]
    public float threshold = 2f;
    [Tooltip("The number of directions, evenly distributed to which the player can move")]
    public float movementDirectionCount = 8;

    [HideInInspector]
    public Vector2 dir;

    private float lastMovedTime;
    private bool hasStartedMoving;
    private Vector2 startPos;
    private Vector2 curPos;
    private float speed;
    //private bool isDashing;
    private bool startTouch;
    private Vector2 pointA;
    private Vector2 pointB;

    [Header("Dash Stuff (not currently working)")]
    [Tooltip("How far should the character dash")]
    public float dashRange = 40;
    [Tooltip("How fast should the dash-speed decrease")]
    public float speedDecrease = 10f;
    public float dashStartTime;
    public bool dashing;
    [Tooltip("How long should the dash-cooldown be")]
    public float cooldownTime = 3;
    public Text cooldownText;
    [Tooltip("How long should the character be dashing")]
    public float maxDashTime = 1.0f;
    [Tooltip("How long should the script wait before reseting the input position (higher values will allow slow finger movement to be recorded but will also reduce the adaptiveness of the joystick)")]
    public float inputResetDelay = 0.1f;

    [Tooltip("A number between 0 and 1 specifying the size of area in the middle of the joystick where the input is not registered.")]
    public float joystickDeadzone = 0.2f;

    private float dashStoppingSpeed = 0.1f;
    private float currentDashTime;
    private float cooldown;
    private bool readyToDash;
    private Vector3 direction;
    //private Vector3 dashSpeed;

    [Header("Health Stuff")]
    [Tooltip("container of health icons")] public GameObject healthContainer;
    public GameObject[] healthArray;
    public int healthInt;
    public float screenshakeMult;
    public float screenshakeDuration;
    public float timeSlowDegree;
    public float timeSlowDuration;

    public float invis = 0.5f;

    [Header("Trail Renderer Stuff")]
    [Tooltip("This is the material for the trail, defining its color")]
    public Material trailMaterial;
    [Tooltip("The fade time of the trail")]
    public float trailTime = .5f;
    [Tooltip("The width of the trails head")]
    public float trailStartWidth = 1;
    [Tooltip("The width of the trails tail")]
    public float trailEndWidth = 0.5f;
    TrailRenderer trail;


    float inputAngleOffset;
    Animator animator;

    // Use this for initialization
    void Start () {
        //GetComponent<Rigidbody>().velocity = Vector3.right * 2f;
        speed = startSpeed;

        direction = Vector3.zero;
        currentDashTime = maxDashTime;
        cooldown = cooldownTime;
        trail = GetComponent<TrailRenderer>();
        trail.material = trailMaterial;
        trail.startWidth = trailStartWidth;
        trail.endWidth = trailEndWidth;
        trail.time = trailTime;
        trail.enabled = false;
        Debug.Log(healthContainer.transform.childCount);

        healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }

        var mainCam = Camera.main;
        inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, mainCam.transform.forward, Vector3.up);
        
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        invis -= Time.deltaTime;

        Movement();
        //dashSpeed = Vector3.Lerp(dashSpeed, new Vector3(0, direction.y, 0), Time.deltaTime * speedDecrease);
        //transform.position += dashSpeed * Time.deltaTime;
        //transform.Translate(dashSpeed);
        cooldownText.text = "Dash CD: " + cooldown.ToString();
        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            readyToDash = false;
        }
        else
        {
            cooldown = 0;
            readyToDash = true;
        }
        
    }
    //private void FixedUpdate()
    //{
    //    bool isRunning = false;
    //    if (startTouch)
    //    {
    //        Vector2 offset = pointB - pointA;
    //        Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
    //        circle.transform.position = new Vector3(pointA.x + direction.x * 50, pointA.y + direction.y * 50, Camera.main.transform.position.z);
    //        if (direction.magnitude > joystickDeadzone)
    //        {
    //            isRunning = true;
    //            var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
    //            angle += inputAngleOffset;
    //            angle *= Mathf.Deg2Rad;
    //            direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

    //            //Convert to vector 3 rotate 45 degrees to match camera rotation
    //            var velocity = new Vector3(direction.x, 0, direction.y) * speed;
    //            transform.Translate(velocity * Time.deltaTime, Space.World);
    //            transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
    //        }
    //    }
    //    animator.SetBool("Running", isRunning);
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            TakeDamage();
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {
            TakeDamage();
        }
        if (collision.gameObject.CompareTag("Killable"))
        {
            
            if (dashing)
            {
                Debug.Log("should kill the thang");
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                //SlowTime(timeSlowDegree, timeSlowDuration);
            }
            else
            {
                TakeDamage();
            }
        }
    }

    public void TakeDamage()
    {
        if (invis <= 0.0f)
        {
            if (healthInt > 0)
            {
                healthArray[healthInt - 1].SetActive(false);
                healthInt--;
            }

            if (healthInt <= 0)
            {
                Debug.Log("YOU DIED");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                
            }
            invis = 0.5f;
        }  
    }

    public IEnumerator SlowTime(float timeSlowDegree, float timeSlowDuration)
    {
        Time.timeScale = timeSlowDegree;
        yield return new WaitForSecondsRealtime(timeSlowDuration);
        Time.timeScale = 1;
    }

    void Movement()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                startPos = Input.GetTouch(0).position;
                //Debug.Log(startPos);
                hasStartedMoving = false;
                dir = curPos - startPos;
                //Debug.Log("StartPos: " + startPos + " CurrentPos: " + curPos);
                dir.Normalize();
                //Debug.Log("Dir = " + dir);
                //circle.transform.position = pointA;
                //outerCircle.transform.position = pointA;
                //circle.gameObject.SetActive(true);
                //outerCircle.gameObject.SetActive(true);
            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                // updating the reset start time to prevent a reset happen until the
                // reset delay has elapsed from now
                lastMovedTime = Time.unscaledTime;
                curPos = Input.GetTouch(0).position;

                if (Vector2.Distance(startPos, curPos) > threshold)
                {
                    dir = curPos - startPos;
                    Debug.DrawLine(curPos, startPos);
                    hasStartedMoving = true;
                    startPos = Input.GetTouch(0).position;
                    dir.Normalize();
                    float angle = Vector2.SignedAngle(new Vector2(-1, 0), dir);

                    // [-1, 1]
                    float relativeAngle = angle / 180;
                    // [0, 1]
                    relativeAngle = (relativeAngle + 1) / 2;

                    // [0, movementDirectionCount] (integer)
                    float directionId = Mathf.Round(relativeAngle * movementDirectionCount);

                    // [0, 2*pi]
                    angle = (directionId / movementDirectionCount) * Mathf.PI * 2;
                    dir = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                if (lastMovedTime + inputResetDelay < Time.unscaledTime)
                {
                    startPos = Input.GetTouch(0).position;
                }
            }
            
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                //circle.gameObject.SetActive(false);
                //outerCircle.gameObject.SetActive(false);
                dir = Vector2.zero;
                startPos = Vector2.zero;
                curPos = Vector2.zero;
            }
            if (Input.touchCount == 2 && Input.GetTouch(1).phase == TouchPhase.Began && readyToDash)
            {
                //Vector3 dir = new Vector3(direction.x * speed * Time.deltaTime, 0, direction.y * speed * Time.deltaTime);
                //DashTranslate(dir);
                cooldown = cooldownTime;
                speed = dashSpeed;
                dashing = true;
                trail.enabled = true;
                Invoke("StopDash", dashTime);
                Debug.Log("Dash!");
                animator.SetTrigger("Attack");
            }
        }
        var velocity = new Vector3(dir.x, 0, dir.y) * speed;

        bool isMoving = velocity.magnitude > 0.2f;
        if (isMoving)
        {
            animator.SetBool("Running", true);
            var angle = Vector2.SignedAngle(new Vector2(1, 0), dir.normalized);
            angle += inputAngleOffset;
            angle *= Mathf.Deg2Rad;
            velocity = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * speed;
            //Debug.Log(velocity);
            transform.position += velocity * Time.deltaTime;
            transform.rotation = Quaternion.LookRotation(velocity.normalized, Vector3.up);
        }
        else
        {
            animator.SetBool("Running", false);
        }

    }

    //public void DashTranslate(Vector3 direction)
    //{
    //    if (readyToDash == true)
    //    {
    //        currentDashTime = 0.0f;
    //        dashStartTime = Time.time;
    //        dashSpeed = direction * dashRange;
    //        readyToDash = false;
    //        cooldown = cooldownTime;
    //    }
    //    if (currentDashTime < maxDashTime)
    //    {
    //        currentDashTime += dashStoppingSpeed;
    //        dashing = true;
    //    }
    //    else
    //    {
    //        dashing = false;
    //    }
    //}
    void StopDash()
    {
        speed = startSpeed;
        dashing = false;
        trail.enabled = false;
    }
}
