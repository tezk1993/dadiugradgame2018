﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {
    [Tooltip("how much the camera shakes in x")] public float shakeX;
    [Tooltip("how much the camera shakes in y")] public float shakeY;
    [Tooltip("how much the camera shakes in z")] public float shakeZ;
    [Tooltip("how frequently the camera changes position during screen shake")]public float screenShakeInterval;
    [HideInInspector] public float shakeDuration;
    private float shakeTime;
    [HideInInspector] public float screenShakeMult;
    private bool shaking;
    private Vector3 originalPos;
    private float screenShakeIntervalTimer;
    public bool camShakeEnabled = true;


    public void Start()
    {
        shakeTime = 0.0f;
        shaking = false;
        //ShakeCam(0.2f, 15.0f);
    }


    public void ShakeCam(float screenShakeMultParam, float shakeDurationParam)
    {
        shakeDuration = shakeDurationParam;
        screenShakeMult = screenShakeMultParam;
        shaking = true;
        shakeTime = 0.0f;
        originalPos = transform.localPosition;
        StartCoroutine(Shake());
    }

    private IEnumerator Shake()
    {
        if (camShakeEnabled)
        {
            while (shakeTime < shakeDuration)
            {
                yield return new WaitForSecondsRealtime(screenShakeIntervalTimer);
                transform.localPosition = new Vector3(originalPos.x + shakeX * screenShakeMult * Random.value, originalPos.y + shakeY * screenShakeMult * Random.value, originalPos.z + shakeZ * screenShakeMult * Random.value);
                screenShakeIntervalTimer = screenShakeIntervalTimer - screenShakeInterval;
                shakeTime += Mathf.Max(Time.unscaledDeltaTime, screenShakeInterval);
            }
            shaking = false;
            transform.localPosition = originalPos;
        }
    }
}
