﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour {

    public GameObject joypad;
    private Button button;
    public GameObject inGameMenu;
    private GameObject debugUI;
    private GameObject dashMeter;
    public bool menuOpen;

    private void Awake()
    {
        button = GetComponent<Button>();
        debugUI = GameObject.Find("IngameUI");
        inGameMenu = debugUI.transform.Find("Menu").gameObject;
        dashMeter = transform.parent.transform.Find("Secondwind").gameObject;
    }

    private void Update()
    {
        //if (Time.timeScale > 0)
        //{
        //    if (joypad.activeSelf && button.IsInteractable())
        //    {
        //        button.interactable = false;
        //    }
        //    if (!joypad.activeSelf && !button.IsInteractable())
        //    {
        //        button.interactable = true;
        //    }
        //}
    }

    public void OpenInGameMenu()
    {
        Time.timeScale = 0f;
        button.interactable = false;
        inGameMenu.SetActive(true);
        //debugUI.SetActive(false);
        dashMeter.SetActive(false);
        menuOpen = true;
    }
    public void CloseInGameMenu()
    {
        Time.timeScale = 1;
        button.interactable = true;
        inGameMenu.SetActive(false);
        debugUI.SetActive(true);
        dashMeter.SetActive(true);
        menuOpen = false;
    }
    
}
