﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorDescent : MonoBehaviour {

	[Tooltip("The meteors initial speed")]
	public float initSpeed = 5;
	float speed;
	[Tooltip("How fast the speed should increment (if at all)")]
	public float speedIncrease = 0.1f;
	
	// Update is called once per frame
	void FixedUpdate () {
		//translate the meteor downwards with the given speed
		transform.Translate(-Vector3.up* speed * Time.deltaTime);
		//incremenent the speed by the given speed increase
		speed += speedIncrease;
	}

	//When the object is enabled in the hierarchy, set the bombs speed to the initial speed
	void OnEnable()
	{
		speed = initSpeed;
	}

}
