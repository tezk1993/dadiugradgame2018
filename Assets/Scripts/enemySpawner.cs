﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class enemySpawner : MonoBehaviour {
    [HideInInspector]
    public List<StateController> spawnedEnemies;
    public GameObject enemyPrefab;
    Vector3 spawnPoint;
    private GameObject enemyContainer;
    private List<StateController> enemiesScript;

    
    public EventManager eventManager;


    /// <summary>
    /// Enemy Spawn Variables
    /// </summary>
    [System.Serializable]
    public class enemyPoolSet
    {
        [SerializeField]
        public Color gizmoColor;
        [Range(0f, 30f)]
        public float spawnDelay;
        [Range(0f, 10f)]
        public float spawnRadius;
        public bool randomSpawn;
        public List<StateController.EnemyTypes> enemyPools;
        public List<GameObject> spawnPoints;

    }

    public List<enemyPoolSet> enemyPoolSets;

    [System.Serializable]
    public class enemySpawnSet
    {
        public List<StateController> enemyScript;
        public List<GameObject> enemySpawned;
    }


    public List<enemySpawnSet> enemySpawnSets;

    private void Start()
    {
        //eventManager = GameObject.FindGameObjectWithTag("EventManager").GetComponent<EventManager>();

        enemyContainer = gameObject.transform.GetChild(0).gameObject;

        CreateEnemies();



    }

    public void enemyDead()
    {
        eventManager.CheckEnemyCount();
    }


    private void OnDrawGizmos()
    {
        if(enemyPoolSets != null) { 
            for (int i = 0; i < enemyPoolSets.Count; i++)
            {
                for (int j = 0; j < enemyPoolSets[i].spawnPoints.Count; j++)
                {
                    if (enemyPoolSets[i].spawnPoints[j] != null)
                    {
                        Gizmos.color = Color.blue;
                        Gizmos.DrawSphere(enemyPoolSets[i].spawnPoints[j].transform.position, 1);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Enemy Spawn Functions
    /// </summary>
    void CreateEnemies()
    {
        
        for (int i = 0; i < enemyPoolSets.Count; i++)
        {
            var test_ = new List<GameObject>();
            var testScript_ = new List<StateController>();

            var _enemySpawnSet = new enemySpawnSet();
            enemySpawnSets.Add(_enemySpawnSet);
            for (int j = 0; j < enemyPoolSets[i].enemyPools.Count; j++)
            {
                GameObject Enemy = GameObject.Instantiate<GameObject>(enemyPrefab);
                Enemy.name = enemyPoolSets[i].enemyPools[j].ToString() + j.ToString();
                Enemy.SetActive(false);
                Enemy.transform.SetParent(enemyContainer.transform);
                testScript_.Add(Enemy.GetComponent<StateController>());
                test_.Add(Enemy);
                //testScript_[i].eventManager = eventManager;

            }
            enemySpawnSets[i].enemyScript = testScript_;
            enemySpawnSets[i].enemySpawned = test_;
            for (int j = 0; j < enemySpawnSets[i].enemyScript.Count; j++)
            {
                enemySpawnSets[i].enemyScript[j].enemyType = enemyPoolSets[i].enemyPools[j];
            }
        }
        for (int i = 0; i < enemySpawnSets.Count; i++)
        {
            for (int j = 0; j < enemySpawnSets[i].enemyScript.Count; j++)
            {
                enemySpawnSets[i].enemyScript[j].eventManager = eventManager;
            }

        }
    }


    public void StartSpawnEnemies(int spawnSetIndex)
    {
        StartCoroutine(spawnEnemies(spawnSetIndex));
    }

    [ContextMenu("Create SpawnPoints")]
    public void MakeSpawnPoints()
    {
        //Make Containers
        /*
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            if(gameObject.transform.GetChild(i).gameObject.name.Contains("SpawnPoint")) { 
                  Destroy(gameObject.transform.GetChild(i).gameObject);
            }
        }*/
        for (int i = 0; i < enemyPoolSets.Count; i++)
        {
            var Container = new GameObject("spawnPointContainer" + i.ToString());
            Container.name = "SpawnPointsContainer" + i.ToString();
            Container.transform.parent = gameObject.transform;
            for (int j = 0; j < enemyPoolSets[i].spawnPoints.Count; j++)
            {
                var spawnPoint = new GameObject("spawnPoint" + j.ToString());
                spawnPoint.transform.parent = Container.transform;
                enemyPoolSets[i].spawnPoints[j] = spawnPoint;
            }
         


        }
    }

    IEnumerator spawnEnemies(int spawnSetIndex)
    {
        for (int i = 0; i < enemySpawnSets[spawnSetIndex].enemySpawned.Count; i++)
        {
            if (enemySpawnSets[spawnSetIndex].enemySpawned[i].gameObject.GetComponent<StateController>().enemyType != StateController.EnemyTypes.Lost) { 
              eventManager.Enemies.Add(enemySpawnSets[spawnSetIndex].enemySpawned[i].gameObject.GetComponent<StateController>());
              eventManager.aliveEnemies.Add(enemySpawnSets[spawnSetIndex].enemySpawned[i].gameObject.GetComponent<StateController>());
            }
        }
        yield return new WaitForSeconds(0.1f);

        if (enemyPoolSets[spawnSetIndex].randomSpawn == true) { 
        RaycastHit hit;

            for (int i = 0; i < enemySpawnSets[spawnSetIndex].enemySpawned.Count;)
            {

                spawnPoint = (Random.insideUnitSphere* enemyPoolSets[spawnSetIndex].spawnRadius) + transform.position;
                spawnPoint.y = 0.5f;
                Debug.DrawRay(spawnPoint, Vector3.up,Color.red,10000f);
                    if (!Physics.Raycast(spawnPoint, Vector3.up,out hit))
                    {
                        enemySpawnSets[spawnSetIndex].enemySpawned[i].GetComponent<NavMeshAgent>().Warp(spawnPoint);

                        enemySpawnSets[spawnSetIndex].enemySpawned[i].transform.parent = null;
                        enemySpawnSets[spawnSetIndex].enemySpawned[i].gameObject.SetActive(true);

                        yield return new WaitForSeconds(enemyPoolSets[spawnSetIndex].spawnDelay);
                        i++;
                    }
                     else
                    {
               
                    }
            }
        }
        else
        {
            for (int i = 0; i < enemySpawnSets[spawnSetIndex].enemySpawned.Count;i++)
            {

                    enemySpawnSets[spawnSetIndex].enemySpawned[i].GetComponent<NavMeshAgent>().Warp(enemyPoolSets[spawnSetIndex].spawnPoints[i].gameObject.transform.position);
                    enemySpawnSets[spawnSetIndex].enemySpawned[i].transform.parent = null;
                    enemySpawnSets[spawnSetIndex].enemySpawned[i].gameObject.SetActive(true);
                   //eventManager.Enemies.Add(enemySpawnSets[spawnSetIndex].enemySpawned[i].GetComponent<StateController>());

                yield return new WaitForSeconds(enemyPoolSets[spawnSetIndex].spawnDelay);       
            }
        }
        //Debug.Log(spawnSetIndex);

    }


}
