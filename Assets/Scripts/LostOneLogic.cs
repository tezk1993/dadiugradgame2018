﻿/**
 * LostOneLogic.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MotionMatching), typeof(FieldOfView))]
public class LostOneLogic : MonoBehaviour
{
    #region Variables

    public float speed = 2.8f;
    public float updateDelay = 3;
    public float damageRange = 2;
    public bool runAway;
    public bool playerWithinRange;

    private MotionMatching motionMatching;
    private FieldOfView fieldOfView;
    public ParticleSystem deathParticles;

    private GameObject dangerZone;
    private float nextRandDirTime;

    #endregion

    #region Unity Methods

    public void Start()
    {
        fieldOfView = GetComponent<FieldOfView>();
        motionMatching = GetComponent<MotionMatching>();

        dangerZone = transform.Find("DangerZone").gameObject;
        dangerZone.transform.localScale = new Vector3(damageRange * 2, 0.25f, damageRange * 2);
        dangerZone.SetActive(false);

        runAway = Random.Range(0, 2) == 0;
        playerWithinRange = false;
    }

    public void Update()
    {        
        if (fieldOfView.visibleTargets.Count > 0)
        {
            ChasingPlayer();
        }
        else
        {
            WanderingAround();
        }

        var eulerAngles = transform.eulerAngles;
        eulerAngles.x = 0;
        eulerAngles.z = 0;
        transform.eulerAngles = eulerAngles;
    }

    #endregion

    #region Methods

    private void WanderingAround()
    {
        if (nextRandDirTime < Time.time)
        {
            nextRandDirTime = Time.time + updateDelay;

            float angle = Random.Range(0f, Mathf.PI * 2);
            bool stop = Random.Range(0, 5) == 0;
            var inputVelocity = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
            inputVelocity *= speed;
            if (stop) inputVelocity = Vector3.zero;
            motionMatching.targetVelocity = inputVelocity;
        }
    }

    private void ChasingPlayer()
    {
        var player = fieldOfView.visibleTargets[0];
        var playerDirection = (player.position - transform.position).normalized;

        if (!playerWithinRange)
        {
            if (runAway) playerDirection *= -1;
            motionMatching.targetVelocity = playerDirection * speed;

            if (Vector3.Magnitude(player.position - transform.position) <= damageRange && !runAway)
            {
                playerWithinRange = true;
                dangerZone.SetActive(true);
            }
        }
        else
        {
            motionMatching.targetVelocity = new Vector3(0, 0, 0);
            if (Vector3.Magnitude(player.position - transform.position) >= damageRange)
            {
                playerWithinRange = false;
                dangerZone.SetActive(false);
            }
        }
    }
    #endregion
}
