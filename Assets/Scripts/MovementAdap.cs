﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MovementAdap : MonoBehaviour {
    [Header("Tweakable values")]
    [Tooltip("Movement speed value")]
    public float startSpeed = 5;
    [Tooltip("Dash speed")]
    public float dashSpeed = 10;
    [Tooltip("Amount of time the dash lasts in seconds")]
    public float dashTime = 1;
    [Tooltip("Threshold of movement, lower value == more responsive (use higher values to avoid jitter)")]
    public float threshold = 2f;

    [Tooltip("The number of directions, evenly distributed to which the player can move")]
    public float movementDirectionCount = 8;

    [Tooltip("How long should the script wait before reseting the input position (higher values will allow slow finger movement to be recorded but will also reduce the adaptiveness of the joystick)")]
    public float inputResetDelay = 0.1f;

    [HideInInspector]
    public Vector2 dir;

    [HideInInspector]
    public float speed;

    [HideInInspector]
    public bool isDashing;

    private float lastMovedTime;
    
    private bool hasStartedMoving;
    private Vector2 startPos;
    private Vector2 curPos;

    private Animator animator;

    
    // Use this for initialization
    void Start ()
    {
        speed = startSpeed;
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        Movement();
    }

    void Movement()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 2 && Input.GetTouch(1).phase == TouchPhase.Began && !isDashing)
            {
                speed = dashSpeed;
                isDashing = true;
                Invoke("StopDash", dashTime);
                animator.SetTrigger("Attack");
            }
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                startPos = Input.GetTouch(0).position;
                hasStartedMoving = false;
                dir = curPos - startPos;
                //Debug.Log("StartPos: " + startPos + " CurrentPos: " + curPos);
                dir.Normalize();
                //Debug.Log("Dir = " + dir);

            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                // updating the reset start time to prevent a reset happen until the
                // reset delay has elapsed from now
                lastMovedTime = Time.unscaledTime;
                curPos = Input.GetTouch(0).position;
                
                if (Vector2.Distance(startPos, curPos) > threshold)
                {
                    dir = curPos - startPos;
                    hasStartedMoving = true;
                    startPos = Input.GetTouch(0).position;
                    dir.Normalize();
                    float angle = Vector2.SignedAngle(new Vector2(-1, 0), dir);

                    // [-1, 1]
                    float relativeAngle = angle / 180;
                    // [0, 1]
                    relativeAngle = (relativeAngle + 1) / 2;

                    // [0, movementDirectionCount] (integer)
                    float directionId = Mathf.Round(relativeAngle * movementDirectionCount);

                    // [0, 2*pi]
                    angle = (directionId / movementDirectionCount) * Mathf.PI * 2;
                    dir = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                if (lastMovedTime + inputResetDelay < Time.unscaledTime)
                {
                    startPos = Input.GetTouch(0).position;
                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                dir = Vector2.zero;
                startPos = Vector2.zero;
                curPos = Vector2.zero;
            }
        }

        var velocity = new Vector3(dir.x, 0, dir.y) * speed;

        bool isMoving = velocity.magnitude > 0.2f;
        if (isMoving)
        {
            animator.SetBool("Running", true);
            Debug.Log(velocity);
            transform.position += velocity * Time.deltaTime;
            transform.rotation = Quaternion.LookRotation(velocity.normalized, Vector3.up);
        }
        else
        {
            animator.SetBool("Running", false);
        }
    }

    void StopDash()
    {
        speed = startSpeed;
        isDashing = false;
    }
}
