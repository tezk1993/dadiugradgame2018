﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public GameObject magazine;

    public IEnumerator Reset(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.transform.parent = magazine.transform;
        GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        transform.localPosition = new Vector3(0, 0, 0);
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Terrain") || other.gameObject.CompareTag("DestructableTerrain"))
        {
            this.StopAllCoroutines();
            StartCoroutine(this.Reset(0));
        }
    }
}
