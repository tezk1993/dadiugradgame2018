﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
 
public class ComboScript : MonoBehaviour {

    public delegate void SpecialCounter();
    public static event SpecialCounter SpecialCount;

    private PlayerScriptJoystickDash movementScript;
    public int specialCombothreshold = 9;
    public float comboCounter;
    public float specialCounter;
    public float timeToKill;
    private float timer;
    private TextMeshProUGUI comboText;
    private Image comboUI;
    public ParticleSystem comboParticle;

    [System.Serializable]
    public class comboEffect{
        public UnityEvent enableEffect;
        public UnityEvent disableEffect;

        public int comboAmount;
        public bool active;
    }

    public List<comboEffect> comboEffects;

    // Use this for initialization
    void Start () {
        comboCounter = 0;
        specialCounter = 0;
        movementScript = GetComponent<PlayerScriptJoystickDash>();
        comboParticle = gameObject.transform.GetChild(5).gameObject.GetComponent<ParticleSystem>();
        var dashMeter = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Secondwind");
        comboText = dashMeter.transform.Find("ComboText").GetComponent<TextMeshProUGUI>();
        comboUI = dashMeter.transform.Find("ComboUI").GetComponent<Image>();
        comboText.gameObject.SetActive(false);
        comboUI.gameObject.SetActive(false);
        timeToKill = 5;
        timer = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            comboUI.material.SetFloat("_Fillpercentage", timer/timeToKill);
            if (timer < 0)
            {
                LoseCombo();
                comboParticle.Stop();
            }
        }
        if(comboCounter > 0)
        {
            comboText.gameObject.SetActive(true);
            comboUI.gameObject.SetActive(true);
        }
     
    }

    private void OnEnable()
    {
        PlayerScriptSplitJoystick.hit += ComboIncrease;
        PlayerScriptSplitJoystick.loseCombo += LoseCombo;
        PlayerScriptJoystickDash.hit += ComboIncrease;
        PlayerScriptJoystickDash.loseCombo += LoseCombo;
        Player.loseCombo += LoseCombo;
        Player.hit += ComboIncrease;


        //DestructiblePillar.hit += ComboIncrease;
    }

    private void OnDisable()
    {
        PlayerScriptSplitJoystick.hit -= ComboIncrease;
        PlayerScriptSplitJoystick.loseCombo -= LoseCombo;
        PlayerScriptJoystickDash.hit -= ComboIncrease;
        PlayerScriptJoystickDash.loseCombo -= LoseCombo;
        Player.loseCombo -= LoseCombo;
        Player.hit -= ComboIncrease;

        //DestructiblePillar.hit -= ComboIncrease;
    }

    void ComboIncrease()
    {
        specialCounter += 1;
        comboCounter += 1;
        AkSoundEngine.SetRTPCValue("Combo_Meter", comboCounter);
        comboText.text = comboCounter.ToString();
        timer = timeToKill;
        comboTrigger();

        if(specialCounter % specialCombothreshold == 0)
        {
            if (SpecialCount != null)
            {
                SpecialCount();
            }
        }
    }

    void LoseCombo()
    {
        specialCounter = 0;
        comboCounter = 0;
        AkSoundEngine.SetRTPCValue("Combo_Meter", comboCounter);
        timer = 0;
        comboText.gameObject.SetActive(false);
        comboUI.gameObject.SetActive(false);
        comboTrigger();

    }

    void comboTrigger()
    {
        for (int i = 0; i < comboEffects.Count; i++)
        {
            if(comboCounter >= comboEffects[i].comboAmount && !comboEffects[i].active)
            {
                comboEffects[i].enableEffect.Invoke();
                comboEffects[i].active = true;
            }
            else if (comboCounter <= comboEffects[i].comboAmount && comboEffects[i].active)
            {
                comboEffects[i].disableEffect.Invoke();
                comboEffects[i].active = false;
            }
        }
    }
}
