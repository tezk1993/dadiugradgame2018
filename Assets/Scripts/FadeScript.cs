﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeScript : MonoBehaviour {

    [HideInInspector]public Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void FadeOutIn()
    {
        animator.SetTrigger("FadeIn");
        animator.SetTrigger("FadeOut");
    }

    public void FadeOut()
    {
        animator.SetTrigger("FadeOut");
    }

    public void FadeIn()
    {
        animator.SetTrigger("FadeIn");
    }
    public void ActivateMenu(int menu)
    {
        transform.parent.GetComponent<MenuControl>().ActivateMenu(menu);
    }
    
}
