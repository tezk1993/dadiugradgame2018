﻿/**
 * WaterReflection.cs
 * Created by: Artur Barnabas
 * Created on: 07/11/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionCam : MonoBehaviour
{
    #region Variables

    public GameObject waterSurface;
    public Material waterTopMaterial;

    [HideInInspector]
    public RenderTexture reflectionRt;

    GameObject mainCam;

    #endregion

    #region Unity Methods

    public void Start()
    {
        var camera = GetComponent<Camera>();
        camera.CopyFrom(Camera.main);
        mainCam = Camera.main.gameObject;

        reflectionRt = new RenderTexture(
            Camera.main.pixelWidth, Camera.main.pixelHeight, 24, RenderTextureFormat.ARGB32
        );
        camera.targetTexture = reflectionRt;

        waterTopMaterial.SetTexture("_ReflectionTex", reflectionRt);
    }

    public void Update()
    {
        Vector3 mainCamPos = mainCam.transform.position;

        float yDiff = waterSurface.transform.position.y - mainCamPos.y;

        transform.position = new Vector3(mainCamPos.x, mainCamPos.y + 2 * yDiff, mainCamPos.z);
        Vector3 mainCamEulerAngles = mainCam.transform.localEulerAngles;
        transform.localEulerAngles = new Vector3(
            -mainCamEulerAngles.x, mainCamEulerAngles.y, mainCamEulerAngles.z
        );
    }

    #endregion

    #region Methods

    #endregion
}
