﻿/**
 * FollowObject.cs
 * Created by: Artur Barnabas
 * Created on: 29/10/18 (dd/mm/yy)
 *
 * This script will transform its game object in such a way that it will move smoothly
 * towards a position which is at a constant offset from the target.
 * In other words it 'tries' to keep its initial position relative to the target.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
	public Transform target;

	[Tooltip("Speed of the camera movement.")]
	public float speed = 3;

	
    [Tooltip("Higher values will result in more on-screen space in front of the player")]
    public float leadFactor = 2;

    public float maxLeadDistance = 1f;

	[HideInInspector]
	public Vector3 offset;

	Vector3 lastPlayerPos;

    private void Awake()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Use this for initialization
    void Start ()
	{
		offset = transform.position - target.position;
		lastPlayerPos = target.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		var newPlayerPos = target.position;
        var playerVelocity = (newPlayerPos - lastPlayerPos).magnitude / Time.deltaTime;
		lastPlayerPos = newPlayerPos;

        var distance = Mathf.Min(maxLeadDistance, playerVelocity * leadFactor);
        
		Vector3 goal = target.position + offset + target.forward * distance;
        if (Time.timeScale > 0)
        {
            transform.position = Vector3.Lerp(transform.position, goal, Time.deltaTime * speed);
        }
	}
}
