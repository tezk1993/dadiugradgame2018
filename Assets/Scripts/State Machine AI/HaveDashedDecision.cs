﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/HaveDashed")]
public class HaveDashedDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        bool dashReady = Read(controller);
        return dashReady;
    }

    private bool Read(StateController controller)
    {
        if (controller.haveDashed == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}