﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/TooClose")]
public class TooCloseDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool targetTooClose = Measure(controller);
        return targetTooClose;
    }

    private bool Measure(StateController controller)
    {
        if (Vector3.Magnitude(controller.gameObject.transform.position - controller.player.transform.position) <= controller.lostLostRange)
        {
            return true;
        }
        else
        {
            return false;

        }
    }
}
