﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Smash")]
public class SmashAction : Action {

    public override void Act(StateController controller,Animator enemyAnimator)
    {        
        Smash(controller,enemyAnimator);
    }

    private void Smash(StateController controller,Animator enemyAnimator)
    {
        if (controller.currentSmashCooldown <= 0.0f)
        {
            controller.haveSmashed = false;
            controller.StartCoroutine(controller.Smash());
        }
    }
}

