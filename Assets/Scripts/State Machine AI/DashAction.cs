﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Dash")]
public class DashAction : Action {

    public override void Act(StateController controller,Animator enemyAnimator)
    {        
        Dash(controller,enemyAnimator);
    }

    private void Dash(StateController controller,Animator enemyAnimator)
    {
        if (controller.currentDashCooldown <= 0.0f)
        {
            controller.haveDashed = false;
            controller.dashCoroutine = controller.StartCoroutine(controller.Dash(controller.dashCooldown,controller.dashSpeed,controller.dashDuration,controller.windupRotating,controller.windupNotRotating, false));
        }
    }
}

