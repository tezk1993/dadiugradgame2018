﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Chase")]
public class ChaseAction : Action
{
    public override void Act(StateController controller, Animator enemyAnimator)
    {
        Chase(controller, enemyAnimator);
    }

    private void Chase(StateController controller, Animator enemyAnimator)
    {
        if (controller.enemyType == StateController.EnemyTypes.Lost)
        {
            controller.navMeshAgent.speed = controller.lostRunSpeed;
        }

        controller.navMeshAgent.destination = controller.chaseTarget.position;
        enemyAnimator.SetBool("Running", true);
        
        controller.navMeshAgent.isStopped = false;
    }
}
