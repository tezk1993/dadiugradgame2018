﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Slash")]
public class SlashAction : Action {

    public override void Act(StateController controller, Animator enemyAnimator)
    {
        Smash(controller, enemyAnimator);
    }

    private void Smash(StateController controller, Animator enemyAnimator)
    {
        if (controller.currentSlashCooldown <= 0.0f)
        {
            controller.haveSlashed = false;

            controller.enemyAnimator.SetTrigger("Attack");
            controller.StartCoroutine(controller.Slash());
        }
    }
}
