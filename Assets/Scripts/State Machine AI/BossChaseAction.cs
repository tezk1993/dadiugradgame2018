﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/BossChase")]
public class BossChaseAction : Action
{
    public override void Act(StateController controller, Animator enemyAnimator)
    {
        //Debug.Log("Chasing player object @Pos: " + controller.navMeshAgent.destination);
        Chase(controller, enemyAnimator);
    }

    private void Chase(StateController controller, Animator enemyAnimator)
    {
        if (controller.isDoneWithSpecial)
        {
            controller.navMeshAgent.destination = controller.player.transform.position;
            enemyAnimator.SetBool("Running", true);
            controller.navMeshAgent.isStopped = false;
        
            if (controller.currentBossAttackCooldown <= 0.0f)
            {
                controller.isDoneWithSpecial = false;
                controller.ChooseSpecialAbility();

                if (controller.chosenAbility == 2 || controller.chosenAbility == 3)
                {
                    controller.UseSpecialAbility();
                }
                else if (controller.chosenAbility == 1 && Vector3.Magnitude(controller.gameObject.transform.position -
                    controller.player.transform.position) <= controller.bossDashRange ||
                    controller.chosenAbility == 5 && Vector3.Magnitude(controller.gameObject.transform.position -
                    controller.player.transform.position) <= controller.bossDashRange)
                {
                    controller.UseSpecialAbility();
                }
                else
                {
                    controller.StartCoroutine(controller.getWithinRange());
                }
            }
        }
    }
}
