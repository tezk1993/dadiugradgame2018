﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Shoot")]
public class ShootAction : Action
{
    public override void Act(StateController controller, Animator enemyAnimator)
    {
        Shoot(controller, enemyAnimator);
    }

    private void Shoot(StateController controller, Animator enemyAnimator)
    {
        if (controller.currentBulletCooldown <= 0.0f)
        {
            enemyAnimator.SetTrigger("Shoot");
            controller.StartCoroutine(controller.Fire());
            controller.currentBulletCooldown = controller.bulletCooldown;
        }
    }
}
