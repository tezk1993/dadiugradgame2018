﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/HaveSmashed")]
public class HaveSmashedDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        bool smashReady = Read(controller);
        return smashReady;
    }

    private bool Read(StateController controller)
    {
        if (controller.haveSmashed == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}