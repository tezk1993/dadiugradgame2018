﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/HaveSlashed")]
public class HaveSlashedDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool slashReady = Read(controller);
        return slashReady;
    }

    private bool Read(StateController controller)
    {
        if (controller.haveSlashed == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
