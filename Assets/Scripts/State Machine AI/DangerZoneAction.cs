﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Drain")]
public class DangerZoneAction : Action {

    public override void Act(StateController controller, Animator enemyAnimator)
    {
        if (controller.haveDangerZoned == false)
        {
            controller.StartCoroutine(controller.DangerZoneActivation());
        }
    }
}
