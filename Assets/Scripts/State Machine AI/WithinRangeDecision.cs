﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/WithinRange")]
public class WithinRangeDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool targetWithinRange = Meassure(controller);
        return targetWithinRange;
    }

    private bool Meassure(StateController controller)
    {
        if (Vector3.Magnitude(controller.gameObject.transform.position - 
            controller.player.transform.position) <= controller.attackRange)
        {
            return true;
        }        
        else
	    {
            return false;
        }
    }
}
