﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Homebrew;
using UnityEngine.UI;

public class StateController : MonoBehaviour
{
    [SerializeField]
    public enum EnemyTypes
    {
        Chaser,
        BasicShooter,
        SpreadShooter,
        NovaShooter,
        Guard,
        BigGuy,
        Mortar,
        Boss,
        Lost
    }
    public EnemyTypes enemyType;

    [Foldout("Main attributes", true)]
    [HideInInspector] public Animator enemyAnimator;
    public State currentState; //Enables us to assign the enemy any state.
    [HideInInspector] public State remainState; //Used when changing states in the AI.
    private State prevState; //Holds a saved state (used for stagger)
    public List<State> stateList; //Holds idle position for all enemies, used when instantiating enemies.
    [HideInInspector]
    public GameObject player; //Used for AI chase target


	public delegate void KillCount();
	public static event KillCount kills;

    public GameObject bulletPrefab;
    public GameObject mortarPrefab;
    public GameObject mortarDropZonePrefab;

    public Material deathMaterial;
    public List<Material> enemyMaterial;
    public List<SkinnedMeshRenderer> enemyRenderer;
    public RuntimeAnimatorController chaserAnimCtrl;
    public RuntimeAnimatorController guardAnimCtrl;
    public RuntimeAnimatorController basicShooterAnimCtrl;
    public RuntimeAnimatorController novaShooterAnimCtrl;
    [HideInInspector] public FieldOfView fieldOfView;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public int nextWayPoint;
    [HideInInspector] public bool dead;
    public EventManager eventManager;
    private Transform eyes; //Empty Gameobject to spawn debug graphs in.
    private GameObject magazine;
    public List<GameObject> bulletList;
    private GameObject mortarMagazine;
    public List<GameObject> mortarList;
    private GameObject mortarDropZoneMagazine;
    public List<GameObject> mortarDropZoneList;
    public List<RectTransform> mortarRendererList;
    private GameObject shield;
    private GameObject rotatingShield;
    private GameObject enemyModel;
    private GameObject bigGuyModel;
    private GameObject dangerZone;
    private GameObject bossModel;
    public GameObject clone;
    public ParticleSystem hitParticle;
    public ParticleSystem deathParticles;

    private GameObject smashZone;
    private GameObject smashZoneCollider;
    private Image smashZonebase;
    private Image smashZonefill;

    private GameObject slashZone;
    private GameObject slashZoneCollider;
    private Image slashZonebase;
    private Image slashZonefill;

    private GameObject crashZone;
    private GameObject crashZoneCollider;
    private Image crashZonebase;
    private Image crashZonefill;

    private Image dashZonebase;
    private Image dashZonefill;
    private GameObject dashDirection;

    [HideInInspector]
    public Transform chaseTarget, previousPos;

    [HideInInspector]
    public int enemyHP; //Saves the enemy HP, depending on type
    [HideInInspector]
    public float attackRange; //Used for AI check.
    private float moveSpeed; //Saves the enemy movespeed, depending on type
    private float viewAngle;
    private float viewRadius;

    #region boss specific attributes    
    [Foldout("Boss attributes", true)]
    [Tooltip ("How much interval there should be between special attacks. The inbetween attacks are normal dashes.")] 
    public int specialAbilityInterval;
    private int abilityCount = 1;
    public List<int> BossAbilityList;
    public List<int> bossAbilitySequence;
    [HideInInspector] public int chosenAbility;

    [Header("Boss Attributes")]
    public int bossHP;
    public int bossMovespeed;
    public float bossSpecialAbilityCooldown;
    [HideInInspector]
    public float currentBossAttackCooldown;
    public float bossStaggerDuration;
    public float bossExhaustDuration;
    [HideInInspector]
    public bool isDoneWithSpecial;
    private bool generatedAbilitySequence = false;
    private bool immuneToStagger;
    private bool setBossSoundState;

    [Header("- Mortar")]
    public int bossMortarAmount;
    [Tooltip("Delay between starting attack, till mortars start falling.")]
    public float bossMortarDelay;
    [Tooltip("Interval between mortars.")]
    public float bossMortarInterval;
    private bool bossMortarFireSound;
    public float bossMortarTravelTime;

    [Header("- Spreadshot")]
    public float bossSpreadWaveAmount;
    [Tooltip("Interval between waves.")]
    public float bossSpreadWaveInterval;
    [Tooltip("Interval between bullets.")]
    public float bossSpreadBulletInterval;
    [Tooltip("Degrees between bullets.")]
    public float bossSpreadDegree;

    [Header("- Disengage")]
    public int disengageDistance;
    public int disengageSpeed;
    private float disengageTime;
    private Vector3 disengageLocation;
    private bool haveLandedDisengage = false;

    /*
    [Header("- Rotating Shield")]
    public float rotatingShieldDuration;
    public float shieldRotateSpeed;
    [Tooltip("Time time untill the boss can use an ability again. Set this lower than Boss Attack Cooldown.")]
    public float rotatingShieldCooldownReduction;
    */

    [Header("- Dash")]
    public float bossDashRange;
    public float bossDashSpeed;
    public float bossDashDuration;
    public float bossWindupRotating;
    public float bossWindupNotRotating;
    [HideInInspector]
    private float bossDashCooldown; //Don't mind this, it is not used, but needed.

    [Header(" - Frenzy Dash")]
    public int frenzyAmount;
    [Tooltip("Added time between dashes")]
    public float bossFrenzyDashBuffer;
    private int currFrenzyAmount;
    private bool firstDash;
    private bool postFrenzyDashExhaustion;
    #endregion

    #region enemy specific attributes
    [Foldout("Enemy attributes", true)]
    [Header("Chaser Attributes")]
    public int chaserHP;
    public float chaserAttackRange;
    public float chaserMoveSpeed;
    public float chaserViewAngle;
    public float chaserViewRadius;
    public float slashCooldown;
    public float chaserExhaustDuration;
    [Tooltip("How long the charges charges up, before he slashes infront of him.")]
    public float slashChargeTime;
    [HideInInspector] public float currentSlashCooldown; //AI: Checking if the enemy should slash again
    [HideInInspector] public bool haveSlashed = false; //AI check;

    [Header("Basic Shooter Attributes")]
    public int basicShooterHP;
    public float basicShooterAttackRange;
    public float basicShooterMoveSpeed;
    public float basicShooterViewAngle;
    public float basicShooterViewRadius;
    public float basicShooterCoolDown;

    [Header("Spread Shooter Attributes")]
    public int spreadShooterHP;
    public float spreadShooterAttackRange;
    public float spreadShooterMoveSpeed;
    public float spreadShooterViewAngle;
    public float spreadShooterViewRadius;
    public float spreadShooterCoolDown;
    [Tooltip("amount of degrees trishots are in front and behind the player")]
    public float sideShotsDeg;

    [Header("Nova Shooter Attributes")]
    public int novaShooterHP;
    public float novaShooterAttackRange;
    public float novaShooterbulletDelay;
    public float novaShooterMoveSpeed;
    public float novaShooterViewAngle;
    public float novaShooterViewRadius;
    public float novaShooterCoolDown;
    public int novaBulletsPerShot;

    [Header("Mortar Shooter Attributes")]
    public int mortarShooterHP;
    public float mortarShooterAttackRange;
    public float mortarShooterMoveSpeed;
    public float mortarShooterViewAngle;
    public float mortarShooterViewRadius;
    public float mortarShooterCooldown;
    public float mortarShooterTravelTime;

    [Header("Guardian Attributes")]
    public int guardianHP;
    public float guardianAttackRange;
    public float guardianMoveSpeed;
    public float guardianViewAngle;
    public float guardianViewRadius;
    public float dashCooldown;
    public float guardianExhaustDuration;
    [HideInInspector] public float currentDashCooldown; //AI: Checking if the enemy should dash again
    public int dashSpeed;
    [Tooltip("Duration the guardian should be dashing.")]
    public float dashDuration;
    [Tooltip("Duration the guardian should look at the enemy while winding up its dash")]
    public float windupRotating;
    [Tooltip("Duration the guardian should stand still, before charging to the " +
             "last updated player position recorded while it was windin up.")]
    public float windupNotRotating;

    [Header("BigGuy Attributes")]
    public int bigGuyHP;
    public float bigGuyAttackRange;
    public float bigGuyMoveSpeed;
    public float bigGuyViewAngle;
    public float bigGuyViewRadius;
    public float smashCooldown;
    public float bigGuyExhaustDuration;
    [Tooltip("How long the big guy charges up, before he smashes the ground.")]
    public float smashChargeTime;
    [HideInInspector] public float currentSmashCooldown; //AI: Checking if the enemy should smash again
    [HideInInspector] public bool haveSmashed = false; //AI check;    

    [Header("Lost(tm) Attributes")]
    public int lostHP;
    public float lostAttackRange;
    [Tooltip("How far away the lost can get away from the player, before he's lost")]
    public float lostLostRange;
    public float lostRunSpeed;
    public float lostWalkSpeed;
    public float lostViewAngle;
    public float lostViewRadius;
    public List<Vector3> wayPointList; //Used for patrolling.
    public int wayPointAmount;
    public int mapSize;
    public int stoppingDistance;
    [HideInInspector] public bool haveDangerZoned = false;

    #endregion

    #region generic attributes
    [Foldout("Generic attributes", true)]
    public float staggerDuration;
    [HideInInspector]
    public float enemyInvis;
    private float invisDuration;
    private Coroutine staggerCoroutine;
    private float exhaustDuration;

    [Header("Shooting Attributes")]
    [Tooltip("the velocity of all bullets")]
    public float bulletSpeed;
    [Tooltip("time before the bullet is recovered")]
    public float bulletLifeTime;
    [HideInInspector]
    public float bulletCooldown;
    [HideInInspector]
    public float currentBulletCooldown; //AI: checking if the enemy should shoot again
    [Tooltip("keeping track of the amount of bullets shot")]
    private int shotsFired;
    private int mortarsFired;
    private bool standStill;
    private Vector3 targetDir;
    private Vector3 newDir;

    [Header("Mortar Attributes")]
    [Header("If settings that affect drop speed remember to correct telegraph accordingly")]
    [Tooltip("The height at which the meteor should spawn")]
    public float mortarSpawnHeight;
    [Tooltip("How imprecise the mortar shots are, set in units in a quadratic area.")]
    public float mortarImprecision;
    [Tooltip("How big the AoE the mortar has")]
    public float mortarAOEModifier;
    private float mortarTravelTime;
    private float currentTime;

    //Dashing Attributes, but they're all private or hidden
    [HideInInspector]
    public bool haveDashed = false; //AI check
    private bool dashing;
    private bool manualRotation; //Check used for a while loop
    private bool manualPosition; //Check used for a while loop
    private Vector3 oldPos; //Saves the enemy's old position, needed for the navmeshagent when forcing it to stand still
    private Vector3 targetOldPos; //Saves the player's old posititon, which is where the guardian will dash to
    private Vector3 dashdestination;
    [HideInInspector]
    public Coroutine dashCoroutine;
    #endregion

    void Awake() //Gets the enemy the different components it will later use.
    {
        fieldOfView = GetComponent<FieldOfView>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        eventManager = GameObject.FindGameObjectWithTag("EventManager").GetComponent<EventManager>();
        player = GameObject.FindGameObjectWithTag("Player");

        magazine = gameObject.transform.GetChild(1).gameObject;
        shield = gameObject.transform.GetChild(2).gameObject;

        smashZone = gameObject.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject;
        smashZonebase = gameObject.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<Image>();
        smashZonefill = gameObject.transform.GetChild(3).gameObject.transform.GetChild(0).gameObject.transform.GetChild(1).GetComponent<Image>();

        slashZone = gameObject.transform.GetChild(3).gameObject.transform.GetChild(1).gameObject;
        slashZonebase = gameObject.transform.GetChild(3).gameObject.transform.GetChild(1).gameObject.transform.GetChild(0).GetComponent<Image>();
        slashZonefill = gameObject.transform.GetChild(3).gameObject.transform.GetChild(1).gameObject.transform.GetChild(1).GetComponent<Image>();

        dashDirection = gameObject.transform.GetChild(3).gameObject.transform.GetChild(2).gameObject;
        dashZonebase = gameObject.transform.GetChild(3).gameObject.transform.GetChild(2).gameObject.transform.GetChild(0).GetComponent<Image>();
        dashZonefill = gameObject.transform.GetChild(3).gameObject.transform.GetChild(2).gameObject.transform.GetChild(1).GetComponent<Image>();

        crashZonebase = gameObject.transform.GetChild(3).gameObject.transform.GetChild(3).gameObject.transform.GetChild(0).GetComponent<Image>();
        crashZonefill = gameObject.transform.GetChild(3).gameObject.transform.GetChild(3).gameObject.transform.GetChild(1).GetComponent<Image>();
                
        smashZoneCollider = gameObject.transform.GetChild(4).gameObject;
        slashZoneCollider = gameObject.transform.GetChild(5).gameObject;

        mortarMagazine = gameObject.transform.GetChild(7).gameObject;
        mortarDropZoneMagazine = gameObject.transform.GetChild(8).gameObject;
        rotatingShield = gameObject.transform.GetChild(9).gameObject;
        enemyModel = gameObject.transform.GetChild(10).gameObject;
        bigGuyModel = gameObject.transform.GetChild(11).gameObject;
        dangerZone = gameObject.transform.GetChild(12).gameObject;
        bossModel = gameObject.transform.GetChild(13).gameObject;
        clone = Resources.Load("Enemy") as GameObject;

        invisDuration = player.GetComponent<Player>().dashDuration;

        if (enemyType == EnemyTypes.BasicShooter ||
           enemyType == EnemyTypes.SpreadShooter ||
           enemyType == EnemyTypes.NovaShooter ||
           enemyType == EnemyTypes.Mortar)
        {
            standStill = true; //Freezes movement of all shooter enemies - Always true for these
        }
    }

    void Update() //AI update
    {
        currentState.UpdateState(this);
        chaseTarget = player.transform;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            gotHit();
        }
    }

    private void FixedUpdate() //Reduces cooldown on enemy abilities, and handles dashing movement and rotation
    {
        currentBulletCooldown -= Time.deltaTime;
        currentDashCooldown -= Time.deltaTime;
        currentSmashCooldown -= Time.deltaTime;
        currentSlashCooldown -= Time.deltaTime;
        enemyInvis -= Time.deltaTime;
        currentTime = Time.time;

        currentBossAttackCooldown -= Time.deltaTime;
        disengageTime -= Time.deltaTime; //A failsafe, incase the boss bumpes into a wall while disengaging, 
                                         //as the disengage coroutine is otherwise only stopped when a distance have been traveled

        if (standStill) //Used to force enemies to stand still
        {
            targetDir = player.transform.position - transform.position;
            newDir = Vector3.RotateTowards(transform.forward, targetDir, Time.deltaTime, 0);
            transform.rotation = Quaternion.LookRotation(newDir);
        }

        //All three if-statements are used in the Windup/Dash coroutines.
        if (manualRotation)
        {
            if (navMeshAgent.velocity.normalized != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(navMeshAgent.velocity.normalized);
            }
        }
        if (manualPosition)
        {
            transform.position = oldPos;
        }
        if (dashing)
        {
            navMeshAgent.destination = new Vector3(dashdestination.x, 0, dashdestination.z);
        }
    }

    #region boss specific functions

    public void ChooseSpecialAbility()
    {
        if (abilityCount % specialAbilityInterval == 0) //Ensures that 2 out of specialBbilityInterval is the normal dash attack
        {
            if (!generatedAbilitySequence) //Generate random sequence
            {
                BossAbilityList.Add(1);
                BossAbilityList.Add(2);
                BossAbilityList.Add(3);
                var count = BossAbilityList.Count;

                for (int i = 0; i < count; i++)
                {
                    int randomNumber = Random.Range(0, BossAbilityList.Count);
                    bossAbilitySequence.Add(BossAbilityList[randomNumber]);
                    BossAbilityList.RemoveAt(randomNumber);
                }
                generatedAbilitySequence = true;
                ChooseSpecialAbility();
            }
            else
            {
                if (bossAbilitySequence.Count == 1)  //if the ability sequence is only 1 big, empty it and recreate it
                {
                    generatedAbilitySequence = false;
                    bossAbilitySequence.RemoveAt(0);
                    ChooseSpecialAbility();
                }
                else //Take first ability in the sequence and uses
                {
                    chosenAbility = bossAbilitySequence[0];
                    bossAbilitySequence.RemoveAt(0);
                    abilityCount++;
                }
            }
        }
        else //Do the dash!
        {
            abilityCount++;
            chosenAbility = 4;
        }
    }

    public void UseSpecialAbility()
    {
        if (!setBossSoundState)
        {
            setBossSoundState = true;
            AkSoundEngine.SetState("Boss_Phases", "Phase1");
        }

        currentBossAttackCooldown = bossSpecialAbilityCooldown;
        if (chosenAbility == 1)
        {
            StartCoroutine(FrenzyDash(bossDashCooldown, bossDashSpeed, bossDashDuration, bossWindupRotating, bossWindupNotRotating));
        }
        else if (chosenAbility == 2)
        {
            StartCoroutine(Disengage(true)); //boss disengages and then does mortar
        }
        else if (chosenAbility == 3)
        {
            StartCoroutine(Disengage(false)); //boss disengages and then does spread shooting
        }
        else
        {
            StartCoroutine(Dash(bossDashCooldown, bossDashSpeed, bossDashDuration, bossWindupRotating, bossWindupNotRotating, true));
        }
    }

    public IEnumerator Disengage(bool mortar)
    {
        AkSoundEngine.PostEvent("Play_Jump_Boss", gameObject);

        navMeshAgent.isStopped = true; //stops updating the navmesh, as we want to move the boss independently of it.

        disengageLocation = transform.position - (transform.forward * disengageDistance);
        haveLandedDisengage = false;
        navMeshAgent.updateRotation = false;
        disengageTime = 0.25f;

        while (!haveLandedDisengage) //Keeps moving backwards, untill it's close to the disengageLocation, or disengageTime has passed
        {
            transform.position = Vector3.MoveTowards(transform.position, disengageLocation, disengageSpeed);

            if (Vector3.Magnitude(gameObject.transform.position -
            disengageLocation) <= 0.5 || disengageTime <= 0.0f)
            {
                AkSoundEngine.PostEvent("Play_Land_Boss", gameObject);
                haveLandedDisengage = true;
            }
            yield return null;
        }

        navMeshAgent.updateRotation = true;
        StartCoroutine(BossFireShot(mortar));

    }

    public IEnumerator getWithinRange() //Gets withinrange of the play, then the boss uses its dash/fDash attack
    {
        while (Vector3.Magnitude(gameObject.transform.position - player.transform.position) >= bossDashRange)
        {
            navMeshAgent.destination = chaseTarget.position;
            enemyAnimator.SetBool("Running", true);

            yield return null;
        }
        UseSpecialAbility();
    }

    public IEnumerator FrenzyDash(float dashCooldown, float dashSpeed, float dashDuration, float windupRotating, float windupNotRotating, bool bossEnemy = true, bool fDash = true)
    {
        bossDashRange = 1000; //Set arbetrarily high
        firstDash = true;
        for (currFrenzyAmount = 0; currFrenzyAmount < frenzyAmount; currFrenzyAmount++)
        {
            StartCoroutine(Dash(dashCooldown, dashSpeed, dashDuration, windupRotating, windupNotRotating, bossEnemy, fDash));

            yield return new WaitForSeconds(dashDuration + windupRotating + windupNotRotating + bossFrenzyDashBuffer);
        }
        currFrenzyAmount = 0;
        bossDashRange = 7;
    }

    //Used by the boss to fire projectiles, both mortars and bullets.
    public IEnumerator BossFireShot(bool shootingMortar = false)
    {
        navMeshAgent.isStopped = true;

        standStill = true; //Freezes boss Movement
        immuneToStagger = true;

        //Reset bullet/mortar reference, in case the end of the respective magazines have been hit - Object pooling.
        if (shotsFired >= bulletList.Count)
        {
            shotsFired = 0;
        }
        if (mortarsFired >= mortarList.Count)
        {
            mortarsFired = 0;
        }

        if (shootingMortar)
        {
            AkSoundEngine.PostEvent("Play_Mass_Windup_Boss", gameObject);

            yield return new WaitForSeconds(bossMortarDelay);

            immuneToStagger = false;
            standStill = false; //Allows boss to move normaly again after firing mortars
            isDoneWithSpecial = true; //AI: Boss ability is now on cooldown
            currentBossAttackCooldown = bossSpecialAbilityCooldown;

            for (int i = 0; i < bossMortarAmount; i++) //Fires mortars at the play, with a certain imprecision
            {
                GameObject mortar = mortarList[mortarsFired];
                mortar.SetActive(true);
                mortar.transform.parent = null;

                GameObject mortarDropZone = mortarDropZoneList[mortarsFired];
                mortarDropZone.SetActive(true);
                mortarDropZone.transform.parent = null;

                var hitLocation = new Vector3(player.transform.position.x + Random.Range(-mortarImprecision, mortarImprecision),  //x pos
                                              0,                                                                                  //y pos
                                              player.transform.position.z + Random.Range(-mortarImprecision, mortarImprecision)); //z pos

                var spawnLocation = new Vector3(transform.position.x, 1, transform.position.z);

                mortar.transform.position = spawnLocation;
                mortarDropZone.transform.position = hitLocation;
                bossMortarFireSound = true;
                StartCoroutine(FallingMortar(mortarsFired, spawnLocation, hitLocation, mortarTravelTime, currentTime));
                mortarsFired++;

                yield return new WaitForSeconds(bossMortarInterval);
            }
        }
        else
        {
            AkSoundEngine.PostEvent("Play_Spread_Windup_Boss", gameObject);

            var dir = 1;
            yield return new WaitForSeconds(0.5f);
            for (int i = 0; i < bossSpreadWaveAmount; i++)
            {                
                for (int j = -3; j < 4; j++)
                {
                    AkSoundEngine.PostEvent("Play_Spread_Shoot_Boss", gameObject);
                    GameObject bullet = bulletList[shotsFired];
                    bullet.SetActive(true);
                    bullet.transform.parent = null;
                    Vector3 interceptVector = FindInterceptVector(transform.position, bulletSpeed, player.transform.position, player.GetComponent<Rigidbody>().velocity);
                    Vector3 frontInterceptVector = Quaternion.AngleAxis(bossSpreadDegree * j * dir, Vector3.up) * interceptVector;
                    bullet.GetComponent<Rigidbody>().velocity = frontInterceptVector;
                    StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                    shotsFired++;
                    yield return new WaitForSeconds(bossSpreadBulletInterval);
                }

                dir *= -1;
                yield return new WaitForSeconds(bossSpreadWaveInterval);
            }

            immuneToStagger = false;
            standStill = false; //Allows boss to move normaly again after shooting
            isDoneWithSpecial = true; //AI: Boss ability is now on cooldown
            currentBossAttackCooldown = bossSpecialAbilityCooldown;
            shotsFired = 0;
        }
    }

    #endregion

    #region enemy specific functions    

    //Used by all ranged enemies to shoot at the player.
    public IEnumerator Fire()
    {
        if (shotsFired >= bulletList.Count)
        {
            shotsFired = 0;
        }
        if (mortarsFired >= mortarList.Count)
        {
            mortarsFired = 0;
        }

        if (enemyType == EnemyTypes.Mortar)
        {
            GameObject mortar = mortarList[mortarsFired];
            mortar.SetActive(true);
            mortar.transform.parent = null;

            GameObject mortarDropZone = mortarDropZoneList[mortarsFired];
            mortarDropZone.SetActive(true);
            mortarDropZone.transform.parent = null;

            var hitLocation = new Vector3(player.transform.position.x + Random.Range(-mortarImprecision, mortarImprecision),  //x pos
                                          0,                                                                                  //y pos
                                          player.transform.position.z + Random.Range(-mortarImprecision, mortarImprecision)); //z pos

            var spawnLocation = new Vector3(transform.position.x, 1, transform.position.z);
            
            mortar.transform.position = spawnLocation;
            mortarDropZone.transform.position = hitLocation;

            StartCoroutine(FallingMortar(mortarsFired, spawnLocation, hitLocation, mortarTravelTime, currentTime));

            mortarsFired++;
            StartCoroutine(ShooterWindupSound("Nova"));
        }
        else
        {
            if (enemyType != EnemyTypes.NovaShooter)
            {
                AkSoundEngine.PostEvent("Play_Shooter_Attack_Shoot", gameObject);
                GameObject bullet = bulletList[shotsFired];
                bullet.SetActive(true);
                bullet.transform.parent = null;
                Vector3 interceptVector = FindInterceptVector(transform.position, bulletSpeed, player.transform.position, player.GetComponent<Rigidbody>().velocity);
                bullet.GetComponent<Rigidbody>().velocity = interceptVector;
                StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                shotsFired++;
                if (enemyType == EnemyTypes.SpreadShooter)
                {

                    //in front of player
                    bullet = bulletList[shotsFired];
                    bullet.SetActive(true);
                    Vector3 frontInterceptVector = Quaternion.AngleAxis(sideShotsDeg, Vector3.up) * interceptVector;
                    frontInterceptVector = frontInterceptVector.normalized * bulletSpeed;
                    bullet.GetComponent<Rigidbody>().velocity = frontInterceptVector;
                    StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                    shotsFired++;
                    //behind player
                    bullet = bulletList[shotsFired];
                    bullet.SetActive(true);
                    Vector3 behindInterceptVector = Quaternion.AngleAxis(-sideShotsDeg, Vector3.up) * interceptVector;
                    behindInterceptVector = behindInterceptVector.normalized * bulletSpeed;
                    bullet.GetComponent<Rigidbody>().velocity = behindInterceptVector;
                    StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                    shotsFired++;
                }
                StartCoroutine(ShooterWindupSound("Shooter"));
            }
            else if (enemyType == EnemyTypes.NovaShooter)
            {
                for (int i = 0; i < novaBulletsPerShot; i++)
                {
                    yield return new WaitForSeconds(novaShooterbulletDelay);
                    AkSoundEngine.PostEvent("Play_Shooter_Attack_Shoot", gameObject);
                    GameObject bullet = bulletList[shotsFired];
                    bullet.SetActive(true);
                    Vector3 direction = Quaternion.Euler(new Vector3(0, 360 / novaBulletsPerShot * i, 0)) * Vector3.forward;
                    bullet.GetComponent<Rigidbody>().velocity = direction * bulletSpeed;
                    StartCoroutine(bullet.GetComponent<Bullet>().Reset(bulletLifeTime));
                    shotsFired++;
                }
                StartCoroutine(ShooterWindupSound("Shooter"));
            }
        }
        yield return new WaitForSeconds(bulletLifeTime);
    }

    //Used to make the big guy enemy aoe smash the ground
    public IEnumerator Smash()
    {
        haveSmashed = false; //AI: Check if smash on cooldown

        fieldOfView.viewRadius = 1000; //Sat arbitrarily high to avoid the enemy to lose sight of the player
        fieldOfView.obstacleMask = LayerMask.GetMask("Nothing"); //enables the enemy to see through walls

        currentSmashCooldown = 1000; //Sat arbitrarily high

        navMeshAgent.speed = 0;
        smashZone.SetActive(true);
        smashZoneCollider.SetActive(true); //Indicate where the smash is going to hit

        smashZonefill.fillAmount = 0;

        StartCoroutine(fillTelegraph(smashZonefill, smashChargeTime));

        yield return new WaitForSeconds(smashChargeTime);

        smashZoneCollider.tag = "Hazard"; //Makes it so that the player takes damage if its in the area

        yield return new WaitForSeconds(0.05f);

        //Reset enemy back to previous variables
        smashZoneCollider.tag = "Untagged";
        smashZoneCollider.SetActive(false);
        smashZone.SetActive(false);

        fieldOfView.viewRadius = 20;
        fieldOfView.obstacleMask = LayerMask.GetMask("Obstacles");
        currentSmashCooldown = smashCooldown;
        haveSmashed = true;
        navMeshAgent.speed = bigGuyMoveSpeed;

        staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
    }

    //Used to make the chaser enemy aoe slash infront of him
    public IEnumerator Slash()
    {
        haveSlashed = false; //AI: Check if slash on cooldown

        fieldOfView.viewRadius = 1000; //Sat arbitrarily high to avoid the enemy to lose sight of the player
        fieldOfView.obstacleMask = LayerMask.GetMask("Nothing"); //enables the enemy to see through walls

        currentSlashCooldown = 1000; //Sat arbitrarily high

        navMeshAgent.speed = 0;
        slashZone.SetActive(true); //Indicate where the slash is going to hit
        slashZoneCollider.SetActive(true);

        AkSoundEngine.PostEvent("Play_Chaser_attack_Windup", gameObject);
        slashZonefill.fillAmount = 0;

        StartCoroutine(fillTelegraph(slashZonefill, slashChargeTime));

        yield return new WaitForSeconds(slashChargeTime);

        slashZoneCollider.tag = "Hazard"; //Makes it so that the player takes damage if its in the area
        AkSoundEngine.PostEvent("Play_Chaser_Attack_Strike", gameObject);

        yield return new WaitForSeconds(0.05f);

        //Reset enemy back to previous variables
        slashZone.tag = "Untagged";
        slashZone.SetActive(false);
        slashZoneCollider.tag = "Untagged";
        slashZoneCollider.SetActive(false);

        navMeshAgent.speed = chaserMoveSpeed;
        fieldOfView.viewRadius = chaserViewRadius;
        fieldOfView.obstacleMask = LayerMask.GetMask("Obstacles");
        currentSlashCooldown = slashCooldown;
        haveSlashed = true;

        staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
    }

    public IEnumerator DangerZoneActivation()
    {
        haveDangerZoned = true;
        AkSoundEngine.PostEvent("Play_Lost_Attack", gameObject);

        //Forces the lost to stand still while the play is near to it. All damage will be done via scripts on the dangerzone object.
        while (Vector3.Magnitude(gameObject.transform.position - player.transform.position) 
            <= attackRange)
        {
            dangerZone.SetActive(true);
            standStill = true;

            yield return null;
        }
        navMeshAgent.speed = lostWalkSpeed;
        dangerZone.SetActive(false);
        standStill = false; //Frees the Lost movement
        haveDangerZoned = false;
    }

    #endregion

    #region general functions

    //Assigns stats to the enemy, depending on its type
    public void AssignEnemyType()
    {
        if (enemyType == EnemyTypes.Chaser)
        {
            Material[] tempMat = new Material[] { enemyMaterial[0] };
            enemyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[0];
            enemyHP = chaserHP;
            attackRange = chaserAttackRange;
            moveSpeed = chaserMoveSpeed;
            viewRadius = chaserViewRadius;
            viewAngle = chaserViewAngle;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            currentSlashCooldown = slashCooldown;
            enemyAnimator.runtimeAnimatorController = chaserAnimCtrl;
            exhaustDuration = chaserExhaustDuration;
        }
        else if (enemyType == EnemyTypes.BasicShooter)
        {
            Material[] tempMat = new Material[] { enemyMaterial[1] };
            enemyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[2];
            enemyHP = basicShooterHP;
            attackRange = basicShooterAttackRange;
            bulletCooldown = basicShooterCoolDown;
            moveSpeed = basicShooterMoveSpeed;
            viewRadius = basicShooterViewRadius;
            viewAngle = basicShooterViewAngle;
            shotsFired = 0;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            enemyAnimator.runtimeAnimatorController = basicShooterAnimCtrl;

            //Instantiates bullets, based on fire rate and lifetime
            for (int i = 0; i < bulletLifeTime / bulletCooldown; i++)
            {
                var bullet = Instantiate(bulletPrefab) as GameObject;
                bullet.GetComponent<Bullet>().magazine = magazine;
                bullet.transform.position = magazine.transform.position;
                bullet.transform.SetParent(magazine.transform);
            }

            //adds the bullets to a list and sets them false. They are sat true when shot.
            for (int i = 0; i < magazine.transform.childCount; i++)
            {
                bulletList.Add(magazine.transform.GetChild(i).gameObject);
                bulletList[i].gameObject.SetActive(false);
            }
        }
        else if (enemyType == EnemyTypes.SpreadShooter)
        {
            Material[] tempMat = new Material[] { enemyMaterial[2] };
            enemyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[2];
            enemyHP = spreadShooterHP;
            attackRange = spreadShooterAttackRange;
            bulletCooldown = spreadShooterCoolDown;
            moveSpeed = spreadShooterMoveSpeed;
            viewRadius = spreadShooterViewRadius;
            viewAngle = spreadShooterViewAngle;
            shotsFired = 0;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            enemyAnimator.runtimeAnimatorController = basicShooterAnimCtrl;

            for (int i = 0; i < bulletLifeTime / bulletCooldown * 3; i++)
            {
                var bullet = Instantiate(bulletPrefab) as GameObject;
                bullet.GetComponent<Bullet>().magazine = magazine;
                bullet.transform.position = magazine.transform.position;
                bullet.transform.SetParent(magazine.transform);
            }

            for (int i = 0; i < magazine.transform.childCount; i++)
            {
                bulletList.Add(magazine.transform.GetChild(i).gameObject);
                bulletList[i].gameObject.SetActive(false);
            }
        }
        else if (enemyType == EnemyTypes.NovaShooter)
        {
            Material[] tempMat = new Material[] { enemyMaterial[3] };
            enemyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[2];
            enemyHP = novaShooterHP;
            attackRange = novaShooterAttackRange;
            bulletCooldown = novaShooterCoolDown;
            moveSpeed = novaShooterMoveSpeed;
            viewRadius = novaShooterViewRadius;
            viewAngle = novaShooterViewAngle;
            shotsFired = 0;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            enemyAnimator.runtimeAnimatorController = novaShooterAnimCtrl;

            for (int i = 0; i < bulletLifeTime / bulletCooldown * novaBulletsPerShot; i++)
            {
                var bullet = Instantiate(bulletPrefab) as GameObject;
                bullet.GetComponent<Bullet>().magazine = magazine;
                bullet.transform.position = magazine.transform.position;
                bullet.transform.SetParent(magazine.transform);
            }

            for (int i = 0; i < magazine.transform.childCount; i++)
            {
                bulletList.Add(magazine.transform.GetChild(i).gameObject);
                bulletList[i].gameObject.SetActive(false);
            }
        }
        else if (enemyType == EnemyTypes.Guard)
        {
            Material[] tempMat = new Material[] { enemyMaterial[4] };
            bigGuyModel.SetActive(true);
            bigGuyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[1];
            enemyHP = guardianHP;
            attackRange = guardianAttackRange;
            moveSpeed = guardianMoveSpeed;
            viewRadius = guardianViewRadius;
            viewAngle = guardianViewAngle;
            shield.SetActive(true);
            rotatingShield.SetActive(false);
            currentDashCooldown = dashCooldown;
            enemyAnimator.runtimeAnimatorController = guardAnimCtrl;
            exhaustDuration = guardianExhaustDuration;
        }
        else if (enemyType == EnemyTypes.BigGuy)
        {
            Material[] tempMat = new Material[] { enemyMaterial[5] };
            bigGuyModel.SetActive(true);
            bigGuyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[3];
            enemyHP = bigGuyHP;
            attackRange = bigGuyAttackRange;
            moveSpeed = bigGuyAttackRange;
            viewRadius = bigGuyViewRadius;
            viewAngle = bigGuyViewAngle;
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            currentSmashCooldown = smashCooldown;
            enemyAnimator.runtimeAnimatorController = chaserAnimCtrl;
            exhaustDuration = bigGuyExhaustDuration;
        }
        else if (enemyType == EnemyTypes.Mortar)
        {
            Material[] tempMat = new Material[] { enemyMaterial[6] };
            enemyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials = tempMat;
            currentState = stateList[2];
            enemyHP = mortarShooterHP;
            attackRange = mortarShooterAttackRange;
            bulletCooldown = mortarShooterCooldown;
            mortarTravelTime = mortarShooterTravelTime;
            moveSpeed = mortarShooterMoveSpeed;
            viewRadius = mortarShooterViewRadius;
            viewAngle = mortarShooterViewAngle;
            fieldOfView.obstacleMask = LayerMask.GetMask("Untagged");
            shotsFired = 0;
            gameObject.transform.localScale = new Vector3(1, 1, 1);
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            enemyAnimator.runtimeAnimatorController = basicShooterAnimCtrl;

            for (int i = 0; i < 5; i++)
            {
                var mortar = Instantiate(mortarPrefab) as GameObject;
                mortar.transform.position = mortarMagazine.transform.position;
                mortar.transform.SetParent(mortarMagazine.transform);

                var mortarDropZone = Instantiate(mortarDropZonePrefab) as GameObject;
                mortarDropZone.transform.localScale = new Vector3(mortarDropZone.transform.localScale.x * mortarAOEModifier,
                                                                  mortarDropZone.transform.localScale.y,
                                                                  mortarDropZone.transform.localScale.z * mortarAOEModifier);
                mortarDropZone.transform.position = mortarDropZoneMagazine.transform.position;
                mortarDropZone.transform.SetParent(mortarDropZoneMagazine.transform);
                mortarRendererList.Add(mortarDropZone.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<RectTransform>());
            }

            for (int i = 0; i < mortarMagazine.transform.childCount; i++)
            {
                mortarList.Add(mortarMagazine.transform.GetChild(i).gameObject);
                mortarList[i].gameObject.SetActive(false);

                mortarDropZoneList.Add(mortarDropZoneMagazine.transform.GetChild(i).gameObject);
                mortarDropZoneList[i].gameObject.SetActive(false);
            }

        }
        else if (enemyType == EnemyTypes.Boss)
        {
            currentState = stateList[5];
            enemyHP = bossHP;
            moveSpeed = bossMovespeed;
            currentBossAttackCooldown = bossSpecialAbilityCooldown;
            mortarTravelTime = bossMortarTravelTime;
            bulletLifeTime = 5;
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            enemyAnimator.runtimeAnimatorController = chaserAnimCtrl;
            exhaustDuration = bossExhaustDuration;

            isDoneWithSpecial = true;
            BossAbilityList = new List<int>();

            //Instantiates bullets, based on fire rate and lifetime
            for (int i = 0; i < bossSpreadWaveAmount * 7; i++)
            {
                var bullet = Instantiate(bulletPrefab) as GameObject;
                bullet.GetComponent<Bullet>().magazine = magazine;
                bullet.transform.position = magazine.transform.position;
                bullet.transform.SetParent(magazine.transform);
            }

            //adds the bullets to a list and sets them false. They are sat true when shot.
            for (int i = 0; i < magazine.transform.childCount; i++)
            {


                bulletList.Add(magazine.transform.GetChild(i).gameObject);
                bulletList[i].gameObject.SetActive(false);
            }

            //Instantiates mortars and dropzones - the dropzones are irrelevant when the telegraphin is fixed.
            for (int i = 0; i < bossMortarAmount; i++)
            {
                var mortar = Instantiate(mortarPrefab) as GameObject;
                mortar.transform.position = mortarMagazine.transform.position;
                mortar.transform.SetParent(mortarMagazine.transform);
                var mortarDropZone = Instantiate(mortarDropZonePrefab) as GameObject;
                mortarDropZone.transform.localScale = new Vector3(mortarDropZone.transform.localScale.x * mortarAOEModifier,
                                                                  mortarDropZone.transform.localScale.y,
                                                                  mortarDropZone.transform.localScale.z * mortarAOEModifier);
                mortarDropZone.transform.position = mortarDropZoneMagazine.transform.position;
                mortarDropZone.transform.SetParent(mortarDropZoneMagazine.transform);
                mortarRendererList.Add(mortarDropZone.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<RectTransform>());
            }

            //adds the mortars and dropzones to respective magazines.
            for (int i = 0; i < mortarMagazine.transform.childCount; i++)
            {
                mortarList.Add(mortarMagazine.transform.GetChild(i).gameObject);
                mortarList[i].gameObject.SetActive(false);

                mortarDropZoneList.Add(mortarDropZoneMagazine.transform.GetChild(i).gameObject);
                mortarDropZoneList[i].gameObject.SetActive(false);
            }
        }
        else if (enemyType == EnemyTypes.Lost)
        {
            enemyModel.gameObject.transform.Find("Shadow").gameObject.transform.GetComponent<SkinnedMeshRenderer>().materials[0] = enemyMaterial[6];
            enemyHP = lostHP;
            moveSpeed = lostWalkSpeed;
            viewRadius = lostViewRadius;
            viewAngle = lostViewAngle;
            attackRange = lostAttackRange;
            shield.SetActive(false);
            rotatingShield.SetActive(false);
            dangerZone.transform.localScale = new Vector3(lostAttackRange * 2, 0.25f, lostAttackRange * 2);
            currentState = stateList[6];

            for (int i = 0; i < wayPointAmount; i++)
            {
                var waypoint = new Vector3(Random.Range(-mapSize / 2, mapSize / 2),
                                           0,
                                           Random.Range(-mapSize / 2, mapSize / 2));
                wayPointList.Add(waypoint);
            }
        }

        oldPos = transform.position;
        currentState.enemyAnimator = enemyAnimator;
        navMeshAgent.speed = moveSpeed;
        fieldOfView.viewAngle = viewAngle;
        fieldOfView.viewRadius = viewRadius;
        smashZone.SetActive(false);
        slashZone.SetActive(false);
        dashDirection.SetActive(false);
        dangerZone.SetActive(false);
    }

    //Reduces enemy hp, called by the player script
    public void gotHit()
    {
        if (enemyInvis <= 0.0f)
        {
            enemyInvis = invisDuration;
            enemyHP--;
            enemyAnimator.SetTrigger("Hit");
            hitParticle.Play();

            if (enemyType == EnemyTypes.Boss)
            {
                AkSoundEngine.PostEvent("Play_Hit_Boss", gameObject);
                eventManager.checkBossHealth();

                Debug.Log("took damage, now at hp: " + enemyHP);
                if (enemyHP == 6)
                {
                    AkSoundEngine.PostEvent("Play_Mirror_Image_Boss", gameObject);
                    AkSoundEngine.SetState("Boss_Phases", "Phase2");
                    Debug.Log("Spawning first clone");
                    staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
                    var clone1 = Instantiate(clone, gameObject.transform.position + new Vector3(1, 0, 0), this.transform.rotation);
                    clone1.GetComponent<StateController>().enemyHP = 6;
                    clone1.GetComponent<StateController>().StartCoroutine(Stagger(staggerDuration));
                }
                else if (enemyHP == 3)
                {
                    AkSoundEngine.PostEvent("Play_Mirror_Image_Boss", gameObject);
                    AkSoundEngine.SetState("Boss_Phases", "Phase3");
                    Debug.Log("Spawning second clone");
                    staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
                    var clone2 = Instantiate(clone, gameObject.transform.position + new Vector3(-1, 0, 0), this.transform.rotation);
                    clone2.GetComponent<StateController>().enemyHP = 3;
                    clone2.GetComponent<StateController>().StartCoroutine(Stagger(staggerDuration));
                }
                else if (enemyHP <= 0)
                {
                    AkSoundEngine.SetState("Boss_Phases", "None");
                    staggerCoroutine = StartCoroutine(Stagger(10));
                    StartCoroutine(Kill());
                }
                else if (postFrenzyDashExhaustion)
                {
                    isDoneWithSpecial = true; //AI: Boss ability is now on cooldown
                    Disengage(false);
                }
                else if (!immuneToStagger && !postFrenzyDashExhaustion)
                {
                    staggerCoroutine = StartCoroutine(Stagger(bossStaggerDuration));
                }
                
            }
            else
            {
                if (enemyHP <= 0)
                {
                    staggerCoroutine = StartCoroutine(Stagger(10));

                    StartCoroutine(Kill());

                }
                else
                {
                    staggerCoroutine = StartCoroutine(Stagger(staggerDuration));
                }
            }
        }
        
    }

    //changes the enemy's state to idle, and resets it to its former state after a while
    public IEnumerator Stagger(float duration)
    {
        if (enemyType == EnemyTypes.Guard)
        {
            prevState = stateList[1];
        }
        if (enemyType == EnemyTypes.Boss)
        {
            prevState = stateList[5];
            if (isDoneWithSpecial == false)
            {
                isDoneWithSpecial = true; //AI: Boss ability is now on cooldown
            }
        }
        else
        {
            prevState = currentState;
        }

        currentState = stateList[4];

        yield return new WaitForSeconds(duration);

        currentState = prevState;
        postFrenzyDashExhaustion = false;
    }

    //kills the enemy if hp is reduced to 0
    public IEnumerator Kill()
    {
        if (enemyType == EnemyTypes.Boss)
        {
            AkSoundEngine.PostEvent("Play_Death_Boss", gameObject);

        }
        else if (enemyType == EnemyTypes.BigGuy || enemyType == EnemyTypes.Guard)
        {
            AkSoundEngine.PostEvent("Play_Death_Big", gameObject);

        }
        else
        {
            AkSoundEngine.PostEvent("Play_Death_Small", gameObject);
        }


        while (!eventManager.checkEnemyDeath)
        {
            yield return null;
        }
        dead = true;
        eventManager.CheckEnemyCount();
        Instantiate(deathParticles, transform.position, deathParticles.transform.rotation, null);
        gameObject.SetActive(false);
		kills();
        yield return null;
    }

    //Rotates a shield around the enemy - Not used
    /*
    public IEnumerator RotateShield()
    {
        rotatingShield.SetActive(true);
        var prevPosition = rotatingShield.transform.position;
        var prevRotation = rotatingShield.transform.rotation;

        currentBossAttackCooldown = rotatingShieldCooldownReduction;

        float currRotatingShieldDuration = 0;
        AkSoundEngine.PostEvent("Play_Shield_Activate_Boss", gameObject);
        while (currRotatingShieldDuration < rotatingShieldDuration)
        {
            rotatingShield.transform.RotateAround(transform.position, Vector3.up, shieldRotateSpeed * Time.deltaTime);

            currRotatingShieldDuration += Time.deltaTime;
            yield return null;
        }

        rotatingShield.transform.position = prevPosition;
        rotatingShield.transform.rotation = prevRotation;
        rotatingShield.SetActive(false);
        currentBossAttackCooldown = bossSpecialAbilityCooldown;
        isDoneWithSpecial = true; //AI: Boss ability is now on cooldown
    }
    */

    public IEnumerator Dash(float dashCooldown, float dashSpeed, float dashDuration, float windupRotating, float windupNotRotating, bool bossEnemy = false, bool fDash = false)
    {
        immuneToStagger = true; //Boss specific attribute
        currentDashCooldown = 1000; //Sat arbitrarily high

        fieldOfView.viewRadius = 1000; //Sat arbitrarily high to avoid the enemy to lose sight of the player
        fieldOfView.obstacleMask = LayerMask.GetMask("Nothing"); //enables the enemy to see through walls

        oldPos = transform.position; //saves position 
        manualPosition = true; //forces enemy to stay at oldPos;
        dashDirection.SetActive(true);
        if (enemyType == EnemyTypes.Boss && firstDash)
        {
            firstDash = false;
            yield return new WaitForSeconds(windupRotating * 2);
        }
        else
            yield return new WaitForSeconds(windupRotating);

        navMeshAgent.updateRotation = false; //forces the enemy to stop rotating
        targetOldPos = new Vector3(chaseTarget.position.x, 0, chaseTarget.position.z);
        enemyAnimator.SetTrigger("Attack");
        if (bossEnemy)
        {
            AkSoundEngine.PostEvent("Play_Dash_Windup_Boss", gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent("Play_Guard_Attack_Windup", gameObject);
        }
        dashZonefill.fillAmount = 0;
        StartCoroutine(fillTelegraph(dashZonefill, dashDuration));

        yield return new WaitForSeconds(windupNotRotating);

        shield.SetActive(true);
        dashDirection.SetActive(false);
        manualPosition = false; //free up the enemy's movement
        navMeshAgent.updateRotation = true; //free up enemy's rotation

        dashdestination = transform.position + (targetOldPos - transform.position) * 10;
        dashing = true; //sets the enemy's navmesh target to be at the players old position;
        navMeshAgent.speed = dashSpeed;
        manualRotation = true; //forces enemy to look in the direction its moving
        if (bossEnemy)
        {
            AkSoundEngine.PostEvent("Play_Dash_Attack_Boss", gameObject);
        }
        else
        {
            AkSoundEngine.PostEvent("Play_Guard_Attack_Charge", gameObject);
        }
        yield return new WaitForSeconds(dashDuration);

        dashing = false; //allow the enemy's navmesh target to be assigned normally again
        manualRotation = false; //allows the enemy to rotate freely

        //resets the enemy's view radius and makes it so it can't look through walls
        fieldOfView.viewRadius = 20;
        fieldOfView.obstacleMask = LayerMask.GetMask("Obstacles");

        currentDashCooldown = dashCooldown; //start cooldown on dash - not used for boss
        haveDashed = true; //AI: Dash is now on cooldown

        if (bossEnemy)
            navMeshAgent.speed = bossMovespeed; //resets enemy move speed
        else
            navMeshAgent.speed = guardianMoveSpeed; //resets enemy move speed

        if (bossEnemy == true && fDash == true && currFrenzyAmount == frenzyAmount - 1)
        {
            staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
            postFrenzyDashExhaustion = true;
            shield.SetActive(false);
        }
        else if (bossEnemy == true && fDash == false)
        {
            currentBossAttackCooldown = bossSpecialAbilityCooldown; //starts cooldown on special ability - used for boss
            isDoneWithSpecial = true; //AI: Boss ability is now on cooldown
            shield.SetActive(false);
        }
        else if (bossEnemy == false)
        {
            staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
        }

        immuneToStagger = false; //Boss specific attribute
    }

    //Pre-emtively stops the enemy, if he dashes into terrain.
    private void OnTriggerEnter(Collider other)
    {
        if (dashing && other.gameObject.CompareTag("Terrain") && enemyType == EnemyTypes.Guard)
        {
            StopCoroutine(dashCoroutine);
            staggerCoroutine = StartCoroutine(Stagger(exhaustDuration));
        }
    }

    //Used to give ranged enemies an audiotory feedforward.
    IEnumerator ShooterWindupSound(string type)
    {
        yield return new WaitForSeconds(bulletLifeTime - 1f);
        AkSoundEngine.PostEvent("Play_" + type + "_Attack_Windup", gameObject);
    }

    //Used by all ranged enemies to calculate where to shoot, to hit the player.
    private Vector3 FindInterceptVector(Vector3 shotOrigin, float shotSpeed, Vector3 targetOrigin, Vector3 targetVel)
    {
        Vector3 dirToTarget = Vector3.Normalize(targetOrigin - shotOrigin);

        // Decompose the target's velocity into the part parallel to the
        // direction to the cannon and the part tangential to it.
        // The part towards the cannon is found by projecting the target's
        // velocity on dirToTarget using a dot product.
        Vector3 targetVelOrth = Vector3.Dot(targetVel, dirToTarget) * dirToTarget;

        // The tangential part is then found by subtracting the
        // result from the target velocity.
        Vector3 targetVelTang = targetVel - targetVelOrth;

        
        Vector3 shotVelTang = targetVelTang;

        float shotVelSpeed = shotVelTang.magnitude;
        if (shotVelSpeed > shotSpeed)
        {
            // Shot is too slow to intercept target, it will never catch up.
            // Do our best by aiming in the direction of the targets velocity.
            return targetVel.normalized * shotSpeed;
        }
        else
        {
            // We know the shot speed, and the tangential velocity.
            // Using pythagoras we can find the orthogonal velocity.
            float shotSpeedOrth =
            Mathf.Sqrt(shotSpeed * shotSpeed - shotVelSpeed * shotVelSpeed);
            Vector3 shotVelOrth = dirToTarget * shotSpeedOrth;

            // Finally, add the tangential and orthogonal velocities.
            return shotVelOrth + shotVelTang;
        }
    }
    
    //Used to fire mortars at the player.
    public IEnumerator FallingMortar(int mortarIndex, Vector3 startPos, Vector3 endPos, float travelTime, float time) //Fires the mortar towards the ground. Makes the dropzone a hazard, when within proximity.
    {
        //if (bossMortarFireSound) //Play a different sound, based on which type of enemy is using the fallingMortar function
        //{
        //    AkSoundEngine.PostEvent("Play_Mass_Lob_Boss", mortarList[mortarIndex].gameObject);
        //}
        //else
        //{
        //    AkSoundEngine.PostEvent("Play_Nova_Attack_Lob", mortarList[mortarIndex].gameObject);

        //}        
        StartCoroutine(Lopping(mortarList[mortarIndex], startPos, endPos, 4, time, travelTime));
        
        float cTime = (Time.time - time);

        //Fill telegraphing
        while (cTime < travelTime)
        {
            cTime = (Time.time - time);
            float size = cTime / travelTime;
            mortarRendererList[mortarIndex].localScale = new Vector3(size + 0.1f, size + 0.1f, size + 0.1f);
            
            yield return null;
        }

        mortarList[mortarIndex].transform.parent = mortarMagazine.transform;
        mortarList[mortarIndex].transform.localPosition = new Vector3(0, 0, 0);
        mortarList[mortarIndex].SetActive(false);

        //if (bossMortarFireSound) //Play a different sound, based on which type of enemy is using the fallingMortar function
        //{
        //    AkSoundEngine.PostEvent("Play_Mass_Impact_Boss", mortarList[mortarIndex].gameObject);
        //    bossMortarFireSound = false;
        //}
        //else
        //{
        //    AkSoundEngine.PostEvent("Play_Nova_Attack_Impact", mortarList[mortarIndex].gameObject);

        //}

        mortarDropZoneList[mortarIndex].tag = ("Hazard");

        yield return new WaitForSeconds(0.05f); //Small delay before changing the dropzone back to untagged, 
                                                //to give the player script a chance to register the onTriggerStay
        mortarDropZoneList[mortarIndex].tag = ("Untagged");
        mortarDropZoneList[mortarIndex].transform.parent = mortarDropZoneMagazine.transform;
        mortarDropZoneList[mortarIndex].transform.localPosition = new Vector3(0, 0, 0);
        mortarDropZoneList[mortarIndex].SetActive(false);
    }
    
    private IEnumerator Lopping(GameObject mortar, Vector3 startPos, Vector3 endPos, float height, float time, float travelTime)
    {
        var modifier = 1 / travelTime;
        float cTime = (Time.time - time) * modifier;

        while (cTime < travelTime)
        {
            cTime = (Time.time - time) * modifier;

            // calculate straight-line lerp position:
            Vector3 currentPos = Vector3.Lerp(startPos, endPos, cTime);

            // add a value to Y, using Sine to give a curved trajectory in the Y direction
            currentPos.y += height * Mathf.Sin(Mathf.Clamp01(cTime) * Mathf.PI);

            // finally assign the computed position to our gameObject:
            mortar.transform.position = currentPos;

            yield return null;
        }
    }

    #endregion

    //Debug stuff to visually show state
    private void OnDrawGizmos()
    {
        if (currentState != null && eyes != null)
        {
            Gizmos.color = currentState.sceneGizmoColor;
            Gizmos.DrawSphere(eyes.position, 1);
        }
    }

    //Checks if the state it should transition to, isn't the 'remain in state' state.
    public void TransitionToState(State nextState)
    {

        if (nextState != remainState)
        {
            currentState = nextState;
        }
    }
    
    //Spawns the enemies and set their model and stats correctly
    private void OnEnable() 
    {
        if (enemyType == EnemyTypes.BigGuy || enemyType == EnemyTypes.Guard)
        {
            enemyAnimator = bigGuyModel.GetComponent<Animator>();
            enemyModel.SetActive(false);
            bossModel.SetActive(false);
        }
        else if (enemyType == EnemyTypes.Boss)
        {
            enemyAnimator = bossModel.GetComponent<Animator>();
            enemyModel.SetActive(false);
            bigGuyModel.SetActive(false);
        }
        else
        {
           // enemyModel.SetActive(true);
            enemyAnimator = enemyModel.GetComponent<Animator>();
            bigGuyModel.SetActive(false);
            bossModel.SetActive(false);
        }

        AssignEnemyType();
        StartCoroutine(SpawnAnimationHack());
        StartCoroutine(Stagger(3));
    }

    public IEnumerator fillTelegraph(Image fillTelegraphImage, float attackChargeTime)
    {
        float totalTime = 0;
        while (totalTime <= attackChargeTime)
        {
            fillTelegraphImage.fillAmount = totalTime / attackChargeTime;
            totalTime += Time.deltaTime;
            yield return null;
        }

    }

    /// Move the body up from below the surface
    private IEnumerator SpawnAnimationHack()
    {
        // This should sorta mach up with the animation time.
        const float riseTime = 3;

        Bounds bounds = GetComponent<Collider>().bounds;
        float height = bounds.max.y - bounds.min.y;

        float riseSpeed = height / riseTime;
        float heightOffset = -height;

        Vector3 endPos = transform.localPosition;
        
        Vector3 startPos = endPos + new Vector3(0, heightOffset, 0);
        Vector3 currPos = startPos;

        float startTime = Time.time;
        float endTime = startTime + riseTime;

        while (Time.time < endTime)
        {
            //currPos.y += Time.deltaTime * riseSpeed;
            currPos = Vector3.Lerp(startPos, endPos, (Time.time - startTime) / riseTime);
            transform.localPosition = currPos;
            yield return null;
        }
        transform.localPosition = endPos;
        if (enemyType == EnemyTypes.Boss)
        {
            AkSoundEngine.SetState("Boss_Phases", "Intro");
            AkSoundEngine.SetState("Music_State", "Boss");
        }
    }
}