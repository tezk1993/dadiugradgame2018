﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Idle")]
public class IdleAction : Action {

    public override void Act(StateController controller,Animator enemyAnimator)
    {
        Idle(controller, enemyAnimator);
    }

    private void Idle(StateController controller, Animator enemyAnimator)
    {
        controller.navMeshAgent.isStopped = true;
        enemyAnimator.SetBool("Running", false);
    }
}
