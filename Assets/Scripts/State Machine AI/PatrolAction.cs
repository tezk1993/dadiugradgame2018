﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Patrol")]
public class PatrolAction : Action
{
    public override void Act(StateController controller, Animator enemyAnimator)
    {
        Patrol(controller, enemyAnimator);
    }

    private void Patrol(StateController controller, Animator enemyAnimator)
    {
        if (controller.enemyType == StateController.EnemyTypes.Lost)
        {
            controller.navMeshAgent.speed = controller.lostWalkSpeed;
        }

        controller.navMeshAgent.destination = controller.wayPointList[controller.nextWayPoint];
        controller.navMeshAgent.isStopped = false;
        
        if (controller.navMeshAgent.remainingDistance <= controller.stoppingDistance && !controller.navMeshAgent.pathPending)
        {
            controller.nextWayPoint = (controller.nextWayPoint + 1) % controller.wayPointList.Count;
        }
    }
}