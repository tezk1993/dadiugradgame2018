﻿/**
 * PlayerScriptDragJoystick.cs
 * Created by: Artur Barnabas
 * Created on: 08/11/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


[RequireComponent(typeof(TrailRenderer))]
public class PlayerScriptDragJoystick : MonoBehaviour
{
    public delegate void EnemyHit();
    public static event EnemyHit hit;

    public delegate void LoseCombo();
    public static event LoseCombo loseCombo;
    [Header("Testing")]
    public int screenWidth;
    public int screenHeigth;

    [Header("REMEMBER!!!!")]
    [Header("DebugUI prefab + HealthUI prefab in the scene!")]
    [Header("")]
    [Header("")]
    [Header("Movement Stuff")]
    public float startSpeed = 5;
    public float dashSpeed = 30;
    public float dashTime = 0.1f;
    public float curDashTime = 0;
    public float durDash = 0.2f;
    public ParticleSystem dashEffect;

    public float upwardAnddownwardModifier;

    private bool leftFirstTouch;
    private bool rightTouched;
    private bool leftTouched;
    private Vector3 dashVelocity;

    private Transform circle;
    private Transform outerCircle;
    private Transform circle2;
    private Transform outerCircle2;
    public Transform dirIndicator;
    public Transform chargeIndicator;

    
    public float speed;
    //private bool isDashing;

    [Header("Joystick Stuff")]
    [Tooltip("A number between 0 and 1 specifying the size of area in the middle of the joystick where the input is not registered.")]
    public float joystickDeadzone = 0.2f;

    [Tooltip("The time the user has to let go of the screen in order for it to be counted as a tap")]
    public float tapTime = 0.2f;

    [Tooltip("The maximum distance of the center of the joystick from the finger position. As the finger tries to move further, they joystick will follow.")]
    public float fingerMaxDistance = 30;

    public float joystickScreenBorder = 20;

    private bool startTouch;
    private Vector2 pointA;
    private Vector2 pointB;
    private Vector2 pointADash;
    private Vector2 pointBDash;


    [Header("Dash Stuff")]
    [Tooltip("How far should the character dash")]
    public float dashRange = 40;
    [Tooltip("How fast should the dash-speed decrease")]
    public float speedDecrease = 10f;
    public float dashStartTime;
    public bool dashing;
    [Tooltip("How long should the dash-cooldown be")]
    public float cooldownTime = 1;
    private Text cooldownText;
    [Tooltip("How long should the character be dashing")]
    public float maxDashTime = 1.0f;
    public float dashWindDownTime = 0.2f;
    public float dashWindDownMinSpeed;
    private float dashWindDownTimer = 0;

    [Tooltip("The time inbetween each charge upgrade")]
    public float chargeTime = 0.5f;
    private float timeleft;
    private float dashChargeTime;
    private bool chargingDash;
    private bool rightHolding;
    public int chargeMultiplier = 1;

    public int maxDashCharges = 2;
    private int dashCharges;

    private bool hasDied;

   
    private float currentDashTime;
    private float cooldown;
    private bool readyToDash;
    private bool hasStoppedMoving;
    private Vector3 direction;
    //private Vector3 dashSpeed;

    [Header("Health Stuff")]
    [Tooltip("container of health icons")] private GameObject healthContainer;
    public GameObject[] healthArray;
    public int healthInt = 3;
    

    [Header("Screenshake Stuff")]
    public float screenshakeMult = 1.5f;
    public float screenshakeDuration = 0.35f;

    [Header("Time Slow Stuff")]
    public AnimationCurve timeSlowCurve;
    public float numberOfTimeScaleUpdates;
    private float timeScaleUpdateCounter;
    public float timeSlowCurveDuration;

    public float invis = 0.5f;

    [Header("Trail Renderer Stuff")]
    [Tooltip("This is the material for the trail, defining its color")]
    public Material trailMaterial;
    [Tooltip("The fade time of the trail")]
    public float trailTime = .5f;
    [Tooltip("The width of the trails head")]
    public float trailStartWidth = 1;
    [Tooltip("The width of the trails tail")]
    public float trailEndWidth = 0.5f;
    TrailRenderer trail;

    

    float inputAngleOffset;
    
    Animator animator;

    // Use this for initialization
    void Start () {
        screenHeigth = Screen.height;
        screenWidth = Screen.width;

        circle = GameObject.FindGameObjectWithTag("DebugUI").transform.Find("Inner");
        outerCircle = GameObject.FindGameObjectWithTag("DebugUI").transform.Find("Outer");
        //circle2 = GameObject.FindGameObjectWithTag("DebugUI").transform.Find("Inner2");
        //outerCircle2 = GameObject.FindGameObjectWithTag("DebugUI").transform.Find("Outer2");
        cooldownText = GameObject.FindGameObjectWithTag("DebugUI").transform.Find("Cooldown text").GetComponent<Text>();
        healthContainer = GameObject.FindGameObjectWithTag("HealthUI").transform.Find("HealthContainerForRun").gameObject;


        dashCharges = maxDashCharges;
        speed = startSpeed;

        direction = Vector3.zero;
        currentDashTime = maxDashTime;
        cooldown = cooldownTime;
        trail = GetComponent<TrailRenderer>();
        trail.material = trailMaterial;
        trail.startWidth = trailStartWidth;
        trail.endWidth = trailEndWidth;
        trail.time = trailTime;
        trail.enabled = false;
        //Debug.Log(healthContainer.transform.childCount);

        healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }

        var mainCam = Camera.main;
        inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, mainCam.transform.forward, Vector3.up);
        
        animator = GetComponent<Animator>();

        //StartCoroutine(DashWindDown());
    }

    private void Update()
    {
        invis -= Time.deltaTime;
        if(timeleft > 0 && rightHolding)
        {
            timeleft -= Time.deltaTime;
        }
        if(timeleft <= 0 && rightHolding && !chargingDash && 0 < dashCharges)
        {
            chargingDash = true;
            chargeIndicator.gameObject.SetActive(true);
            dashChargeTime = chargeTime;
            StartCoroutine(IndicatorDash());
        }
        if(dashChargeTime > 0 && chargingDash && 0 < dashCharges)
        {
            dashChargeTime -= Time.deltaTime;
        }
        if(dashChargeTime <= 0 && chargingDash && chargeMultiplier < dashCharges)
        {
            chargeMultiplier++;
            StartCoroutine(IndicatorDash());
        }
        

        Movement();
        //dashSpeed = Vector3.Lerp(dashSpeed, new Vector3(0, direction.y, 0), Time.deltaTime * speedDecrease);
        //transform.position += dashSpeed * Time.deltaTime;
        //transform.Translate(dashSpeed);

        if (dashCharges < maxDashCharges)
        {
            cooldown -= Time.deltaTime;

            if (cooldown <= 0)
            {
                dashCharges++;
                cooldown = cooldownTime;
                AkSoundEngine.PostEvent("Play_Dash_Recharge", gameObject);
            }
        }
        
        cooldownText.text = "Dash Charge: " + dashCharges.ToString() + "\n" + "Dash CD: " + cooldown.ToString("F2");



    }

    private void FixedUpdate()
    {
        bool isRunning = false;
        if (!hasDied)
        {
            if (startTouch)
            {
                Vector2 offset = pointB - pointA;
                if (offset.magnitude > fingerMaxDistance)
                {
                    offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
                    pointA = pointB - offset;
                    AdjustJoystickToBorder();        
                }

                Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
                circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);

                if (direction.magnitude > joystickDeadzone && !dashing && leftTouched)
                {
                    //isRunning = true;
                    var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
                    angle += inputAngleOffset;
                    angle *= Mathf.Deg2Rad;
                    direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));


                    //Convert to vector 3 rotate 45 degrees to match camera rotation

                    if (direction.x > 0.7f && direction.x < 1f  && !rightTouched || direction.x < -0.7f && direction.x > -1f && !rightTouched)
                    {
                        var velocity = new Vector3(direction.x, 0, direction.y);
                        dashVelocity = velocity;
                        velocity = new Vector3(direction.x, 0, direction.y) * (speed + upwardAnddownwardModifier);
                        if (!chargingDash)
                        {
                            isRunning = true;
                            transform.Translate(velocity * Time.deltaTime, Space.World);
                        }
                        transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                        dirIndicator.transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                    }
                    else if(!rightTouched)
                    {
                        var velocity = new Vector3(direction.x, 0, direction.y);
                        dashVelocity = velocity;
                        velocity = new Vector3(direction.x, 0, direction.y) * speed;
                        if (!chargingDash)
                        {
                            isRunning = true;
                            transform.Translate(velocity * Time.deltaTime, Space.World);
                        }
                        
                        transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                        dirIndicator.transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                    }
                    if (rightTouched)
                    {
                        var velocity = new Vector3(direction.x, 0, direction.y);
                        dashVelocity = velocity;
                    }
                }
            }
        }
        animator.SetBool("Running", isRunning);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            TakeDamage();
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {
            Debug.Log(collision.contacts[0].normal.z);
            Debug.Log("bumped into shield");

            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject);
                    Debug.Log("dashed into shield");
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (hit != null)
                {
                    hit();
                }
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject);
                Debug.Log("Murder: " + collision.gameObject.name);
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                //SlowTime(timeSlowDegree, timeSlowDuration);
                dashCharges++;
            }
            else
            {
                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structure_Impact", gameObject);
                StopDash();
            }

                
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                StopDash();
            }
               
        }
        else if (collision.gameObject.CompareTag("Shield"))
        {
            Debug.Log(collision.contacts[0].normal.z);
            Debug.Log("bumped into shield");

            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject); // test to see if the sound would keep playing
                    Debug.Log("dashed into shield");
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (hit != null)
                {
                    hit();
                }
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject); // test to see if the sound would keep playing
                Debug.Log("Murder: " + collision.gameObject.name);
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                //SlowTime(timeSlowDegree, timeSlowDuration);
                dashCharges++;
            }
            else
            {
                TakeDamage();
            }
        }
    }

    public void Dashed()
    {
        dashEffect.Play();
        dashCharges--;
        speed = dashSpeed;
        dashing = true;
        trail.enabled = true;
        Invoke("StopDash", dashTime);
        //Invoke("StartCoroutine(DashWindDown())", dashTime);
        //StartCoroutine(DashWindDown());
        animator.SetTrigger("Attack");
        AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);
    }
    

    public void TakeDamage()
    {
        if (invis <= 0.0f)
        {
            if (healthInt > 0)
            {
                if (loseCombo != null)
                {
                    loseCombo();
                }
                healthArray[healthInt - 1].SetActive(false);
                healthInt--;
                AkSoundEngine.PostEvent("Play_Hit_Hero", gameObject);

            }

            if (healthInt <= 0)
            {
                Debug.Log("YOU DIED");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                hasDied = true;
                startTouch = false;
                AkSoundEngine.PostEvent("Play_Death_Hero", gameObject);
                
            }
            invis = 0.5f;
        }  
    }

    public IEnumerator SlowTimeNew()
    {
        while (timeScaleUpdateCounter < numberOfTimeScaleUpdates)
        {
            Time.timeScale = timeSlowCurve.Evaluate(timeScaleUpdateCounter / numberOfTimeScaleUpdates);
            timeScaleUpdateCounter++;
            //Debug.Log(Time.timeScale);
            yield return new WaitForSecondsRealtime(timeSlowCurveDuration / numberOfTimeScaleUpdates);
        }
        Time.timeScale = 1;
    }

    public IEnumerator SlowTime(float timeSlowDegree, float timeSlowDuration)
    {
        Time.timeScale = timeSlowDegree;
        yield return new WaitForSecondsRealtime(timeSlowDuration);
        Time.timeScale = 1;
    }

    void Movement()
    {
        if (Input.touchCount == 0)
        {
            hasDied = false;  
        }
        if (!hasDied)
        {
            if (Input.touchCount > 0)
            {
                const float joystickMaxVerticalPositionRelative = 0.7f;
                float joysticMaxVertPos = screenHeigth * joystickMaxVerticalPositionRelative;
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    if (Input.GetTouch(0).position.y < joysticMaxVertPos)
                    {
                        if (Input.GetTouch(0).position.x < screenWidth / 2)
                        {
                            leftTouched = true;

                            pointA = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);

                            circle.transform.position = pointA;
                            outerCircle.transform.position = pointA;

                            AdjustJoystickToBorder();

                            circle.gameObject.SetActive(true);
                            outerCircle.gameObject.SetActive(true);
                            dirIndicator.gameObject.SetActive(true);
                        }

                        if (Input.GetTouch(0).position.x > screenWidth / 2) // if the first touch is on the right side of the screen
                        {
                            rightHolding = true;
                            rightTouched = true;
                            timeleft = tapTime;

                            //dirIndicator.gameObject.SetActive(true);
                        }
                    }

                }
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    startTouch = true;
                    if (Input.GetTouch(0).position.y < joysticMaxVertPos && leftTouched)
                    {
                        pointB = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
                    }

                }
                else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
                {
                    startTouch = true;
                    if (Input.GetTouch(0).position.y < joysticMaxVertPos && leftTouched)
                    {
                        pointB = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
                    }

                }
                else
                {
                    //startTouch = false;
                    StartCoroutine(StopStartTouch(startTouch));
                }
                if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    startTouch = false;
                    if (leftTouched)
                    {
                        leftTouched = false;
                        circle.gameObject.SetActive(false);
                        outerCircle.gameObject.SetActive(false);
                    }
                    else
                    {
                        rightHolding = false;
                        rightTouched = false;
                        if(dashCharges > 1 && chargeMultiplier > 1)
                        {
                            dashCharges--;
                            dashCharges--;
                            StartCoroutine(Dash());
                            chargeIndicator.position = transform.position;
                            chargeIndicator.gameObject.SetActive(false);
                        }
                        else if (dashCharges > 0)
                        {
                            dashCharges--;
                            StartCoroutine(Dash());
                            chargeIndicator.position = transform.position;
                            chargeIndicator.gameObject.SetActive(false);
                        }
                        else
                        {
                            chargingDash = false;
                            AkSoundEngine.PostEvent("Play_Dash_Denied", gameObject);
                        }
                        dirIndicator.gameObject.SetActive(false);
                    }
                }

                if (Input.touchCount > 1)
                {
                    if (Input.GetTouch(1).phase == TouchPhase.Began)
                    {
                        if (Input.GetTouch(1).position.y < joysticMaxVertPos) // change this value to something like screenheight / somenumber
                        {
                            if (Input.GetTouch(1).position.x < screenWidth / 2 && rightTouched)
                            {
                                //leftFirstTouch = true;

                                pointA = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);

                                circle.transform.position = pointA;
                                outerCircle.transform.position = pointA;

                                AdjustJoystickToBorder();

                                circle.gameObject.SetActive(true);
                                outerCircle.gameObject.SetActive(true);
                                dirIndicator.gameObject.SetActive(true);
                            }

                            if (Input.GetTouch(1).position.x > screenWidth / 2 && leftTouched) // if the second touch is on the right side of the screen
                            {
                                rightHolding = true;
                                timeleft = tapTime;

                                //dirIndicator.gameObject.SetActive(true);
                            }
                        }
                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Moved)
                    {

                        if (Input.GetTouch(1).position.y < joysticMaxVertPos && rightTouched)
                        {
                            pointB = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                        if (Input.GetTouch(1).position.y < joysticMaxVertPos && leftTouched) // if the second touch is on the right side of the screen
                        {

                        }
                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Stationary)
                    {

                        if (Input.GetTouch(1).position.y < joysticMaxVertPos && rightTouched)
                        {
                            pointB = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                        if (Input.GetTouch(1).position.y < joysticMaxVertPos && leftTouched) // if the second touch is on the right side of the screen
                        {

                        }
                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Ended)
                    {
                        if (rightTouched)
                        {
                            leftTouched = false;
                            circle.gameObject.SetActive(false);
                            outerCircle.gameObject.SetActive(false);
                        }
                        else
                        {
                            rightHolding = false;
                            rightTouched = false;
                            if (dashCharges > 1 && chargeMultiplier > 1)
                            {
                                dashCharges--;
                                dashCharges--;
                                StartCoroutine(Dash());
                                chargeIndicator.position = transform.position;
                                chargeIndicator.gameObject.SetActive(false);
                            }
                            else if (dashCharges > 0)
                            {
                                dashCharges--;
                                StartCoroutine(Dash());
                                chargeIndicator.position = transform.position;
                                chargeIndicator.gameObject.SetActive(false);
                            }
                            else
                            {
                                chargingDash = false;
                                AkSoundEngine.PostEvent("Play_Dash_Denied", gameObject);
                            }
                            //circle2.gameObject.SetActive(false);
                            //outerCircle2.gameObject.SetActive(false);
                            dirIndicator.gameObject.SetActive(false);
                        }

                    }
                }
            }
        }
        
    }

    void AdjustJoystickToBorder()
    {
        pointA.x = Mathf.Max(pointA.x, joystickScreenBorder);
        pointA.x = Mathf.Min(pointA.x, screenWidth - joystickScreenBorder);
        pointA.y = Mathf.Max(pointA.y, joystickScreenBorder);
        pointA.y = Mathf.Min(pointA.y, screenHeigth - joystickScreenBorder);

        outerCircle.transform.position = pointA;
    }

    IEnumerator StopStartTouch(bool stop)
    {
        yield return new WaitForSeconds(0.1f);
        stop = false;
    }

    IEnumerator Dash()
    {
        chargingDash = false;
        
        dashEffect.Play();
        curDashTime = 0;
        speed = dashSpeed;
        dashing = true;
        trail.enabled = true;
        animator.SetTrigger("Attack");
        AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);
        while (curDashTime < durDash * chargeMultiplier)
        {
            curDashTime += Time.deltaTime;
            transform.Translate(dashVelocity * Time.deltaTime * speed, Space.World);
            transform.rotation = Quaternion.LookRotation(dashVelocity, Vector3.up);
            yield return null;
        }
        chargeMultiplier = 1;
        StopDash();

    }
    IEnumerator IndicatorDash()
    {
        float curIndiDashTime = 0;
        speed = dashSpeed;
        while (curIndiDashTime < durDash)
        {
            curIndiDashTime += Time.deltaTime;
            chargeIndicator.transform.Translate(dashVelocity * Time.deltaTime * speed, Space.World);
            yield return null;
        }


    }

    public IEnumerator DashWindDown()
    {
        //yield return new WaitForSeconds(dashTime);
        while (dashWindDownTimer < dashWindDownTime)
        {
            //speed = dashSpeed -dashWindDownMinSpeed- dashWindDownTimer / dashWindDownTime * dashSpeed + dashWindDownMinSpeed;
            speed = Mathf.Lerp(dashWindDownMinSpeed, startSpeed, dashWindDownTimer/dashWindDownTime);
            yield return null;
            dashWindDownTimer += Time.deltaTime;
            //Debug.Log("is slow now");
        }
        speed = startSpeed;
        
    }

    void StopDash()
    {
        
        dashWindDownTimer = 0;
        speed = dashWindDownMinSpeed;
        dashing = false;
        trail.enabled = false;
        StartCoroutine(DashWindDown());
    }
}
