﻿/**
 * CameraMovement.cs
 * Created by: Artur Barnabas
 * Created on: 19/11/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// 
/// This script will automatically place the camera such that it is centered on the target
/// 
[RequireComponent(typeof(Camera))]
public class CameraMovement : MonoBehaviour
{
    
    #region Types

    private enum CameraState { Following, AnimatingIn, AnimatingOut }

    private class CameraWaypoint
    {
        public Quaternion rotation;
        public Vector3 position;
        public float fov;
    }
    #endregion

    #region Variables

    public Transform followTarget;

	[Tooltip("Speed of the camera movement.")]
	public float speed = 3;

	
    [Tooltip("Higher values will result in more on-screen space in front of the player")]
    public float leadFactor = 2;

    public float maxLeadDistance = 1f;

    float animateInTime;
    float animateOutTime;

    Camera thisCam;
    CameraWaypoint followState;

    // value between 0 and 1 where 0 means the begining of the waypoints,
    // 1 means reached end of waypoints
    float animateRatio;
    CameraState cameraState;

    CameraWaypoint[] waypoints;

	Vector3 offset;
    Quaternion originalRot;

	Vector3 lastPlayerPos;

    #endregion

    #region Unity Methods

    public void Awake()
    {
        followTarget = GameObject.FindGameObjectWithTag("Player").transform;
        thisCam = GetComponent<Camera>();
        cameraState = CameraState.Following;
        followState = new CameraWaypoint{
            position = transform.position,
            rotation = transform.rotation,
            fov = thisCam.fieldOfView
        };
    }

    public void Start()
    {
        Bounds bounds = GetHierarchyBounds(followTarget.gameObject);
        float height = bounds.max.y - bounds.min.y;

        offset = 
            Vector3.up * height/2 +
            -transform.forward * Vector3.Distance(transform.position, followTarget.position);

		lastPlayerPos = followTarget.position;
    }

    public void Update()
    {
        if (Time.deltaTime > 0)
        {
            followState.position = GetPositionOnFollow();
        }

        switch (cameraState)
        {
            case CameraState.Following:
                transform.position = followState.position;
                break;
            
            case CameraState.AnimatingIn:
            {
                Debug.Log("Camera stuff");
                float zoomSpeed = 1 / animateInTime;
                animateRatio += Time.deltaTime * zoomSpeed;
                animateRatio = Mathf.Min(1, animateRatio);
                SetCameraFromAnimationRatio();
                break;
            }
            
            case CameraState.AnimatingOut:
            {
                float zoomSpeed = 1 / animateOutTime;
                animateRatio -= Time.deltaTime * zoomSpeed;
                if (animateRatio <= 0)
                {
                    animateRatio = 0;
                    cameraState = CameraState.Following;
                }
                SetCameraFromAnimationRatio();
                break;
            }
        }
    }
    #endregion

    #region Methods

    Vector3 GetPositionOnFollow()
    {
        var newPlayerPos = followTarget.position;
        var playerVelocity = newPlayerPos - lastPlayerPos;
        var playerSpeed = 0f;
        if (Time.deltaTime > 0)
        {
            playerSpeed = playerVelocity.magnitude / Time.deltaTime;
        }
        lastPlayerPos = newPlayerPos;

        var distance = Mathf.Min(maxLeadDistance, playerSpeed * leadFactor);
        
        var goal = followTarget.position + offset + playerVelocity.normalized * distance;
        return Vector3.Lerp(followState.position, goal, Time.deltaTime * speed);
    }

    [ContextMenu("ZoomOnMural")]
    public void ZoomOnMural()
    {
        animateInTime = 2;
        cameraState = CameraState.AnimatingIn;
        UpdateWaypoints();
    }

    [ContextMenu("ZoomBackFromMural")]
    public void ZoomBackFromMural()
    {
        animateOutTime = 1.5f;
        cameraState = CameraState.AnimatingOut;
        UpdateWaypoints();
    }

    public void AnimateFromCurrent(GameObject waypointsContainer, float animateTime)
    {
        animateInTime = Mathf.Max(float.Epsilon, animateTime);
        animateRatio = 0;
        cameraState = CameraState.AnimatingIn;
        UpdateWaypointsFromCurrent(waypointsContainer);
    }

    public void AnimateToFollowState(float animateTime)
    {
        animateOutTime = animateTime;
        animateRatio = 1;
        cameraState = CameraState.AnimatingOut;
        CameraWaypoint currState = new CameraWaypoint {
            position = transform.position,
            rotation = transform.rotation,
            fov = thisCam.fieldOfView
        };
        waypoints = new CameraWaypoint[] {
            followState, followState, currState
        };
    }

    void SetCameraFromAnimationRatio()
    {
        float t = Mathf.SmoothStep(0, 1, animateRatio);

        Quaternion targetRotation = Quaternion.Lerp(
            waypoints[1].rotation, waypoints[2].rotation, t
        );
        Vector3 targetPosition = Vector3.Lerp(
            waypoints[1].position, waypoints[2].position, t
        );

        float targetFov = Mathf.Lerp(
            waypoints[1].fov, waypoints[2].fov, t
        );

        transform.rotation = Quaternion.Lerp(waypoints[0].rotation, targetRotation, t);
        transform.position = Vector3.Lerp(waypoints[0].position, targetPosition, t);
        thisCam.fieldOfView = Mathf.Lerp(waypoints[0].fov, targetFov, t); 
    }

    void UpdateWaypointsFromCurrent(GameObject parent)
    {
        var sceneWaypoints = GetWaypoints(parent);
        CameraWaypoint currState = new CameraWaypoint {
            position = transform.position,
            rotation = transform.rotation,
            fov = thisCam.fieldOfView
        };
        waypoints = new CameraWaypoint[] {
            currState, sceneWaypoints[0], sceneWaypoints[1]
        };
    }


    void UpdateWaypoints()
    {
        var sceneWaypoints = GetMuralWaypoints();
        waypoints = new CameraWaypoint[] {
            followState, sceneWaypoints[0], sceneWaypoints[1]
        };
    }

    Bounds GetHierarchyBounds(GameObject hierarchy)
    {
        Renderer[] renderers = hierarchy.GetComponentsInChildren<Renderer>(true);
        Bounds bounds = renderers[0].bounds;

        for (int i = 0; i < renderers.Length; i++)
        {
            bounds.Encapsulate(renderers[i].bounds);
        }

        return bounds;
    }

    CameraWaypoint[] GetWaypoints(GameObject waypointsGameObject)
    {
        CameraWaypoint[] result = null;
        if (waypointsGameObject != null)
        {
            var waypointContainer = waypointsGameObject.transform;
            result = new CameraWaypoint[waypointContainer.childCount];
            for (int i = 0; i < waypointContainer.childCount; i++)
            {
                var childTransform = waypointContainer.GetChild(i); 
                result[i] = new CameraWaypoint {
                    position = childTransform.position,
                    rotation = childTransform.rotation,
                    fov = childTransform.GetComponent<Camera>().fieldOfView
                };
            }
        }
        return result;
    }

    CameraWaypoint[] GetMuralWaypoints()
    {
        return GetWaypoints(GameObject.Find("MuralZoomWaypoints"));
    }

    #endregion
}
