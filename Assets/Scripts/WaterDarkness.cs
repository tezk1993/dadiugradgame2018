﻿/**
 * WaterDarkness.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDarkness : MonoBehaviour
{
    #region Variables
    public float darknessRadius = 1.3f;
    public float pulse = 1;

    private Material material;
    #endregion

    #region Unity Methods

    public void Start()
    {
        material = GetComponent<Renderer>().material;
    }

    public void Update()
    {
        float radiusOffset = Mathf.Cos(Time.time * 1.5f + Mathf.Cos(Time.time * 3.2f) * 0.25f) * 0.04f;
        radiusOffset *= pulse;
        material.SetFloat("_Radius", darknessRadius + radiusOffset);
    }

    #endregion

    #region Methods

    #endregion
}
