﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerZone : MonoBehaviour {

    [Header("IMPORTANT:")]
    [Header("Player needs the player script on it for this to function!")]

    [Header("")]
    public float delay;
    Player Player;
    Coroutine dangerZoneCoroutine;

    public void Awake()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<Player>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            dangerZoneCoroutine = StartCoroutine(StartCountDown(collision));
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        StopCoroutine(dangerZoneCoroutine);
    }

    private IEnumerator StartCountDown(Collider collision)
    {
        yield return new WaitForSeconds(delay);
        Player.TakeDamage(gameObject);
    }
}
