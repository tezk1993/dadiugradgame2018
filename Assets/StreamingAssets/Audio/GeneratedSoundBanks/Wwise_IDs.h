/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_ANIMA_LURE = 4173043911U;
        static const AkUniqueID PLAY_BAMBOO_DESTROYED = 755227754U;
        static const AkUniqueID PLAY_BAMBOO_STICK = 1631405651U;
        static const AkUniqueID PLAY_BIGGUY_ATTACK_SLAM = 4066854932U;
        static const AkUniqueID PLAY_BIGGUY_ATTACK_WINDUP = 4211331902U;
        static const AkUniqueID PLAY_BOSS_INTRO_SEQUENCE = 626214642U;
        static const AkUniqueID PLAY_BOSS_OUTRO_SEQUENCE = 125733407U;
        static const AkUniqueID PLAY_CHASER_ATTACK_STRIKE = 2519020194U;
        static const AkUniqueID PLAY_CHASER_ATTACK_WINDUP = 3782471493U;
        static const AkUniqueID PLAY_CLOSE_MENU = 3819740920U;
        static const AkUniqueID PLAY_DASH_ATTACK_BOSS = 4104026327U;
        static const AkUniqueID PLAY_DASH_DENIED = 4009313840U;
        static const AkUniqueID PLAY_DASH_HERO = 576814053U;
        static const AkUniqueID PLAY_DASH_RECHARGE = 1372609206U;
        static const AkUniqueID PLAY_DASH_WINDUP_BOSS = 1305828828U;
        static const AkUniqueID PLAY_DEATH_BIG = 2747582231U;
        static const AkUniqueID PLAY_DEATH_BOSS = 3520353328U;
        static const AkUniqueID PLAY_DEATH_HERO = 1850460615U;
        static const AkUniqueID PLAY_DEATH_SMALL = 2419729128U;
        static const AkUniqueID PLAY_FOOTSTEPS_BOSS = 2456687621U;
        static const AkUniqueID PLAY_GATE_ENDING = 3505451827U;
        static const AkUniqueID PLAY_GUARD_ATTACK_CHARGE = 1043404327U;
        static const AkUniqueID PLAY_GUARD_ATTACK_WINDUP = 4086128386U;
        static const AkUniqueID PLAY_HIT_BOSS = 2045000163U;
        static const AkUniqueID PLAY_HIT_HERO = 1387045464U;
        static const AkUniqueID PLAY_HITBYDASH_ENEMY = 2022323175U;
        static const AkUniqueID PLAY_HITBYDASH_SHIELDED = 3235444141U;
        static const AkUniqueID PLAY_JUMP_BOSS = 3707726502U;
        static const AkUniqueID PLAY_LAND_BOSS = 1455329907U;
        static const AkUniqueID PLAY_LAUGH_BOSS = 2120454587U;
        static const AkUniqueID PLAY_LOCOMOTION_HERO = 1288866080U;
        static const AkUniqueID PLAY_LOST_ATTACK = 228798651U;
        static const AkUniqueID PLAY_MASS_IMPACT_BOSS = 263559081U;
        static const AkUniqueID PLAY_MASS_LOB_BOSS = 1029760328U;
        static const AkUniqueID PLAY_MASS_WINDUP_BOSS = 1637625532U;
        static const AkUniqueID PLAY_MIRROR_IMAGE_BOSS = 823988083U;
        static const AkUniqueID PLAY_MUSIC_SYSTEM = 3260968109U;
        static const AkUniqueID PLAY_NOVA_ATTACK_IMPACT = 3811014276U;
        static const AkUniqueID PLAY_NOVA_ATTACK_LOB = 964673089U;
        static const AkUniqueID PLAY_NOVA_ATTACK_WINDUP = 4011668827U;
        static const AkUniqueID PLAY_OPEN_MENU = 3151804212U;
        static const AkUniqueID PLAY_PHASE_CHANGE = 2151773798U;
        static const AkUniqueID PLAY_PORTAL = 2424882924U;
        static const AkUniqueID PLAY_SECOND_WIND_ACTIVATED = 2956095087U;
        static const AkUniqueID PLAY_SECOND_WIND_CHARGED = 3752037390U;
        static const AkUniqueID PLAY_SECOND_WIND_POWERUP = 2213997850U;
        static const AkUniqueID PLAY_SECOND_WIND_RESPAWN = 600854118U;
        static const AkUniqueID PLAY_SHIELD_ACTIVATE_BOSS = 245659229U;
        static const AkUniqueID PLAY_SHOOTER_ATTACK_SHOOT = 2428573409U;
        static const AkUniqueID PLAY_SHOOTER_ATTACK_WINDUP = 1208061853U;
        static const AkUniqueID PLAY_SPREAD_SHOOT_BOSS = 3082869285U;
        static const AkUniqueID PLAY_SPREAD_WINDUP_BOSS = 1200018737U;
        static const AkUniqueID PLAY_START_GAME = 3294915283U;
        static const AkUniqueID PLAY_STRUCTURES_DESTROYED = 2834811760U;
        static const AkUniqueID PLAY_STRUCTURES_IMPACT = 3599129793U;
        static const AkUniqueID PLAY_STRUCTURES_MOVEMENT = 2966937388U;
        static const AkUniqueID PLAY_UI_TAP = 2286464654U;
        static const AkUniqueID STOP_COOL_DASH = 1464196662U;
        static const AkUniqueID STOP_STRUCTURES_MOVEMENT = 1372852098U;
        static const AkUniqueID TRIGGER_COOL_DASH = 3203748196U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace BOSS_PHASES
        {
            static const AkUniqueID GROUP = 3456390069U;

            namespace STATE
            {
                static const AkUniqueID END = 529726532U;
                static const AkUniqueID INTRO = 1125500713U;
                static const AkUniqueID PHASE1 = 3630028971U;
                static const AkUniqueID PHASE2 = 3630028968U;
                static const AkUniqueID PHASE3 = 3630028969U;
            } // namespace STATE
        } // namespace BOSS_PHASES

        namespace DASH_COOL
        {
            static const AkUniqueID GROUP = 124777455U;

            namespace STATE
            {
                static const AkUniqueID ACTIVE = 58138747U;
                static const AkUniqueID DEACTIVE = 2759900874U;
            } // namespace STATE
        } // namespace DASH_COOL

        namespace GAME_STATE
        {
            static const AkUniqueID GROUP = 766723505U;

            namespace STATE
            {
                static const AkUniqueID GAMEPLAY = 89505537U;
                static const AkUniqueID SEQUENCE = 311444866U;
            } // namespace STATE
        } // namespace GAME_STATE

        namespace MENU_STATE
        {
            static const AkUniqueID GROUP = 3941853002U;

            namespace STATE
            {
                static const AkUniqueID ACTIVE = 58138747U;
                static const AkUniqueID DEACTIVE = 2759900874U;
            } // namespace STATE
        } // namespace MENU_STATE

        namespace MUSIC_STATE
        {
            static const AkUniqueID GROUP = 3826569560U;

            namespace STATE
            {
                static const AkUniqueID ANIMA = 1155287239U;
                static const AkUniqueID BATTLE = 2937832959U;
                static const AkUniqueID BOSS = 1560169506U;
                static const AkUniqueID SAFE = 938058686U;
            } // namespace STATE
        } // namespace MUSIC_STATE

        namespace PLAYER_STATE
        {
            static const AkUniqueID GROUP = 4071417932U;

            namespace STATE
            {
                static const AkUniqueID ALIVE = 655265632U;
                static const AkUniqueID DEAD = 2044049779U;
            } // namespace STATE
        } // namespace PLAYER_STATE

        namespace SECOND_WIND
        {
            static const AkUniqueID GROUP = 1721145506U;

            namespace STATE
            {
                static const AkUniqueID ACTIVE = 58138747U;
                static const AkUniqueID DEACTIVE = 2759900874U;
            } // namespace STATE
        } // namespace SECOND_WIND

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID COMBO_METER = 798590115U;
        static const AkUniqueID DASH_CHARGES = 2823030531U;
        static const AkUniqueID DISTANCE_FROM_PC = 3236604937U;
        static const AkUniqueID MASTER_VOLUME = 4179668880U;
        static const AkUniqueID MUSIC_VOLUME = 1006694123U;
        static const AkUniqueID SAFE_ZONE_PROGRESS = 759738019U;
        static const AkUniqueID SECOND_WIND_PROGRESS = 2143311084U;
        static const AkUniqueID SECOND_WIND_TIME = 3892156774U;
        static const AkUniqueID SLOWDOWN = 3101299514U;
        static const AkUniqueID SOUND_VOLUME = 495870151U;
        static const AkUniqueID THREAT_METER = 3033660203U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID END = 529726532U;
        static const AkUniqueID MAIN = 3161908922U;
        static const AkUniqueID MID = 1182670505U;
        static const AkUniqueID SOUNDBANK = 1661994096U;
        static const AkUniqueID START = 1281810935U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID ANIMA = 1155287239U;
        static const AkUniqueID CHARACTER = 436743010U;
        static const AkUniqueID ENEMIES = 2242381963U;
        static const AkUniqueID ENVIRONMENT = 1229948536U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SOUND = 623086306U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID COOLDASH_FX = 3936913611U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID DEFAULT_MOTION_DEVICE = 4230635974U;
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
