﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(enemySpawner))]
public class SpawnerEditor : Editor
{

    void OnSceneGUI()
    {
        enemySpawner spawner = (enemySpawner)target;
        for (int i = 0; i < spawner.enemyPoolSets.Count; i++)
        {
            Handles.color = spawner.enemyPoolSets[i].gizmoColor;
            Handles.DrawWireArc(spawner.transform.position, Vector3.up, Vector3.forward, 360, spawner.enemyPoolSets[i].spawnRadius);
        }
        

    }

}
