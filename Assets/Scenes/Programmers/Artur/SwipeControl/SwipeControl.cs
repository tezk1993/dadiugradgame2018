﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeControl : MonoBehaviour {

	public float speed = 16;
	public float dashFadeout = 32;

	Vector3 currVel = Vector3.zero;

	bool readyToDash = true;

	float dashStartTime = 0;
	float dashSpeedSustaionTime = 0.2f;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.touchCount > 0)
		{
			var touch = Input.GetTouch(0);
			if (touch.phase == TouchPhase.Ended)
			{
				readyToDash = true;
			}
			else if (touch.phase == TouchPhase.Moved)
			{
				if (readyToDash)
				{
					var delta = touch.deltaPosition.normalized * speed;
					currVel = new Vector3(delta.x, 0, delta.y);
					transform.rotation = Quaternion.LookRotation(currVel, Vector3.up);
					readyToDash = false;
					dashStartTime = Time.time;
				}
			}
		}

		transform.position += currVel * Time.deltaTime;
		//currVel = Vector3.MoveTowards(currVel, Vector3.zero, Time.deltaTime * dashFadeout);
		if (dashStartTime + dashSpeedSustaionTime < Time.time)
		{
			currVel = Vector3.Lerp(currVel, Vector3.zero, Time.deltaTime * dashFadeout);
		}
	}
}
