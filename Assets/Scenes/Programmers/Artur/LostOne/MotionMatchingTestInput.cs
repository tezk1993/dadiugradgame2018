﻿/**
 * MotionMatchingTestInput.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionMatchingTestInput : MonoBehaviour
{
    #region Variables

    public float speed = 2;
    public float updateDelay = 3;

    private MotionMatching motionMatching;

    #endregion

    #region Unity Methods

    public void Start()
    {
        motionMatching = GetComponent<MotionMatching>();
        StartCoroutine(UpdateInputCoroutine());
    }

    public void Update()
    {
    }


    IEnumerator UpdateInputCoroutine()
    {
        while(true)
        {
            float angle = Random.Range(0f, Mathf.PI * 2);
            bool stop = Random.RandomRange(0, 5) == 0;
            var inputVelocity = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
            inputVelocity *= speed;
            if (stop) inputVelocity = Vector3.zero;
            motionMatching.targetVelocity = inputVelocity;
            yield return new WaitForSeconds(updateDelay);
        }
    }


    #endregion

    #region Methods

    #endregion
}
