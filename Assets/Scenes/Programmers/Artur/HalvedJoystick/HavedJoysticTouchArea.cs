﻿/**
 * HavedJoysticTouchArea.cs
 * Created by: Artur Barnabas
 * Created on: 30/10/18 (dd/mm/yy)
 * 
 * This script is meant to be a base class for the left and right sides of the halved joystick
 * touch controller classes. (See HalvedJoysticRight.cs and HalvedJoysticLeft.cs)
 *
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

public class HavedJoysticTouchArea :
	MonoBehaviour, IDragHandler,  IPointerDownHandler, IPointerUpHandler
{
	/// A value between -1 and 1 representing where the vertical position of the touch on
	/// this ui element relative to the height of the ui element.
	[HideInInspector]
	public float inputPosition;

	[HideInInspector]
	public bool pressed;

	private RectTransform rectTransform;
	
	void Start ()
	{
		rectTransform = GetComponent<RectTransform>();
		GetComponent<UnityEngine.UI.Image>().color = Color.clear;
	}

	public void OnDrag(PointerEventData eventData)
	{
		UpdateInputPosition(eventData);
	}

	public void OnPointerDown(PointerEventData eventData)
    {
        pressed = true;
		UpdateInputPosition(eventData);
    }

    //Detect if clicks are no longer registering
    public void OnPointerUp(PointerEventData eventData)
    {
        pressed = false;
    }

	private void UpdateInputPosition(PointerEventData pointerEventData)
	{
		var rect = rectTransform.rect;
		float yOffset = pointerEventData.position.y - rect.yMin;

		// [0, 1]
		float relativeOffset = yOffset / rect.height;

		// [-1, 1]
		inputPosition = (relativeOffset - 0.5f) * 2;
	}
}
