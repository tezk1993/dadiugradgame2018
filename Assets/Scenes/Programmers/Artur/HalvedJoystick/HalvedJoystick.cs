﻿/**
 * HavedJoysticTouchArea.cs
 * Created by: Artur Barnabas
 * Created on: 30/10/18 (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalvedJoystick : MonoBehaviour
{

	public float speed;

	private HalvedJoysticLeft leftInput;
	private HalvedJoysticRight rightInput;

	// Use this for initialization
	void Start ()
	{
		leftInput = Object.FindObjectOfType<HalvedJoysticLeft>();
		rightInput = Object.FindObjectOfType<HalvedJoysticRight>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector2 inputDirection = Vector2.zero;

		const int steps = 3;
		const float angleRange = Mathf.PI / 4;

		if (leftInput.pressed)
		{
			int input = (int)((leftInput.inputPosition / 2) * steps);
			float angle = Mathf.PI - input * angleRange;
			var leftInputComponent = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
			inputDirection += leftInputComponent;
		}
		if (rightInput.pressed)
		{
			int input = (int)((rightInput.inputPosition / 2) * steps);
			float angle = input * angleRange;
			var rightInputComponent = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
			inputDirection += rightInputComponent;
		}

		if (inputDirection.magnitude > 0.1f)
		{
			inputDirection = inputDirection.normalized;

			var velocity = new Vector3(inputDirection.x, 0, inputDirection.y) * speed;
			transform.position += velocity * Time.deltaTime;
		}
	}
}
