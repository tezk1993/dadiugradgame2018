﻿/**
 * SwipeDashTouchEnd.cs
 * Created by: #AUTHOR#
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDashTouchEnd : MonoBehaviour
{
    #region Variables

    public float speed = 22;
	public float dashFadeout = 8;

	Vector3 currVel = Vector3.zero;

    Vector2 touchStartPos;

	bool readyToDash = true;

	float dashStartTime = 0;
	float dashSpeedSustaionTime = 0.2f;

    #endregion

    #region Unity Methods

    public void Start()
    {
    
    }

    public void Update()
    {
        if (Input.touchCount > 0)
		{
			var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                touchStartPos = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
			{
				readyToDash = true;
                var touchDelta = touch.position - touchStartPos;
                Dash(touchDelta);
			}
			else if (touch.phase == TouchPhase.Moved)
			{

			}
		}

		transform.position += currVel * Time.deltaTime;
		//currVel = Vector3.MoveTowards(currVel, Vector3.zero, Time.deltaTime * dashFadeout);
		if (dashStartTime + dashSpeedSustaionTime < Time.time)
		{
			currVel = Vector3.Lerp(currVel, Vector3.zero, Time.deltaTime * dashFadeout);
		}
    }

    #endregion

    #region Methods

    void Dash(Vector2 touchDirection)
    {
        if (readyToDash)
        {
            var delta = touchDirection.normalized * speed;
            currVel = new Vector3(delta.x, 0, delta.y);
            transform.rotation = Quaternion.LookRotation(currVel, Vector3.up);
            readyToDash = false;
            dashStartTime = Time.time;
        }
    }

    #endregion
}
