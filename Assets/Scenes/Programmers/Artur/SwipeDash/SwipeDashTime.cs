﻿/**
 * SwipeDashTimed.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDashTime : MonoBehaviour
{
    #region Variables

    public float speed = 22;
	public float dashFadeout = 8;

	Vector3 currVel = Vector3.zero;

    Vector2 touchStartPos;
    Vector2 lastTouchPos;
    float touchStartTime;
    public float maxSwipeTime = 0.1f;

	bool readyToDash = true;

	float dashStartTime = 0;
	float dashSpeedSustaionTime = 0.2f;

    #endregion

    #region Unity Methods

    public void Start()
    {
    }

    public void Update()
    {
        if (Input.touchCount > 0)
		{
			var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                touchStartTime = Time.unscaledTime;
                touchStartPos = touch.position;
                readyToDash = true;
            }
            else if (touch.phase == TouchPhase.Ended)
			{
                var touchDelta = touch.position - touchStartPos;
                Dash(touchDelta);
			}
			else if (touch.phase == TouchPhase.Moved)
			{
                lastTouchPos = touch.position;
			}
		}

        if (touchStartTime + maxSwipeTime < Time.unscaledTime)
        {
            var touchDelta = lastTouchPos - touchStartPos;
            Dash(touchDelta);
        } 

		transform.position += currVel * Time.deltaTime;
		//currVel = Vector3.MoveTowards(currVel, Vector3.zero, Time.deltaTime * dashFadeout);
		if (dashStartTime + dashSpeedSustaionTime < Time.time)
		{
			currVel = Vector3.Lerp(currVel, Vector3.zero, Time.deltaTime * dashFadeout);
		}
    }

    #endregion

    #region Methods

    void Dash(Vector2 touchDirection)
    {
        if (readyToDash)
        {
            var delta = touchDirection.normalized * speed;
            currVel = new Vector3(delta.x, 0, delta.y);
            transform.rotation = Quaternion.LookRotation(currVel, Vector3.up);
            readyToDash = false;
            dashStartTime = Time.time;
        }
    }

    #endregion
}
