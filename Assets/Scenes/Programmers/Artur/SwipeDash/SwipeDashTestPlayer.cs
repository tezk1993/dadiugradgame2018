﻿/**
 * SwipeDashTestPlayer.cs
 * Created by: #AUTHOR#
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDashTestPlayer : MonoBehaviour
{
    #region Variables
    public float spawnDistance = 6;
    public GameObject targetPrefab;

    #endregion

    #region Unity Methods

    public void Start()
    {
        CreateTarget();
    }

    public void Update()
    {
    }

    public void OnTriggerEnter(Collider collider)
    {
        var target = collider.gameObject.GetComponent<SwipeDashTarget>();
        if (target != null)
        {
            Destroy(collider.gameObject);

            CreateTarget();
        }
    }

    #endregion

    #region Methods

    private void CreateTarget()
    {
        var randDir = Random.onUnitSphere * spawnDistance;
        var newPos = transform.position + new Vector3(randDir.x, 0, randDir.z);
        Instantiate(targetPrefab, newPos, Quaternion.identity);
    }

    #endregion
}
