﻿/**
 * AnimationControllerTestPlayer.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControllerTestPlayer : MonoBehaviour
{
    #region Variables
    //Joystick joystick;
    Animator animator;

    MovementAdap movementInput;

    #endregion

    #region Unity Methods

    public void Start()
    {
        // joystick = Object.FindObjectOfType<Joystick>();
        animator = GetComponent<Animator>();
        movementInput = GetComponent<MovementAdap>();
    }

    public void Update()
    {
        //var direction = new Vector3(joystick.Horizontal, 0, joystick.Vertical);
        var dir = movementInput.dir;
        var velocity = new Vector3(dir.x, 0, dir.y) * movementInput.speed;

        bool shouldMove = movementInput.dir.magnitude > 0.2f;
        if (shouldMove)
        {
            animator.SetBool("Running", true);
            transform.position += velocity * Time.deltaTime;
            transform.rotation = Quaternion.LookRotation(velocity.normalized, Vector3.up);
        }
        else
        {
            animator.SetBool("Running", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetTrigger("Attack");
        }

        var animState = animator.GetCurrentAnimatorStateInfo(0);
        if (animState.IsName("Attack"))
        {
            //transform.rotation *= animator.deltaRotation;
            //transform.position += animator.deltaPosition;
        }
        else if (shouldMove)
        {
        }
    }

    #endregion

    #region Methods

    #endregion
}
