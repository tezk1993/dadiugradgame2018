﻿/**
 * MoveInCircle.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveInCircle : MonoBehaviour
{
    #region Variables

    public float radius = 4;
    public float speed = 2;

    Vector3 startPos;

    #endregion

    #region Unity Methods

    public void Start()
    {
        startPos = transform.position;
    }

    public void Update()
    {
        float t = Time.time * speed;
        var offset = new Vector3(Mathf.Cos(t), 0, Mathf.Sin(t)) * radius;
        transform.position = startPos + offset;
    }

    #endregion

    #region Methods

    #endregion
}
