﻿/**
 * TestSceneSwitchTrigger.cs
 * Created by: Artur Barnabas
 * Created on: #CREATIONDATE# (dd/mm/yy)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSceneSwitchTrigger : MonoBehaviour
{
    #region Variables

    #endregion

    #region Unity Methods

    public void Start()
    {
    }

    public void Update()
    {
    }

    #endregion

    #region Methods

    public void OnTriggerEnter(Collider collider)
    {
       // Object.FindObjectOfType<SceneSwitcher>().LoadScene("Gym - Adaptive Joystick 3");
    }

    #endregion
}
