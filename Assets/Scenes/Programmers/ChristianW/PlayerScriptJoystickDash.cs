﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.AI;



[RequireComponent(typeof(TrailRenderer))]
public class PlayerScriptJoystickDash : MonoBehaviour {
    public delegate void EnemyHit();
    public static event EnemyHit hit;
    EventManager eventManager;

    public delegate void LoseCombo();
    public static event LoseCombo loseCombo;

    [HideInInspector]
    private int screenWidth;
    [HideInInspector]
    private int screenHeigth;

    [Header("REMEMBER!!!!")]
    [Header("DebugUI prefab + HealthUI prefab in the scene!")]
    [Header("")]
    [Header("")]
    [Header("Movement Stuff")]
    public float startSpeed;
    public float dashSpeed;
    public float dashTime;
    public float dashDuration = 0.2f;
    private float curDashTime = 0;
    public int maxDashCharges;
    private int dashCharges;
    [Tooltip("How long should the dash-cooldown be")]
    public float cooldownTime;
    private float cooldown;
    private Text cooldownText;
    public bool dashing;
    public ParticleSystem dashEffect;
    public float invis = 0.5f;

    private float upwardAndDownwardModifier;
    public float joystickScreenBorder = 20;
    public float fingerMaxDistance = 30;

    private bool rightTouched;
    private bool leftTouched;
    private Vector3 dashVelocity;

    private Transform circle;
    private Transform outerCircle;
    private Transform circle2;
    private Transform outerCircle2;
    public Transform dirIndicator;
    public Rigidbody rb;

    private float speed;
    private bool startTouch;
    private Vector2 pointA;
    private Vector2 pointB;
    private Vector2 pointADash;
    private Vector2 pointBDash;

    // Not used 
    //[Header("Dash Stuff")]
    [Tooltip("How far should the character dash")]
    private float dashRange;
    [Tooltip("How fast should the dash-speed decrease")]
    private float speedDecrease;
    private float dashStartTime;
    [Tooltip("How long should the character be dashing")]
    private float maxDashTime = 1.0f;
    private float dashWindDownTime = 0.2f;
    private float dashWindDownMinSpeed;
    private float dashWindDownTimer = 0;
    private float currentDashTime;
    private bool readyToDash;
    // -

    [Header("Second Wind Stuff")]
    public State secondWindState;
    private List<State> enemiesPrevState;
    public GameObject safeSpace;
    private GameObject safeSpaceZone;
    private Image secondWindUI;
    private bool savedBySecondWind;
    public float secondWindDuration;
    private float secondWindUITimer;
    private bool startSecondWindTimer;


    [Header("Health Stuff")]
    [Tooltip("container of health icons")] private GameObject healthContainer;
    public GameObject[] healthArray;
    public int healthInt;    

    [Header("Screenshake Stuff")]
    public float screenshakeMult = 1.5f;
    public float screenshakeDuration = 0.35f;

    [Header("Time Slow Stuff")]
    public float timeSlowDegree;
    public float timeSlowDuration;

    [Header("Trail Renderer Stuff")]
    [Tooltip("This is the material for the trail, defining its color")]
    public Material trailMaterial;
    [Tooltip("The fade time of the trail")]
    public float trailTime = .5f;
    [Tooltip("The width of the trails head")]
    public float trailStartWidth = 1;
    [Tooltip("The width of the trails tail")]
    public float trailEndWidth = 0.5f;
    TrailRenderer trail;

    [Tooltip("A number between 0 and 1 specifying the size of area in the middle of the joystick where the input is not registered.")]
    public float joystickDeadzone = 0.2f;

    public float timeLeftVariable = 0.2f;
    private float timeleft;

    private bool aboutToDie;

    private float dashStoppingSpeed = 0.1f;
    private bool hasStoppedMoving;
    private Vector3 direction;


    float inputAngleOffset;
    
    Animator animator;

    // Use this for initialization
    void Start () {

        screenHeigth = Screen.height;
        screenWidth = Screen.width;

        circle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Inner");
        outerCircle = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Outer");
        circle2 = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Inner2");
        outerCircle2 = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Outer2");
        cooldownText = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Cooldown text").GetComponent<Text>();
        healthContainer = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("HealthContainerForRun").gameObject;
        eventManager = GameObject.FindGameObjectWithTag("EventManager").transform.GetComponent<EventManager>();
        rb = GetComponent<Rigidbody>();

        dashCharges = maxDashCharges;
        speed = startSpeed;
        aboutToDie = false;

        direction = Vector3.zero;
        currentDashTime = maxDashTime;
        cooldown = cooldownTime;
        trail = GetComponent<TrailRenderer>();
        trail.material = trailMaterial;
        trail.startWidth = trailStartWidth;
        trail.endWidth = trailEndWidth;
        trail.time = trailTime;
        trail.enabled = false;
        //Debug.Log(healthContainer.transform.childCount);

        healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }
        
        secondWindUI = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("SecondWindUI").GetComponent<Image>();
        secondWindUI.gameObject.SetActive(false);
        secondWindUITimer = 0;

        var mainCam = Camera.main;
        inputAngleOffset = -Vector3.SignedAngle(Vector3.forward, mainCam.transform.forward, Vector3.up);
        
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        

        Movement();
        //dashSpeed = Vector3.Lerp(dashSpeed, new Vector3(0, direction.y, 0), Time.deltaTime * speedDecrease);
        //transform.position += dashSpeed * Time.deltaTime;
        //transform.Translate(dashSpeed);

        if (dashCharges < maxDashCharges)
        {
            cooldown -= Time.deltaTime;

            if (cooldown <= 0)
            {
                dashCharges++;
                cooldown = cooldownTime;
                AkSoundEngine.PostEvent("Play_Dash_Recharge", gameObject);
            }
        }
        
        cooldownText.text = "Dash Charge: " + dashCharges.ToString() + "\n" + "Dash CD: " + cooldown.ToString("F2");

        if (startSecondWindTimer == true)
        {
            if (secondWindUITimer > 0)
            {
                secondWindUITimer -= Time.deltaTime;
                secondWindUI.fillAmount = secondWindUITimer / secondWindDuration;
            }

            dashCharges = 1;
            maxDashCharges = 1;
        }

    }

    private void FixedUpdate()
    {
        invis -= Time.deltaTime;
        bool isRunning = false;
        if (startTouch && !aboutToDie)
        {
            timeleft -= Time.deltaTime;
            Vector2 offset = pointB - pointA;
            if (offset.magnitude > fingerMaxDistance)
            {
                offset = Vector2.ClampMagnitude(offset, fingerMaxDistance);
                pointA = pointB - offset;
                AdjustJoystickToBorder();
            }

            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            circle.transform.position = new Vector3(pointA.x + offset.x, pointA.y + offset.y, 0);

            //Duo Joystick
            Vector2 offsetDash = pointBDash - pointADash;
            Vector2 directionDash = Vector2.ClampMagnitude(offsetDash, 1.0f);
            circle2.transform.position = new Vector3(pointADash.x + directionDash.x * 50, pointADash.y + directionDash.y * 50, Camera.main.transform.position.z);
            // Button setup
            Vector2 offsetDashButton = pointB - pointA;
            Vector2 directionDashButton = Vector2.ClampMagnitude(offsetDashButton, 1.0f);

            if (direction.magnitude > joystickDeadzone && !dashing)
            {
                isRunning = true;
                var angle = Vector2.SignedAngle(new Vector2(1, 0), direction.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                //Joystick dash
                var angleIndi = Vector2.SignedAngle(new Vector2(1, 0), directionDash.normalized);
                angleIndi += inputAngleOffset;
                angleIndi *= Mathf.Deg2Rad;
                directionDash = new Vector2(Mathf.Cos(angleIndi), Mathf.Sin(angleIndi));
                var velocityIndi = new Vector3(direction.x, 0, direction.y) * speed;
                var velocityIndiDash = new Vector3(directionDash.x, 0, directionDash.y) * speed;
                //Button dash
                var angleIndiButton = Vector2.SignedAngle(new Vector2(1, 0), directionDashButton.normalized);
                angleIndiButton += inputAngleOffset;
                angleIndiButton *= Mathf.Deg2Rad;
                directionDashButton = new Vector2(Mathf.Cos(angleIndiButton), Mathf.Sin(angleIndiButton));
                var velocityIndiButton = new Vector3(direction.x, 0, direction.y) * speed;
                var velocityIndiDashButton = new Vector3(direction.x, 0, direction.y) * speed;

                //Debug.Log(velocityIndi);
                if (Input.touchCount == 1) { 
                    dirIndicator.transform.rotation = Quaternion.LookRotation(velocityIndi, Vector3.up);
                }
                else if(Input.touchCount == 2)
                {
                    dirIndicator.transform.rotation = Quaternion.LookRotation(velocityIndiDash, Vector3.up);

                }

                //Convert to vector 3 rotate 45 degrees to match camera rotation

                if (direction.x > 0.7f && direction.x < 1f || direction.x < -0.7f && direction.x > -1f) {
                    var velocity = new Vector3(direction.x, 0, direction.y);
                    dashVelocity = velocity;
                    velocity = new Vector3(direction.x, 0, direction.y) * (speed + upwardAndDownwardModifier);
                    transform.Translate(velocity * Time.deltaTime, Space.World);
                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
                else
                {
                    var velocity = new Vector3(direction.x, 0, direction.y) * speed;
                    dashVelocity = velocity;
                    velocity = new Vector3(direction.x, 0, direction.y);
                    transform.Translate(velocity * Time.deltaTime, Space.World);
                    transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
                }
      
            }

            if (dashing && timeleft < 0)
            {
                //Debug.Log("Joystick");
                dashEffect.Play();
                var angle = Vector2.SignedAngle(new Vector2(1, 0), directionDash.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                directionDash = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                //Convert to vector 3 rotate 45 degrees to match camera rotation
                var velocity = new Vector3(directionDash.x, 0, directionDash.y) * speed;
                transform.Translate(velocity * Time.deltaTime, Space.World);
                transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
            }
            else if (dashing)
            {
                //Debug.Log("Button");

                
                var angle = Vector2.SignedAngle(new Vector2(1, 0), directionDashButton.normalized);
                angle += inputAngleOffset;
                angle *= Mathf.Deg2Rad;
                directionDashButton = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

                //Convert to vector 3 rotate 45 degrees to match camera rotation
                var velocity = new Vector3(directionDashButton.x, 0, directionDashButton.y) * speed;
                transform.Translate(velocity * Time.deltaTime, Space.World);
                transform.rotation = Quaternion.LookRotation(velocity, Vector3.up);
            }
        }
        
        animator.SetBool("Running", isRunning);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            TakeDamage();
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {
            if (collision.contacts[0].normal.z <= 0)
            {
                if (dashing)
                {
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (aboutToDie)
                {
                    savedBySecondWind = true;
                    ChooseYourDestiny();
                }

                if (hit != null)
                {
                    hit();
                }
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                //SlowTime(timeSlowDegree, timeSlowDuration);
                dashCharges++;
            }
            else
            {
               // TakeDamage();
            }

        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {                
                AkSoundEngine.PostEvent("Play_Structures_Impact", gameObject);
                StopDash();
            }                
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {
            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (hit != null)
                {
                    hit();
                }
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                //SlowTime(timeSlowDegree, timeSlowDuration);
                dashCharges++;
            }
            else
            {
               // TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structures_Impact", gameObject);
                StopDash();
            }
        }
    }

    public void Dashed()
    {
        circle2.gameObject.SetActive(false);
        outerCircle2.gameObject.SetActive(false);
        
        dashCharges--;
        speed = dashSpeed;
        dashing = true;
        trail.enabled = true;
        //Invoke("StopDash", dashTime);
        StartCoroutine(DashWindDown());
        animator.SetTrigger("Attack");
        AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);
    }

    public void TakeDamage()
    {
        if (invis <= 0.0f && aboutToDie == false)
        {
            if (healthInt > 0)
            {
                if (loseCombo != null)
                {
                    loseCombo();
                }
                healthArray[healthInt - 1].SetActive(false);
                healthInt--;
                AkSoundEngine.PostEvent("Play_Hit_Hero", gameObject);
            }

            if (healthInt <= 0)
            {
                aboutToDie = true;
                StartCoroutine(secondWind());                
            }
            invis = 0.5f;
        }  
    }

    public void ChooseYourDestiny()
    {
        if (savedBySecondWind == true)
        {
            aboutToDie = false;
            maxDashCharges = 2;

            for (int i = 0; i < eventManager.Enemies.Count / 2; i++)
            {
                eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState = enemiesPrevState[i];
            }

            startSecondWindTimer = false;
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            startTouch = false;
            AkSoundEngine.PostEvent("Play_Death_Hero", gameObject);
        }
    }

    public IEnumerator secondWind()
    {
        enemiesPrevState = new List<State>();

        for (int i = 0; i < eventManager.Enemies.Count/2; i++)
        {
            enemiesPrevState.Add(eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState);
            eventManager.Enemies[i].gameObject.GetComponent<StateController>().currentState = secondWindState;
        }

        rb.isKinematic = true;
        safeSpaceZone = Instantiate(safeSpace, this.transform, false);
        float progress = 0;

        while (progress < 1)
        {
            safeSpaceZone.transform.localScale = Vector3.Lerp(safeSpaceZone.transform.localScale, new Vector3(3, 3, 3), progress);
            progress += Time.deltaTime;
            yield return null;
        }

        startSecondWindTimer = true;
        secondWindUI.gameObject.SetActive(true);
        secondWindUITimer = secondWindDuration;
        Destroy(safeSpaceZone);
        rb.isKinematic = false;

        yield return new WaitForSeconds(secondWindDuration);
        ChooseYourDestiny();
    }

    public IEnumerator SlowTime(float timeSlowDegree, float timeSlowDuration)
    {
        Time.timeScale = timeSlowDegree;
        yield return new WaitForSecondsRealtime(timeSlowDuration);
        Time.timeScale = 1;
    }

    void Movement()
    {
        if (Input.touchCount > 0 && aboutToDie)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                timeleft = timeLeftVariable;

                if (Input.GetTouch(0).position.y < 900)
                {
                    pointADash = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);

                    circle2.transform.position = pointADash;
                    outerCircle2.transform.position = pointADash;

                    AdjustJoystickToBorder();

                    circle2.gameObject.SetActive(true);
                    outerCircle2.gameObject.SetActive(true);

                    dirIndicator.gameObject.SetActive(true);
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {

                if (Input.GetTouch(0).position.y < 900)
                {

                    pointBDash = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {

                if (Input.GetTouch(0).position.y < 900)
                {
                    pointBDash = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                StartCoroutine(Dash());
                //Dashed();
            }
        } 
        else if (Input.touchCount > 0 && !aboutToDie)
        {

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {

                if (Input.GetTouch(0).position.x < screenWidth / 2) // Left side
                {
                    leftTouched = true;

                    pointA = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);

                    circle.transform.position = pointA;
                    outerCircle.transform.position = pointA;

                    AdjustJoystickToBorder();

                    circle.gameObject.SetActive(true);
                    outerCircle.gameObject.SetActive(true);
                    dirIndicator.gameObject.SetActive(true);
                }
                if (Input.GetTouch(0).position.x > screenWidth / 2) // if the first touch is on the right side of the screen
                {
                    rightTouched = true;
                    pointADash = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);

                    circle2.transform.position = pointADash;
                    outerCircle2.transform.position = pointADash;

                    circle2.gameObject.SetActive(true);
                    outerCircle2.gameObject.SetActive(true);

                    dirIndicator.gameObject.SetActive(true);

                }
            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {

                startTouch = true;
                if (Input.GetTouch(0).position.y < 900 && leftTouched)
                {
                    pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                }
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                startTouch = true;
                if (Input.GetTouch(0).position.y < 900 && leftTouched)
                {
                    pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
                }
            }
            else
            {
                startTouch = false;
            }


            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                startTouch = false;
                if (leftTouched)
                {
                    leftTouched = false;
                    circle.gameObject.SetActive(false);
                    outerCircle.gameObject.SetActive(false);
                }
                else
                {
                    circle2.gameObject.SetActive(false);
                    outerCircle2.gameObject.SetActive(false);
                    rightTouched = false;
                    if (dashCharges > 0)
                    {
                        dashCharges--;
                        StartCoroutine(Dash());
                    }
                    else
                    {
                        AkSoundEngine.PostEvent("Play_Dash_Denied", gameObject);
                    }



                }
                if (Input.touchCount > 1)
                {
                    if (Input.GetTouch(1).phase == TouchPhase.Began)
                    {
                        //timeleft = timeLeftVariable;

                        if (Input.GetTouch(1).position.y < 900)
                        {
                            if (Input.GetTouch(1).position.x < screenWidth / 2 && rightTouched)
                            {
                                pointA = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);

                                circle.transform.position = pointA;
                                outerCircle.transform.position = pointA;

                                AdjustJoystickToBorder();

                                circle.gameObject.SetActive(true);
                                outerCircle.gameObject.SetActive(true);
                                dirIndicator.gameObject.SetActive(true);
                            }

                            if (Input.GetTouch(1).position.x > screenWidth / 2 && leftTouched) // if the second touch is on the right side of the screen
                            {
                                pointADash = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);

                                circle2.transform.position = pointADash;
                                outerCircle2.transform.position = pointADash;

                                circle2.gameObject.SetActive(true);
                                outerCircle2.gameObject.SetActive(true);

                                dirIndicator.gameObject.SetActive(true);

                            }
                        }
                    }

                    if (Input.GetTouch(1).phase == TouchPhase.Moved)
                    {
                        if (Input.GetTouch(1).position.y < 900 && rightTouched)
                        {
                            pointB = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                        if (Input.GetTouch(1).position.y < 900 && leftTouched)
                        {
                            pointBDash = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }

                    }
                    if (Input.GetTouch(1).phase == TouchPhase.Stationary)
                    {
                        if (Input.GetTouch(1).position.y < 900 && rightTouched)
                        {
                            pointB = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                        if (Input.GetTouch(1).position.y < 900 && leftTouched)
                        {
                            pointBDash = new Vector3(Input.GetTouch(1).position.x, Input.GetTouch(1).position.y, Camera.main.transform.position.z);
                        }
                    }

                    if (Input.GetTouch(1).phase == TouchPhase.Ended)
                    {

                        if (rightTouched)
                        {
                            leftTouched = false;
                            circle.gameObject.SetActive(false);
                            outerCircle.gameObject.SetActive(false);
                        }
                        else
                        {
                            rightTouched = false;

                            if (dashCharges > 0)
                            {
                                dashCharges--;
                                StartCoroutine(Dash());
                            }
                            else
                            {
                                AkSoundEngine.PostEvent("Play_Dash_Denied", gameObject);
                            }
                        }
                    }
                }
            }
        }
    }

    void AdjustJoystickToBorder()
    {
        pointA.x = Mathf.Max(pointA.x, joystickScreenBorder);
        pointA.x = Mathf.Min(pointA.x, screenWidth - joystickScreenBorder);
        pointA.y = Mathf.Max(pointA.y, joystickScreenBorder);
        pointA.y = Mathf.Min(pointA.y, screenHeigth - joystickScreenBorder);

        outerCircle.transform.position = pointA;
    }

    public IEnumerator DashWindDown()
    {
        
        while (dashWindDownTime > dashWindDownTimer)
        {
            //speed = dashSpeed -dashWindDownMinSpeed- dashWindDownTimer / dashWindDownTime * dashSpeed + dashWindDownMinSpeed;
            speed = Mathf.Lerp(dashWindDownMinSpeed, startSpeed, dashWindDownTimer/dashWindDownTime);
            yield return null;
            dashWindDownTimer += Time.deltaTime;
        }
        speed = startSpeed;
    }

    public IEnumerator Dash()
    {
        dashEffect.Play();
        curDashTime = 0;
        speed = dashSpeed;
        dashing = true;
        trail.enabled = true;
        animator.SetTrigger("Attack");
        AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);
        while (curDashTime < dashDuration)
        {
            curDashTime += Time.deltaTime;
            transform.Translate(dashVelocity * Time.deltaTime * speed, Space.World);
            transform.rotation = Quaternion.LookRotation(dashVelocity, Vector3.up);
            yield return null;
        }
        StopDash();
    }
    
    public void StopDash()
    {
        dashWindDownTimer = 0;
        speed = dashWindDownMinSpeed;
        dashing = false;
        trail.enabled = false;
        StartCoroutine(DashWindDown());
    }
}
