﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VJMovement : MonoBehaviour {

    public float startSpeed;
    public float dashSpeed;
    public float dashTime;
    public Transform circle;
    public Transform outerCircle;

    private float speed;
    private bool isDashing;
    private bool startTouch;
    private Vector2 pointA;
    private Vector2 pointB;


	// Use this for initialization
	void Start () {
        speed = startSpeed;
	}
	
	// Update is called once per frame
	void Update () {
        
        Movement();

    }
    private void FixedUpdate()
    {
        if (startTouch)
        {
            Vector2 offset = pointB - pointA;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1.0f);
            transform.Translate(direction.x * speed * Time.deltaTime, 0, direction.y * speed * Time.deltaTime);

            circle.transform.position = new Vector3(pointA.x + direction.x * 50, pointA.y + direction.y * 50, Camera.main.transform.position.z);
        }
        

    }

    void Movement()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {

                pointA = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);

                circle.transform.position = pointA;
                outerCircle.transform.position = pointA;

                circle.gameObject.SetActive(true);
                outerCircle.gameObject.SetActive(true);



            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                startTouch = true;
                pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                startTouch = true;

                pointB = new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, Camera.main.transform.position.z);
            }
            else
            {
                startTouch = false;
            }


            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                circle.gameObject.SetActive(false);
                outerCircle.gameObject.SetActive(false);

            }
            if (Input.touchCount == 2 && Input.GetTouch(1).phase == TouchPhase.Began)
            {
                speed = dashSpeed;
                isDashing = true;
                Invoke("StopDash", dashTime);

            }
        }
    }
    
    void StopDash()
    {
        speed = startSpeed;
        isDashing = false;
    }
}
