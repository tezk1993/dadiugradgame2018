﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WASDMovement : MonoBehaviour {
    public delegate void EnemyHit();
    public static event EnemyHit hit;

    public delegate void LoseCombo();
    public static event LoseCombo loseCombo;
    [Header("Testing")]
    public int screenWidth;
    public int screenHeigth;

    [Header("REMEMBER!!!!")]
    [Header("DebugUI prefab + HealthUI prefab in the scene!")]
    [Header("")]

    public float upwardAnddownwardModifier;

    private bool leftFirstTouch;
    private bool rightTouched;
    private bool leftTouched;
    private Vector3 dashVelocity;

    private Transform circle;
    private Transform outerCircle;
    private Transform circle2;
    private Transform outerCircle2;
    public Transform dirIndicator;
    public Transform chargeIndicator;


    private float speed;
    //private bool isDashing;
    private bool startTouch;
    private Vector2 pointA;
    private Vector2 pointB;
    private Vector2 pointADash;
    private Vector2 pointBDash;

    [Header("Dash Stuff")]
    public float startSpeed;
    public float dashSpeed;
    public float dashTime;
    public float curDashTime = 0;
    public float durDash = 0.2f;
    public bool dashing;
    [Tooltip("How long should the dash-cooldown be")]
    public float cooldownTime = 1;
    private Text cooldownText;
    public ParticleSystem dashEffect;

    [Tooltip("A number between 0 and 1 specifying the size of area in the middle of the joystick where the input is not registered.")]
    public float joystickDeadzone = 0.2f;

    [Tooltip("The time the user has to let go of the screen in order for it to be counted as a tap")]
    public float tapTime = 0.2f;
    [Tooltip("The time inbetween each charge upgrade")]
    public float chargeTime = 0.5f;
    private float timeleft;
    private float dashChargeTime;
    private bool chargingDash;
    private bool rightHolding;
    public int chargeMultiplier = 1;

    public int maxDashCharges = 2;
    private int dashCharges;

    private bool hasDied;


    private float currentDashTime;
    private float cooldown;
    private bool readyToDash;
    private bool hasStoppedMoving;
    private Vector3 direction;

    [Header("Health Stuff")]
    [Tooltip("container of health icons")] private GameObject healthContainer;
    public GameObject[] healthArray;
    public int healthInt = 3;


    [Header("Screenshake Stuff")]
    public float screenshakeMult = 1.5f;
    public float screenshakeDuration = 0.35f;

    [Header("Time Slow Stuff")]
    public AnimationCurve timeSlowCurve;
    public float numberOfTimeScaleUpdates;
    private float timeScaleUpdateCounter;
    public float timeSlowCurveDuration;

    public float invis = 0.5f;

    [Header("Trail Renderer Stuff")]
    [Tooltip("This is the material for the trail, defining its color")]
    public Material trailMaterial;
    [Tooltip("The fade time of the trail")]
    public float trailTime = .5f;
    [Tooltip("The width of the trails head")]
    public float trailStartWidth = 1;
    [Tooltip("The width of the trails tail")]
    public float trailEndWidth = 0.5f;
    TrailRenderer trail;
    Animator animator;

    // Use this for initialization
    void Start()
    {
        screenHeigth = Screen.height;
        screenWidth = Screen.width;
        cooldownText = GameObject.FindGameObjectWithTag("IngameUI").transform.Find("Cooldown text").GetComponent<Text>();
        healthContainer = GameObject.FindGameObjectWithTag("HealthUI").transform.Find("HealthContainerForRun").gameObject;
        
        dashCharges = maxDashCharges;
        speed = startSpeed;
        cooldown = cooldownTime;
        trail = GetComponent<TrailRenderer>();
        trail.material = trailMaterial;
        trail.startWidth = trailStartWidth;
        trail.endWidth = trailEndWidth;
        trail.time = trailTime;
        trail.enabled = false;
        animator = GetComponent<Animator>();

        healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }
    }

    private void Update()
    {
        invis -= Time.deltaTime;       

        if (dashCharges < maxDashCharges)
        {
            cooldown -= Time.deltaTime;

            if (cooldown <= 0)
            {
                dashCharges++;
                cooldown = cooldownTime;
                AkSoundEngine.PostEvent("Play_Dash_Recharge", gameObject);
            }
        }
        cooldownText.text = "Dash Charge: " + dashCharges.ToString() + "\n" + "Dash CD: " + cooldown.ToString("F2");

    }

    private void FixedUpdate()
    {

        bool isRunning = false;
        if (!hasDied)
        {
            if (Input.GetKey(KeyCode.D))
            {
                isRunning = true;

                if (Input.GetKey(KeyCode.W))
                {
                    transform.Translate((Vector3.right) * speed * Time.deltaTime);
                }
                else if (Input.GetKey(KeyCode.S))
                {
                    transform.Translate((-Vector3.forward) * speed * Time.deltaTime);
                }
                else
                {
                    transform.Translate((Vector3.right - Vector3.forward) * speed * Time.deltaTime);
                }
            }
            else if (Input.GetKey(KeyCode.A))
            {
                isRunning = true;

                if (Input.GetKey(KeyCode.W))
                {
                    transform.Translate((Vector3.forward) * speed * Time.deltaTime);
                }
                else if (Input.GetKey(KeyCode.S))
                {
                    transform.Translate((-Vector3.right) * speed * Time.deltaTime);
                }
                else
                {
                    transform.Translate((-Vector3.right + Vector3.forward) * speed * Time.deltaTime);
                }
            }
            else if (Input.GetKey(KeyCode.S))
            {
                isRunning = true;

                if (Input.GetKey(KeyCode.A))
                {
                    transform.Translate((-Vector3.right) * speed * Time.deltaTime);
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    transform.Translate((-Vector3.forward) * speed * Time.deltaTime);
                }
                else
                {
                    transform.Translate((-Vector3.right - Vector3.forward) * speed * Time.deltaTime);
                }
            }
            else if (Input.GetKey(KeyCode.W))
            {
                isRunning = true;

                if (Input.GetKey(KeyCode.A))
                {
                    transform.Translate((Vector3.forward) * speed * Time.deltaTime);
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    transform.Translate((Vector3.right) * speed * Time.deltaTime);
                }
                else
                {
                    transform.Translate((Vector3.right + Vector3.forward) * speed * Time.deltaTime);
                }
            }

            if (Input.GetKey(KeyCode.Space))
            {
                isRunning = true;

                StartCoroutine(Dash());
            }
        }
        animator.SetBool("Running", isRunning);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bullet"))
        {
            TakeDamage();
            Bullet bullet = other.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Shield"))
        {

            if (collision.contacts[0].normal.z >= 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject);
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (hit != null)
                {
                    hit();
                }
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject);
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                //SlowTime(timeSlowDegree, timeSlowDuration);
                dashCharges++;
            }
            else
            {
                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                AkSoundEngine.PostEvent("Play_Structure_Impact", gameObject);
                StopDash();
            }


        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            if (dashing)
            {
                StopDash();
            }

        }
        else if (collision.gameObject.CompareTag("Shield"))
        {
            if (collision.contacts[0].normal.z < 0)
            {
                if (dashing)
                {
                    AkSoundEngine.PostEvent("Play_HitByDash_Shielded", gameObject); // test to see if the sound would keep playing
                    StopDash();
                }

                TakeDamage();
            }
        }
        else if (collision.gameObject.CompareTag("Killable"))
        {
            if (dashing)
            {
                if (hit != null)
                {
                    hit();
                }
                AkSoundEngine.PostEvent("Play_HitByDash_Enemy", gameObject); // test to see if the sound would keep playing
                collision.gameObject.GetComponent<StateController>().Kill();
                Camera.main.GetComponent<CameraShake>().ShakeCam(screenshakeMult, screenshakeDuration);
                dashCharges++;
            }
            else
            {
                TakeDamage();
            }
        }
    }

    public void TakeDamage()
    {
        if (invis <= 0.0f)
        {
            if (healthInt > 0)
            {
                if (loseCombo != null)
                {
                    loseCombo();
                }
                healthArray[healthInt - 1].SetActive(false);
                healthInt--;
                AkSoundEngine.PostEvent("Play_Hit_Hero", gameObject);

            }

            if (healthInt <= 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                hasDied = true;
                startTouch = false;
                AkSoundEngine.PostEvent("Play_Death_Hero", gameObject);
            }
            invis = 0.5f;
        }
    }

    public IEnumerator SlowTimeNew()
    {
        while (timeScaleUpdateCounter < numberOfTimeScaleUpdates)
        {
            Time.timeScale = timeSlowCurve.Evaluate(timeScaleUpdateCounter / numberOfTimeScaleUpdates);
            timeScaleUpdateCounter++;
            yield return new WaitForSecondsRealtime(timeSlowCurveDuration / numberOfTimeScaleUpdates);
        }
        Time.timeScale = 1;
    }

    IEnumerator Dash()
    {
        chargingDash = false;

        dashEffect.Play();
        curDashTime = 0;
        speed = dashSpeed;
        dashing = true;
        trail.enabled = true;
        animator.SetTrigger("Attack");
        AkSoundEngine.PostEvent("Play_Dash_Hero", gameObject);
        while (curDashTime < durDash * chargeMultiplier)
        {
            curDashTime += Time.deltaTime;
            transform.Translate(dashVelocity * Time.deltaTime * speed, Space.World);
            transform.rotation = Quaternion.LookRotation(dashVelocity, Vector3.up);
            yield return null;
        }
        chargeMultiplier = 1;
        StopDash();

    }

    void StopDash()
    {
        speed = startSpeed;
        dashing = false;
        trail.enabled = false;
    }
}
