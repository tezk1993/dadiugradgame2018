﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementDrag : MonoBehaviour {

    public float startSpeed;
    public float dashSpeed;
    public float dashTime;

    private float speed;
    private bool startPosChosen;
    private Vector2 startPos;
    private Vector2 curPos;
    private float dist;
    private Vector2 dir;
    private float screenHeight;
    private float screenWidth;
    private bool isDashing;

	// Use this for initialization
	void Start () {

        screenHeight = Screen.currentResolution.height;
        screenWidth = Screen.currentResolution.width;
        startPos = new Vector2(screenWidth / 2, screenHeight / 2);
        speed = startSpeed;
	}
	
	// Update is called once per frame
	void Update () {
        Movement();
        
    }

    void Movement()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 2 && Input.GetTouch(1).phase == TouchPhase.Began && !isDashing)
            {
                speed = dashSpeed;
                isDashing = true;
                Invoke("StopDash", dashTime);
            }
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                //Debug.Log("TouchPhase = Began");
                startPosChosen = true;
                //startPos = Input.GetTouch(0).position;
                dir = curPos - startPos;
                dir.Normalize();

            }
            if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                //Debug.Log("TouchPhase = Moved");
                curPos = Input.GetTouch(0).position;
                dir = curPos - startPos;

                dir.Normalize();
                //Debug.Log("Dir = " + dir);
                //Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

                transform.Translate(dir.x * speed * Time.deltaTime, 0, dir.y * speed * Time.deltaTime);
            }
            if (Input.GetTouch(0).phase == TouchPhase.Stationary)
            {
                transform.Translate(dir.x * speed * Time.deltaTime, 0, dir.y * speed * Time.deltaTime);
            }
            if (Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                //Debug.Log("TouchPhase = Ended");
                startPosChosen = false;
                dir = Vector2.zero;
                dist = 0f;
            }
            
            
        }
    }

    void StopDash()
    {
        speed = startSpeed;
        isDashing = false;
    }
}
