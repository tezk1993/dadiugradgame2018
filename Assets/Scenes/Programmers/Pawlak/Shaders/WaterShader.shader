﻿Shader "Unlit/WaterShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseTex("Noise texture", 2D) = "white" {}
		_CausticTeX("Caustic", 2D) = "white" {}

		_Color("Water color", Color) = (1,1,1,1)
		_waterPeriod("Period of the water", Range(-10,50)) = 1
		_waterMagnitude("Magnitude", Range(0,0.05)) = 0.05
		_offset("offset", Range(0,10)) = 1


	}
		SubShader
		{
			Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
			ZWrite Off Lighting Off Cull Off Fog { Mode Off } Blend One Zero
			LOD 110

			GrabPass {"_GrabTexture"}

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				sampler2D _GrabTexture;
				sampler2D _MainTex;
				sampler2D _NoiseTex;
				sampler2D _CausticTex;

				float4 _Color;
				float _waterPeriod;
				float _waterMagnitude;
				float _offset;

				float2 sinusoid(float2 x, float2 m, float2 M, float2 period) 
				{
					float2 excursion = M - m;
					float2 coefficient = 3.1415 * 2.0 / period;
					return excursion / 2.0 * (1.0 + sin(x*coefficient)) + m;
				}

			struct vertInput
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
			};

			struct vertOutput
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;

				float4 worldSpacePos : TEXCOORD2;
				float4 grabUV : TEXCOORD3;
			};

			
			vertOutput vert (vertInput v)
			{
				vertOutput o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.texcoord = v.texcoord;
				o.texcoord1 = v.texcoord1;
					
				o.worldSpacePos = mul(unity_ObjectToWorld, v.vertex);
				o.grabUV = ComputeGrabScreenPos(o.vertex);
				return o;
			}
			
			fixed4 frag (vertOutput i) : SV_Target
			{
				fixed4 noise = tex2D(_NoiseTex, i.texcoord);
				fixed4 mainColor = tex2D(_MainTex, i.texcoord1);

				float time = _Time[1];
				float2 waterDisplacement =
				sinusoid
					(
						float2 (time, time) + (noise.xy) * _offset,
						float2(-_waterMagnitude, -_waterMagnitude),
						float2(+_waterMagnitude, +_waterMagnitude),
						float2(_waterPeriod, _waterPeriod)
					);

				i.grabUV.xy += waterDisplacement;
				fixed4 col = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.grabUV));
				fixed4 causticColor = tex2D(_CausticTex, i.texcoord.xy*0.25 + waterDisplacement * 5);
				return col * mainColor * _Color * causticColor;
			}
			ENDCG
		}
	}
}
