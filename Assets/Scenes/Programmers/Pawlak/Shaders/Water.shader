﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'



// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/WaterIntersect"
{
	Properties
	{
	   _Color("Main Color", Color) = (1, 1, 1, .5)
	   _IntersectionColor("Intersection Color", Color) = (1, 1, 1, .5)
	   _IntersectionThresholdMax("Intersection Threshold Max", float) = 1
	   _DisplGuide("Displacement guide", 2D) = "white" {}
	   _DisplAmount("Displacement amount", float) = 0
	   _Strength("Wave strength", Range(0,1)) = 0.5
	   _Speed("Wave speed", Range(-200,200)) = 100
	   _Transparency("Transparency of water", Range(0,1)) = 0.5

	   _specColor ("Specular reflection color", Color) = (1,1,1,1)
	   _Shininess ("Shininess", Float) = 10
	}
		SubShader
	   {
		   Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }

		   Pass
		   {
			  Blend SrcAlpha OneMinusSrcAlpha
			  ZWrite Off

			  CGPROGRAM
			  #pragma vertex vert
			  #pragma fragment frag
			  #pragma multi_compile_fog
			  #include "UnityCG.cginc"


			  struct appdata
			  {
				  float4 vertex : POSITION;
				  float3 normal : NORMAL;
				  float2 uv : TEXCOORD0;
			  };

			  struct v2f
			  {
				  float2 uv : TEXCOORD0;
				  UNITY_FOG_COORDS(1)
				  float4 vertex : SV_POSITION;
				  float2 displUV : TEXCOORD2;
				  float4 scrPos : TEXCOORD3;
				  float4 col : COLOR;
			  };

			  sampler2D _CameraDepthTexture;
			  float4 _Color;
			  float4 _IntersectionColor;
			  uniform float4 _LightColor0;
			  uniform float4 _specColor;
			  uniform float _Shininess;
			  float _IntersectionThresholdMax;
			  sampler2D _DisplGuide;
			  float4 _DisplGuide_ST;
			  float _Strength;
			  float _Speed;

			  v2f vert(appdata v)
			  {
				  v2f o;
				  //float3 normalDir = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
				  //float3 lightDir;
				  //float3 viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz);
				  //float attenuation;
				  //if(0.0 == _WorldSpaceLightPos0.w)
				  //{
					 // attenuation = 1.0;
					 // lightDir = normalize(_WorldSpaceLightPos0.xyz);
				  //}
				  //else 
				  //{
					 // float3 vertexToLightSource = _WorldSpaceLightPos0.xyz - mul(unity_ObjectToWorld, v.vertex).xyz;
					 // float distance = length(vertexToLightSource);
					 // attenuation = 1.0 / distance;
					 // lightDir = normalize(vertexToLightSource);
				  //}
				  //float3 ambientLight = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;
				  //float3 diffuseReflection = attenuation * _LightColor0.rgb * _Color.rgb * max(0.0, dot(normalDir, lightDir));
				  //float3 specularReflection;
				  //if (dot(normalDir, lightDir) < 0.0) 
				  //{
					 // specularReflection = float3 (0.0, 0.0, 0.0);
				  //}
				  //else 
				  //{
					 // specularReflection = attenuation * _LightColor0.rgb * _specColor.rgb * pow(max(0.0, dot(reflect(-lightDir, normalDir), viewDir)), _Shininess);
				  //}
				  //o.col = float4(ambientLight + diffuseReflection + specularReflection, 1.0);
				  float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				  float displacement = (cos(worldPos.y) + cos(worldPos.x + (_Time.z*_Speed) + v.vertex.x + v.vertex.z));
				  worldPos.z += displacement * _Strength;
				  worldPos.y += displacement * _Strength;
				  //o.vertex = UnityObjectToClipPos(v.vertex);
				  o.vertex = mul(UNITY_MATRIX_VP, worldPos);
				  o.scrPos = ComputeScreenPos(o.vertex);
				  o.displUV = TRANSFORM_TEX(v.uv, _DisplGuide);
				  o.uv = v.uv;
				  UNITY_TRANSFER_FOG(o,o.vertex);
				  return o;
			  }

			  half _DisplAmount;
			  float _Transparency;

			   half4 frag(v2f i) : SV_TARGET
			   {
				  float depth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.scrPos)));

				  float2 displ = tex2D(_DisplGuide, i.displUV - _Time.y / 5).xy;
				  displ = ((displ * 2) - 1) * _DisplAmount;

				  float diff = (saturate(_IntersectionThresholdMax * (depth - i.scrPos.w) + displ));
				  //float4 colorLight = float4(_Color.rgb, _Transparency) * i.col;
				  //fixed4 col = i.col*(lerp(_IntersectionColor, float4(_Color.rgb, _Transparency), step(0.5, diff)));
				  fixed4 col = lerp(_IntersectionColor, _Color, step(0.5, diff));
				  UNITY_APPLY_FOG(i.fogCoord, col);
				  return col;
			   }

			   ENDCG
			}
	   }
		   FallBack "VertexLit"
}