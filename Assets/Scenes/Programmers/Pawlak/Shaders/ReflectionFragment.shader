﻿Shader "Unlit/ReflectionFragment"
{
	Properties
	{
		_CubeMap("Skybox cubemap", Cube) = "white" {}
		_BumpMap("Bumpmap", 2D) = "bump"{}
		_Modifier("A mysterious modifier", Range(-10,10)) = 1
		_MainColor("main color", Color) = (1,1,1,1)
	}
		SubShader
	{
		Tags { "Queue" = "Transparent" }
		LOD 100

		Pass
		{
			ZWrite Off
			Cull off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform samplerCUBE _CubeMap;
			uniform sampler2D _BumpMap;
			sampler2D _CameraDepthTexture;
			float _Modifier;
			float4 _MainColor;

			struct vertInput 
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float3 texcoord : TEXCOORD0;
			};

			struct vertOutput 
			{
				float4 vertex : SV_POSITION;
				float3 texcoord : TEXCOORD0;
				float3 normalDir : TEXCOORD1;
				float3 viewDir : TEXCOORD3;

			};

			vertOutput vert(vertInput input) 
			{
				vertOutput o;
				o.viewDir = mul(unity_ObjectToWorld, input.vertex).xyz - _WorldSpaceCameraPos;
				o.normalDir = normalize(mul(float4(input.normal, 0.0), unity_WorldToObject).xyz);

				//o.normal = normalize(mul(float4(input.normal, 0.0), unity_WorldToObject).xyz);
				o.vertex = UnityObjectToClipPos(input.vertex);
				o.texcoord = input.texcoord;
				o.texcoord.x += _Time;
				return o;
			}

			float4 frag(vertOutput input) : COLOR
			{
				float3 reflectedDir = reflect(input.viewDir, normalize(input.normalDir));
				return texCUBE(_CubeMap, reflectedDir);
			}
			ENDCG
		}
	}
}
