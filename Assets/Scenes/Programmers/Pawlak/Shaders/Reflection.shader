﻿Shader "Custom/Reflection" {
	Properties{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_CubeMap("Reflection Cubemap", CUBE) = "" {}
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Smoothness("Smoothness", Range(0,1)) = 0.0
		_NormalMap("Normal Map", 2D) = "White" {}
		_Color("Color", Color) = (1,1,1,1)

	   _IntersectionColor("Intersection Color", Color) = (1, 1, 1, .5)
	   _IntersectionThresholdMax("Intersection Threshold Max", float) = 1
	   _DisplGuide("Displacement guide", 2D) = "white" {}
	   _DisplAmount("Displacement amount", float) = 0
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200
			Cull off
			CGPROGRAM
			#pragma surface surf Lambert vertex:vert

			struct Input {
				float2 uv_MainTex;
				float2 uv_NormalMap;
				float2 uv_CubeMap;
				float3 worldRefl;
				half Metallic;
				half Smoothness;
				INTERNAL_DATA
			};

			sampler2D _MainTex;
			sampler2D _DisplGuide;
			sampler2D _NormalMap;
			sampler2D _CameraDepthTexture;
			samplerCUBE _CubeMap;
			float4 _Color;
			half _Metallic;
			half _Smoothness;
			void vert(inout appdata_full v)
			{
				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
				v.vertex = mul(UNITY_MATRIX_VP, worldPos);
				v.texcoord = ComputeScreenPos(v.vertex);
			}

			void surf(Input IN, inout SurfaceOutput o) {
				//float depth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(IN.screenPos)));

				//float displ = tex2D(_DisplGuide, )

				IN.uv_NormalMap.x += _Time;
				o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
				//o.Albedo = IN.worldRefl.rgb;
				//o.Metallic = _Metallic;
				//o.Smoothness = _Smoothness;
				o.Albedo = texCUBE(_CubeMap, WorldReflectionVector(IN, o.Normal));
				o.Emission = texCUBE(_CubeMap, WorldReflectionVector(IN, o.Normal));
				//o.Emission = texCUBE(_CubeMap, in_uv_CubeMap);

			}
			ENDCG
	   }
		   FallBack "Diffuse"
}
