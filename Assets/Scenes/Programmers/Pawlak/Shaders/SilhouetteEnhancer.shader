﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/SilhouetteEnhancer"
{
	Properties
	{
		_Color("Main color", Color) = (1,1,1,1)
		_MainTex("Texture", 2D) = "white" {}
		_Intensity("Silhouette intensity", Range(0,1)) = 0.01
		_Speed("Speed", Range(0,10)) = 1
	}
		SubShader
	{

		Tags { "Queue" = "Transparent+5" }

		Pass
		{
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha
		    Ztest Always
			CGPROGRAM

		 #pragma vertex vert  
		 #pragma fragment frag 

		 #include "UnityCG.cginc"

		 uniform float4 _Color; // define shader property for shaders
		sampler2D _MainTex;
		float _Intensity;
		float _Speed;

		 struct vertexInput {
			float4 vertex : POSITION;
			float3 normal : NORMAL;
			float2 uv : TEXCOORD0;
		 };
		 struct vertexOutput {
			float4 pos : SV_POSITION;
			float3 normal : TEXCOORD;
			float3 viewDir : TEXCOORD1;
			float2 uv : TEXCOORD2;
		 };

		 vertexOutput vert(vertexInput input)
		 {
			vertexOutput output;

			float4x4 modelMatrix = unity_ObjectToWorld;
			float4x4 modelMatrixInverse = unity_WorldToObject;

			output.normal = normalize(
			   mul(float4(input.normal, 0.0), modelMatrixInverse).xyz);
			output.viewDir = normalize(_WorldSpaceCameraPos
			   - mul(modelMatrix, input.vertex).xyz);
			output.uv = input.uv;
			output.uv.x += _Time * _Speed;
			output.pos = UnityObjectToClipPos(input.vertex);
			return output;
		 }

		 float4 frag(vertexOutput input) : COLOR
		 {
			float3 normalDirection = normalize(input.normal);
			float3 viewDirection = normalize(input.viewDir);

			float newOpacity = min(1.0, _Color.a
			   / abs(dot(viewDirection, normalDirection))*_Intensity);
			float4 col = tex2D(_MainTex, input.uv) * _Color;
			col.a = newOpacity;
			return col;
			}
			ENDCG
		}
	}
}
