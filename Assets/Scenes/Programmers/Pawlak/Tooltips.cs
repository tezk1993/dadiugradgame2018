﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltips : MonoBehaviour {

	bool firstTimeDash;
	bool firstTimeMural;
	public bool firstTimeSecondWind = false;

	public int killCount = 0;
	public float timeAlive = 0.0f;
	public int timesDashed = 0;
	public int timesSecondWind = 0;

	public float minTooltipTime = 3;
	float tooltipTime;
	public GameObject swTooltip;
	public GameObject swCloseTooltip;
	public GameObject deathScene;
	public Text kills;
	public Text aliveTime;
	public Text secondWindTimes;
	public Text dashesMade;

	public Player player;

	private void Awake()
	{
		
	}
	void OnEnable()
	{
		Player.swCount += SecondWindStat;
		Player.aliveTime += TimeAlive;
		Player.dashCount += IncreaseDash;
		Player.deathScene += DeathScene;
		StateController.kills += IncreaseKill;
	}

	void OnDisable()
	{
		Player.swCount -= SecondWindStat;
		Player.aliveTime -= TimeAlive;
		Player.dashCount -= IncreaseDash;
		Player.deathScene -= DeathScene;
		StateController.kills -= IncreaseKill;
	}


	// Use this for initialization
	void Start () {
		tooltipTime = 0;
		swTooltip = GameObject.Find("SWToolTip");
		swCloseTooltip = GameObject.Find("CloseSWToolTip");
		deathScene = GameObject.Find("DeathScene");

		deathScene.SetActive(false);
		player = GameObject.Find("Player").GetComponent<Player>();
		swTooltip.SetActive(false);
		swCloseTooltip.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {
		if (timesSecondWind > 0 && firstTimeSecondWind)
		{
			swTooltip.SetActive(true);
			swCloseTooltip.SetActive(true);
			Time.timeScale = 0;
			tooltipTime += Time.deltaTime;
			//if (tooltipTime >= minTooltipTime) 
			//{
			//	closeTooltip.SetActive(true);
			//}
		}
		TimeAlive();
	}

	public void CloseToolTip()
	{
		Time.timeScale = 1;
		Debug.Log("K I go awae");
		swTooltip.SetActive(false);
		swCloseTooltip.SetActive(false);
		tooltipTime = 0;
		firstTimeSecondWind = false;
	}

	void IncreaseKill()
	{
		killCount++;
	}

	private void IncreaseDash() 
	{
		timesDashed++;
	}
	void SecondWindStat() 
	{
		timesSecondWind++;
	}
	void TimeAlive() 
	{
		timeAlive += Time.unscaledDeltaTime;
	}

	public void DeathScene() 
	{
		deathScene.SetActive(true);
		kills.text += killCount;
		aliveTime.text += timeAlive;
		dashesMade.text += timesDashed;
		secondWindTimes.text += timesSecondWind;
	}

	public void RestartLevel()
	{
		kills.text = "kills: ";
		aliveTime.text = "Time Alive: ";
		dashesMade.text = "Dashes made: ";
		secondWindTimes.text = "Times in Second Wind: ";
		killCount = 0;
		timeAlive = 0.0f;
		timesDashed = 0;
		timesSecondWind = 0;
		deathScene.SetActive(true);
		SceneSwitcher.ReloadAllInScene();
	}
}
