﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run : MonoBehaviour {


    [Tooltip("container of health icons")] public GameObject healthContainer;
    private GameObject[] healthArray;
    public int healthInt;
    // Use this for initialization
    void Start () {
        GetComponent<Rigidbody>().velocity = Vector3.forward * 0.2f;

        healthArray = new GameObject[healthContainer.transform.childCount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = healthContainer.transform.GetChild(i).gameObject;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            TakeDamage();
            Bullet bullet = collision.gameObject.GetComponent<Bullet>();
            bullet.StopAllCoroutines();
            StartCoroutine(bullet.Reset(0));
        }
    }

    public void TakeDamage()
    {
        if (healthInt > 0)
        {
            healthArray[healthInt-1].SetActive(false);
            healthInt--;
        }

        if (healthInt <= 0 )
        {
            Debug.Log("YOU DIED");
        }
    }
}
