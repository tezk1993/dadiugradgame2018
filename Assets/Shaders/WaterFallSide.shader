﻿Shader "Custom/WaterFallSide"
{
	Properties
	{
		_WaterfallTexture ("Waterfall texture", 2D) = "white" {}
		_Color("Water color", color) = (1,1,1,1)
		_Speed("Speed", Range(-50,50)) = 50
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 100
			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"



				struct vertInput
				{
					float4 vertex : POSITION;
					float4 color : COLOR;
					float2 texcoord : TEXCOORD0;

				};

				struct vertOutput
				{
					float4 vertex : SV_POSITION;
					float4 color : COLOR;
					float2 texcoord : TEXCOORD0;
					float2 displUV : TEXCOORD1;
				};

				sampler2D _WaterfallTexture;
				float4 _WaterfallTexture_ST;
				sampler2D _DisplTex;
				float4 _DisplTex_ST;
				float4 _Color;
				float4 _Color2;
				float _Speed;

				vertOutput vert(vertInput v)
				{
					vertOutput o;
					o.texcoord = v.texcoord;
					o.texcoord.y += _Time * _Speed;
					o.vertex = UnityObjectToClipPos(v.vertex);
					return o;
				}

				float4 frag(vertOutput input) : COLOR
				{

					float2 displacement = tex2D(_DisplTex, input.displUV);
					displacement = ((displacement * 2) - 1);
					//float4 col = tex2D(_WaterfallTexture, input.texcoord) * (1-_Color);
					//return 1 - col;
					return tex2D(_WaterfallTexture, input.texcoord) * _Color;
				}
				ENDCG
			}


		}
	Fallback "VertexLit"
}
