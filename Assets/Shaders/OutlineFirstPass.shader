﻿Shader "Custom/OutlineFirstPass"
{
	Properties
	{
		_MainTex ("Texture", any) = "" {}
		_PixelSize ("Pixel Size", Vector) = (1, 1, 1, 1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			ZTest Always
			Cull Off
			ZWrite Off

			CGPROGRAM
			#pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
            uniform float4 _MainTex_ST;
			float2 _PixelSize;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture

				// pixelSize.x is the same as 1.0/screenWidth
				// See https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
				//float2 pixelSize = float2(_ScreenParams.z - 1, _ScreenParams.w - 1);
				float2 pixelSize = _PixelSize.xy;
				//float2 pixelSize = 0;
				int radius = 1;
				fixed4 color = 0;
				for (int dy = -radius; dy <= radius; dy++)
				{
					for (int dx = -radius; dx <= radius; dx++)
					{
						//the pixel offset in texture coordinates
						float2 texDelta = float2(dx, dy) * pixelSize;
						float2 uv = i.uv + texDelta;
						color = max(color, tex2D(_MainTex, uv));
					}
				}
				return color;
			}
			ENDCG
		}
	}
	Fallback Off
}
