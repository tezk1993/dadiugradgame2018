﻿Shader "Custom/WaterTopReflections"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Foam1Tex ("Foam 1 Texture", 2D) = "white" {}
		_Foam2Tex ("Foam 2 Texture", 2D) = "white" {}
		_Speed ("Speed", Float) = 0
		_FoamSpeed ("Foam Speed", Float) = 0
		_Reflectivity ("Reflectivity", Range(0, 1)) = 0.5
	}
	SubShader
	{
		Tags {"RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv_raw : TEXCOORD1;
				float2 screen_flipped_vert : TEXCOORD2;
				UNITY_FOG_COORDS(3)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			sampler2D _Foam1Tex;
			sampler2D _Foam2Tex;
			sampler2D _ReflectionTex;

			float _Speed;
			float _FoamSpeed;
			float _Reflectivity;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.screen_flipped_vert = ComputeScreenPos(o.vertex);
				o.screen_flipped_vert.y = 1 - o.screen_flipped_vert.y;
				o.uv_raw = v.uv;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				const float waveDensity = 16;
				// sample the texture
				float2 offset = float2(
					cos(i.uv.x * waveDensity + _Time.x * _Speed),
					sin(i.uv.y * waveDensity + _Time.x * _Speed)
				);
				offset *= 0.02;
				fixed4 col = tex2D(_MainTex, i.uv + offset);

				// apply side
				const float inverse_size = 0.95;
				float side_ramp = abs(i.uv_raw.x - 0.5) * 2;
				side_ramp = lerp(side_ramp, 1 , abs(i.uv_raw.y - 0.5) * 2);
				side_ramp = max(0, side_ramp - inverse_size) / (1-inverse_size);
				side_ramp *= side_ramp;

				fixed4 foam1 = tex2D(_Foam1Tex, i.uv_raw);
				//foam1 *= foam1 * foam1; // TODO REMOVE THIS FROM THE HAND PAINTED VERSION

				fixed4 foam2 = tex2D(_Foam2Tex, -i.uv_raw);
				//foam2 *= foam2 * foam2; // TODO REMOVE THIS FROM THE HAND PAINTED VERSION

				fixed4 foam = lerp(foam1, foam2, sin(_Time.y * _FoamSpeed) * 0.5 + 0.5);
				//foam *= side_ramp * lerp(_Tint, 1, 0.5);

				col += foam * foam.a;

				col = lerp(col, tex2D(_ReflectionTex, i.screen_flipped_vert) * 0.5, _Reflectivity);

				col = min(1, col);

				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return float4(col.xyz, 1);
			}
			ENDCG
		}
	}
}
